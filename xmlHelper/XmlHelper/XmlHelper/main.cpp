#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main()
{
	std::cout << "Enter name of the file." << std::endl;
	std::string fileName;
	std::cin >> fileName;

	std::ifstream inStream;
	inStream.open(fileName + ".txt");

	std::vector<std::string> inVec;

	if (!inStream.is_open())
	{
		std::cout << "\n\nSomething fucked up.";
		return 0;
	}

	std::string line = "";
	while (std::getline(inStream, line))
	{
		inVec.push_back(line);
	}

	std::cout << "\nEnter the node's tag." << std::endl;
	std::string nodeTag;
	std::cin >> nodeTag;

	std::ofstream outStream;
	outStream.open("out" + fileName + ".txt");

	for (auto n : inVec)
	{
		outStream << "<" << nodeTag << " value=\"" << n << "\"/>" << std::endl;
	}

	return 0;
}