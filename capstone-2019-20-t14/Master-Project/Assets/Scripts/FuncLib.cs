﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public static class FuncLib
{
    // https://stackoverflow.com/questions/13870725/how-to-search-and-replace-exact-matching-strings-only
    public static string ExactReplace(string input, string find, string replace)
    {
        string textToFind = string.Format(@"\b{0}\b", find);
        return Regex.Replace(input, textToFind, replace);
    }

    public static Transform FindInAll(Transform t, string nameToFind)
    {
        foreach(Transform c in t)
        {
            if (c.name == nameToFind)
                return c;
            else
                return FindInAll(c, nameToFind);
        }

        return null;
    }

    public static bool Contains(this string[] a, string b)
    {
        foreach(var v in a)
        {
            if (v == b)
                return true;
        }

        return false;
    }
}
