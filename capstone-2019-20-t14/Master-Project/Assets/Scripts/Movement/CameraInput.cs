﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CameraInput : MonoBehaviour
{
    public CinemachineBrain brain;
    public GameObject cameraFollowTarget;
    
    [Header("Camera follow boundries")]
    public GameObject bottomLeft;
    public GameObject bottomRight;
    public GameObject topLeft;
    public GameObject topRight;

    private CinemachineFreeLook boardCam; //board camera
    float xSpeed;
    float ySpeed;

    bool init = false;  

    private void Start()
    {
        boardCam = GetComponent<CinemachineFreeLook>();
        xSpeed = boardCam.m_XAxis.m_MaxSpeed;
        ySpeed = boardCam.m_YAxis.m_MaxSpeed;
        //getting initial camera speed values

        boardCam.m_XAxis.m_MaxSpeed = 0;
        boardCam.m_YAxis.m_MaxSpeed = 0;

        StartCoroutine(tempWait());
    }
    void Update()
    {
        if (init)
        {
            if (brain.ActiveVirtualCamera.VirtualCameraGameObject.GetComponent<CinemachineFreeLook>() == boardCam)
            {
                if (Input.GetMouseButtonDown(1))
                {
                    Debug.Log("Moving cam");
                    boardCam.m_XAxis.m_MaxSpeed = xSpeed;
                    boardCam.m_YAxis.m_MaxSpeed = ySpeed;

                }
                else if (Input.GetMouseButtonUp(1))
                {
                    boardCam.m_YAxis.m_MaxSpeed = 0f;
                    boardCam.m_XAxis.m_MaxSpeed = 0f;

                }
            }
            else
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {

                    {
                        brain.ActiveVirtualCamera.Priority = 9;
                        boardCam.Priority = 11;
                    }
                }
            }

            if (Input.mouseScrollDelta.y > .1f)//Scroll wheel up
                ScrollFOV(false);
            else if (Input.mouseScrollDelta.y < -.1f)
                ScrollFOV(true);

            MoveLookTarget();
        }
    }  

    void MoveLookTarget()
    {
        cameraFollowTarget.transform.LookAt(boardCam.transform);
        cameraFollowTarget.transform.localEulerAngles = new Vector3(0, cameraFollowTarget.transform.localEulerAngles.y, 0);

        if (Input.GetKey(KeyCode.W))
        {
            //if (CheckPosition(new Vector2(1, 0)))
            cameraFollowTarget.transform.Translate(cameraFollowTarget.transform.forward * -1, Space.World);
        }
        else if (Input.GetKey(KeyCode.S))
        {
            //if (CheckPosition(new Vector2(-1, 0)))
            cameraFollowTarget.transform.Translate(cameraFollowTarget.transform.forward, Space.World);
        }
        if (Input.GetKey(KeyCode.A))
        {
            //if (CheckPosition(new Vector2(0, -1)))
            cameraFollowTarget.transform.Translate(cameraFollowTarget.transform.right, Space.World);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            //if (CheckPosition(new Vector2(0, 1)))
            cameraFollowTarget.transform.Translate(cameraFollowTarget.transform.right * -1, Space.World);
        }

        cameraFollowTarget.transform.position = new Vector3(cameraFollowTarget.transform.position.x, 0, cameraFollowTarget.transform.position.z);
    }

    bool CheckPosition(Vector2 dir)
    {
        bool canMove = false;

        Vector2 newPos = new Vector2(cameraFollowTarget.transform.position.x + dir.x, cameraFollowTarget.transform.position.z + dir.y);

        if (newPos.y < topLeft.transform.position.z)
        {
            if (newPos.y > bottomLeft.transform.position.z)
            {
                if (newPos.x > bottomLeft.transform.position.x)
                {
                    if (newPos.x < bottomRight.transform.position.x)
                    {
                        canMove = true;
                    }
                }
            }
        }

        return canMove;
    }

    void ScrollFOV(bool up)
    {
        if (up)
        {
            if (boardCam.m_Lens.FieldOfView < 40)
                boardCam.m_Lens.FieldOfView += 5;
        }
        else
        {
            if (boardCam.m_Lens.FieldOfView > 5)
                boardCam.m_Lens.FieldOfView -= 5;
        }
    }

    public void CharacterTarget(CinemachineVirtualCameraBase characterCam)
    {
       
        characterCam.Priority = 11; //sets to greater than anything else
    }

    IEnumerator tempWait() //Needed because Cinemachine isn't ready frame 1
    {
        yield return new WaitForSeconds(.1f);

        init = true;
    }
}
