﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;
[RequireComponent(typeof(SphereCollider))]
public class NPCMovement : MonoBehaviour
{
    //source: https://gist.github.com/Templar2020/8e4f5296de96d8ccf03263bf1a9f277f
    //but added a stop when the player is near, and a random range for how long

    //Animation Vars
    
    //anim is on the child
    Animator anim;

    //(0, 0) - Idle
    //(1, 1) - Character Waddle 1
    //(-1, 1) - Character Waddle 2
    //(-1, -1) - Character Waddle 3
    //(1, -1) - Character Waddle 4
    Vector2 blendXY;
    Vector2 tempBlendXY = new Vector2(0, 0);
    [Header("Animation Vars")]
    public float animTimeMod = .01f;
    public float animTargetRange = .01f;
    float animTime = 0;

    bool doneRampUp = true;
    bool doneRampDown = true;
    bool animating = false; 

    //make sure min and max timer are more than 0.
    [Header("Wander Vars")]
    public float minWanderTimer = 3f;
    public float maxWanderTimer = 10f;
    public float wanderRadius = .5f;
    public bool isPlayerNear;

    private NavMeshAgent agent;
    private Transform goal;

    bool goingToImportantPlace = false;
    [Tooltip("This number is the % chance that this NPC will go to an important place rather than wander randomly")]
    public float chanceToGoToImportantPlace = 1;

    private float timer;
    private float timerEnd;

    struct ImportantPlace
    {
        //Constructor
        public ImportantPlace(Vector3 pos, string n)
        {
            position = pos;
            name = n;
        }

        public Vector3 position;
        public string name;
    }

    List<ImportantPlace> importantLocations = null;

    //private TMP_Text NPCDataText;
    public InformationUIManager manager;

    public bool wantsToTalk = true;
    public float talkCd = 10.0f;
    private DiageticUIManager man;

    private void Awake()
    {
        //NPCDataText = GameObject.Find("NPCDataText").GetComponent<TMP_Text>();
    }

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = transform.GetChild(0).GetComponent<Animator>();
        timerEnd = Rand.GetFloatRange(minWanderTimer, maxWanderTimer);
        timer = timerEnd;
        importantLocations = new List<ImportantPlace>();
        man = GameObject.FindObjectOfType<DiageticUIManager>();
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if(timer >= timerEnd && !isPlayerNear && !goingToImportantPlace)
        {
            int willGoToImportantPlace = Random.Range(0, 101);

            if (willGoToImportantPlace < chanceToGoToImportantPlace)
                goingToImportantPlace = true;

            if (!goingToImportantPlace)
            {
                Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
                agent.SetDestination(newPos);
                timer = 0;
                timerEnd = Rand.GetFloatRange(minWanderTimer, maxWanderTimer);
            }
            else
            {
                agent.destination = GetImportantLocation("");
            }

            if (Mathf.Abs(tempBlendXY.x) < .1f)
                GetNewWaddle();
        }
        if (goingToImportantPlace && (agent.remainingDistance < 1.5f))
        {
            //If going inside building, that code would go here

            goingToImportantPlace = false;
        }

        AnimUpdate();
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layerMask)
    {
        Vector3 randomDirection = Random.insideUnitSphere * dist;

        randomDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randomDirection, out navHit, dist, layerMask);

        return navHit.position;
    }

    void AnimUpdate()
    {
        if (agent.velocity.magnitude > .2f && !animating)
        {
            animTime = 0;
            GetNewWaddle();
        }
        else if (agent.velocity.magnitude <= .2f && animating && (agent.remainingDistance < 1.5f))
        {
            animTime = 0;
            blendXY = Vector2.zero;
            animating = false;
            doneRampDown = false;
            RampDownAnimToIdle();
        }
        else if (animating && (Mathf.Abs(tempBlendXY.x) < .01f))//Should stop gliding
            StartCoroutine(CheckWaddle());

        if (!doneRampUp)
            RampUpAnim(blendXY);

        else if (!doneRampDown)
            RampDownAnimToIdle();
    }

    IEnumerator CheckWaddle()
    {
        yield return new WaitForSeconds(.1f);

        if (Mathf.Abs(tempBlendXY.x) < .01f)
            GetNewWaddle();
    }

    
    public void DebugInteract()
    {
        Debug.Log("interacted with " + gameObject.name);
        BringUpMenuDisplay();
    }

    public void BringUpMenuDisplay()
    {
        //NPCDataText.transform.parent.gameObject.SetActive(true);
        //NPCDataText.text = GetComponent<TownNPC>().GetNPCData();
        //manager.DisplayNPCStats(GetComponent<TownNPC>().GetNPCData());
        //NPCDataText.transform.parent.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        //NPCDataText.transform.parent.GetComponent<Image>().CrossFadeAlpha(1.0f, 0.5f, true);
    }

    Vector3 GetImportantLocation(string specificPlace)
    {
        importantLocations.Clear();

        //Find GameObject with tag ("Important Place")
        GameObject[] temp = GameObject.FindGameObjectsWithTag("Important Place");

        foreach (GameObject go in temp)
            importantLocations.Add(new ImportantPlace(go.transform.position, go.name));

        GameObject[] temp2 = GameObject.FindGameObjectsWithTag("MultiTag");

        foreach (GameObject go in temp)
        {
            if (go.GetComponent<MultiTag>())
                if (go.GetComponent<MultiTag>().ContainsTag("Important Place"))
                    importantLocations.Add(new ImportantPlace(go.transform.position, go.GetComponent<MultiTag>().GetTagAt(1)));
        }

        //Are we looking for a specific place?
        int randomLocation = -1;
        if (specificPlace != "")
        {
            for (int i = 0; i < importantLocations.Count; i++)
            {
                if (importantLocations[i].name == specificPlace)
                    randomLocation = i;
            }
        }
        else//Get a random one based on the list size
            randomLocation = Rand.GetRandom(0, importantLocations.Count);

        //Go there
        return importantLocations[randomLocation].position;
    }

    void GetNewWaddle()
    {
        animating = true;
        SetNewWaddle();
        tempBlendXY = Vector2.zero;
        doneRampUp = false;
        RampUpAnim(blendXY);
    }

    void SetNewWaddle()
    {
        int x = Random.Range((int)-1, (int)2);
        int y = Random.Range((int)-1, (int)2);

        while (x == 0)
            x = Random.Range((int)-1, (int)2);

        while (y == 0)
            y = Random.Range((int)-1, (int)2);

        blendXY = new Vector2(x, y);
    }

    void RampUpAnim(Vector2 xy)
    {
        animTime += animTimeMod;
        tempBlendXY.x = Mathf.Lerp(tempBlendXY.x, blendXY.x, animTime);
        tempBlendXY.y = Mathf.Lerp(tempBlendXY.y, blendXY.y, animTime);

        anim.SetFloat("BlendX", tempBlendXY.x);
        anim.SetFloat("BlendY", tempBlendXY.y);

        if (Mathf.Abs(blendXY.x) - Mathf.Abs(tempBlendXY.x) < animTargetRange)
        {
            doneRampUp = true;
        }
    }

    void RampDownAnimToIdle()
    {
        animTime += animTimeMod;
        tempBlendXY.x = Mathf.Lerp(tempBlendXY.x, 0, animTime);
        tempBlendXY.y = Mathf.Lerp(tempBlendXY.y, 0, animTime);

        anim.SetFloat("BlendX", tempBlendXY.x);
        anim.SetFloat("BlendY", tempBlendXY.y);

        if (Mathf.Abs(tempBlendXY.x) < animTargetRange)
        {
            doneRampDown = true;
        }
    }

    public void DoDialogue(NPCDataStorage.Guild_NPC npc)
    {
        var dialogue = new TownNPCDialogue(GetComponent<TownNPC>().getNPC(), npc);
        dialogue.ConstructConversation();
        man.GenerateDialogueButton(dialogue, transform.position);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "NPC")
        {
            var otherNPC = other.gameObject.GetComponent<TownNPC>().getNPC();
            if (wantsToTalk && other.gameObject.GetComponent<NPCMovement>().wantsToTalk)
            {
                wantsToTalk = false;
                other.gameObject.GetComponent<NPCMovement>().wantsToTalk = false;
                DoDialogue(otherNPC);
                StartCoroutine(TalkCooldown(talkCd));
                other.gameObject.GetComponent<NPCMovement>().StartCoroutine(other.gameObject.GetComponent<NPCMovement>().TalkCooldown(talkCd));
            }
        }
    }

    public IEnumerator TalkCooldown(float t)
    {
        yield return new WaitForSeconds(t);
        wantsToTalk = true;
    }

}
