﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;
public class CharacterMovement : MonoBehaviour
{

    //source: https://www.youtube.com/watch?v=GANwdCKoimU


    public Transform tpCam;
    public float speed;

    //private CharacterController cc;
    private NavMeshAgent navMeshAgent;
    private Vector3 moveDirection = Vector3.zero;

    private void Start()
    {
        //cc = GetComponent<CharacterController>();

        navMeshAgent = GetComponent<NavMeshAgent>();
    }
 

    void Update()
    {
        //get movement vector and speed
        /* moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
         moveDirection *= speed;

         Vector3 actualMotion = Quaternion.Euler(0, tpCam.eulerAngles.y, 0) * moveDirection;

         cc.Move(actualMotion  * Time.deltaTime);
         */

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            if(Physics.Raycast(ray, out hit, 2000))
            {
                navMeshAgent.destination = hit.point;
            }
        }
        
    }
}
