﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFreeMovement : MonoBehaviour
{
    /*
     * Controls:
     * WASD - Directional movement
     * Right Mouse Down - Enable drag to turn
     * Middle Mouse Down - Enable pan (lock y position)
     * Mouse wheel - FOV change
     * Q - Raise
     * E - Lower
     * Left Shift - Double speed movement
     * Alt + Right Mouse Down - Rotate around what you're looking at or world center 
     * F - Emergency Button, resets position to Start
     */
    public Transform mapCenter;
    Vector3 lookPoint;

    public bool invertCameraX = true;
    public bool invertCameraY = false;

    public bool invertMiddleMouseX = false;
    public bool invertMiddleMouseY = false;

    Vector2 inverseCameraControls = new Vector2(1, 1);
    Vector2 inverseMiddleMouseControls = new Vector2(1, 1);

    public float moveSpeed = 1f;
    public float speedMultiply = 2f; 
    public float lookSensitivity = 1f;
    public float rotateSpeed = 1f;

    public float maxSpeed = 3f;

    float lerpTimeToStop = .25f;

    float upwardsGainMultiplier = .5f;

    float middleMousePanLockY;
    public float middleMouseMoveMultiplier = 10f;

    bool shiftSpeed = false;

    bool isPanMoving = false;

    Vector3 ogPos;
    Vector3 ogEuler;

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        ogPos = gameObject.transform.position;
        ogEuler = gameObject.transform.eulerAngles;

        InvertCamera(true, invertCameraX);
        InvertCamera(false, invertCameraY); 
        InvertMiddleMouse(true, invertMiddleMouseX);
        InvertMiddleMouse(false, invertMiddleMouseY);
    }

    void Update()
    {
        if ((rb.drag > 0 && rb.velocity.magnitude < .1f)
            || (rb.drag > 0 && isPanMoving))
        {
            rb.drag = 0;
            rb.velocity = Vector3.zero;
        }

        if (Input.GetKey(KeyCode.LeftShift))
            shiftSpeed = true;
        else
            shiftSpeed = false;

        if (!Input.GetMouseButton(2))
            PanMovement();
        else
        {
            if (Input.GetMouseButtonDown(2))
                middleMousePanLockY = gameObject.transform.position.y;

            MiddleMousePan();
        }

        //Used for world rotation
        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            //If I'm looking at something, rotate around that
            //else, rotate around world center
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(.5f, .5f, 0));
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 10, Color.yellow);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.name == "GroundCollider")
                    lookPoint = hit.point;
                else
                    lookPoint = mapCenter.position;
            }
            else
                lookPoint = mapCenter.position;
        }

        if (Input.GetMouseButton(1))
        {
            if (!Input.GetKey(KeyCode.LeftAlt))
                RotateCamera();
            else
                RotateAroundWorldCenter();
        }

        if (Input.mouseScrollDelta.y > .5f)
            FOVZoom(true);
        else if (Input.mouseScrollDelta.y < -.5f)
            FOVZoom(false);

        //Pay respects and reset your position
        if (Input.GetKeyDown(KeyCode.F))
        {
            gameObject.transform.position = ogPos;  
            gameObject.transform.eulerAngles = ogEuler;
        }
    }

    void PanMovement()
    {
        if (Input.GetKey(KeyCode.W))
        {
            rb.AddRelativeForce(Vector3.forward * moveSpeed, ForceMode.Acceleration);
            //gameObject.transform.Translate(Vector3.forward * newSpeed, Space.Self);
            isPanMoving = true;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            rb.AddRelativeForce(Vector3.forward * moveSpeed * -1, ForceMode.Acceleration);
            //gameObject.transform.Translate(Vector3.forward * newSpeed * -1, Space.Self);
            isPanMoving = true;
        }
        if (Input.GetKey(KeyCode.A))
        {
            rb.AddRelativeForce(Vector3.right * moveSpeed * -1, ForceMode.Acceleration);
            //gameObject.transform.Translate(Vector3.right * newSpeed * -1, Space.Self);
            isPanMoving = true;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.AddRelativeForce(Vector3.right * moveSpeed, ForceMode.Acceleration);
            //gameObject.transform.Translate(Vector3.right * newSpeed, Space.Self);
            isPanMoving = true;
        }
        if (Input.GetKey(KeyCode.Q))
        {
            rb.AddRelativeForce(Vector3.up * moveSpeed * -1, ForceMode.Acceleration);
            //gameObject.transform.Translate(Vector3.up * newSpeed * -1 * upwardsGainMultiplier, Space.Self);
            isPanMoving = true;
        }
        else if (Input.GetKey(KeyCode.E))
        {
            rb.AddRelativeForce(Vector3.up * moveSpeed, ForceMode.Acceleration);
            //gameObject.transform.Translate(Vector3.up * newSpeed * upwardsGainMultiplier, Space.Self);
            isPanMoving = true;
        }

        if (isPanMoving)
        {
            if (shiftSpeed)
                rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed * speedMultiply);
            else
                rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
            //rb.velocity = new Vector3 (Mathf.Clamp(rb.velocity.x, maxSpeed * -1, maxSpeed), Mathf.Clamp(rb.velocity.y, maxSpeed * -1, maxSpeed), Mathf.Clamp(rb.velocity.z, maxSpeed * -1, maxSpeed));
        }

        if (isPanMoving && !IsMoving(true))
        {
            rb.drag = 10;
            isPanMoving = false;
        }
    }

    void MiddleMousePan()
    {
        rb.AddRelativeForce(Input.GetAxis("Mouse X") * Vector3.right * moveSpeed * middleMouseMoveMultiplier * inverseMiddleMouseControls.x, ForceMode.Acceleration);
        rb.AddRelativeForce(Input.GetAxis("Mouse Y") * Vector3.forward * moveSpeed * middleMouseMoveMultiplier * inverseMiddleMouseControls.y, ForceMode.Acceleration);

        gameObject.transform.position = new Vector3(gameObject.transform.position.x, middleMousePanLockY, gameObject.transform.position.z);

        isPanMoving = true;
        
        if (isPanMoving && !IsMoving(false))
        {
            rb.drag = 10;
            isPanMoving = false;
        }
    }

    void RotateCamera()
    {
        gameObject.transform.Rotate(new Vector3(Input.GetAxis("Mouse Y") * rotateSpeed * inverseCameraControls.y, Input.GetAxis("Mouse X") * rotateSpeed * inverseCameraControls.x, 0), Space.Self);

        //Get rid of pitch
        gameObject.transform.eulerAngles = new Vector3(gameObject.transform.eulerAngles.x, gameObject.transform.eulerAngles.y, 0);
    }

    void FOVZoom(bool zoomIn)
    {
        if (zoomIn)
        {
            if (Camera.main.fieldOfView >= 8)
                Camera.main.fieldOfView -= 3;
        }
        else
        {
            if (Camera.main.fieldOfView <= 47)
                Camera.main.fieldOfView += 3;
        }
    }

    void RotateAroundWorldCenter()
    {
        //Use world center
        gameObject.transform.RotateAround(lookPoint, Vector3.up, Input.GetAxis("Mouse X") * rotateSpeed * inverseCameraControls.x);
        
    }

    bool IsMoving(bool isWASD)
    {
        bool temp = true;

        if (isWASD)
        {
            if (!Input.GetKey(KeyCode.W)
                && !Input.GetKey(KeyCode.S)
                && !Input.GetKey(KeyCode.A)
                && !Input.GetKey(KeyCode.D)
                && !Input.GetKey(KeyCode.Q)
                && !Input.GetKey(KeyCode.E))
                temp = false;
        }
        else
            if (!Input.GetMouseButton(1))
                temp = false;

        return temp;
    }

    public void InvertCamera(bool isX, bool value)
    {
        if (isX)
        {
            if (value)
                inverseCameraControls.x = -1;
            else
                inverseCameraControls.x = 1;
        }
        else
        {
            if (value)
                inverseCameraControls.y = -1;
            else
                inverseCameraControls.y = 1;
        }
    }

    public void InvertMiddleMouse(bool isX, bool value)
    {
        if (isX)
        {
            if (value)
                inverseMiddleMouseControls.x = -1;
            else
                inverseMiddleMouseControls.x = 1;
        }
        else
        {
            if (value)
                inverseMiddleMouseControls.y = -1;
            else
                inverseMiddleMouseControls.y = 1;
        }
    }
}
