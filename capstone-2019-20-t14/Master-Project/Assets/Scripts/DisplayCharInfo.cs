﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisplayCharInfo : MonoBehaviour
{
    public NPCDataStorage data;
    private Text t;
    private int i = 0;
    private void Start()
    {
        t = GetComponent<Text>();
    }
    // Update is called once per frame
    void Update()
    {
        t.text = "Name: " + data.listOfNPCs[i].firstName + " " + data.listOfNPCs[i].lastName +
            "\nLevel 1 " + data.listOfNPCs[i].displayClassName +
            "\nStats: \n" + data.listOfNPCs[i].statData.DisplayStatData(); 
   
    }

    public void GenerateNewNPC()
    {
        data.GenerateNewNPC();
    }

    public void GoNext()
    {
        ++i;
        if (i > data.listOfNPCs.Count - 1)
            --i;
    }

    public void GoPrevious()
    {
        --i;
        if (i < 0)
            i = 0;
    }
}
