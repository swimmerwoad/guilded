﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/Wardrobe")]
public class NPCWardrobe : ScriptableObject
{
    [Tooltip("Example: /Meshes/Characters/Armor/")]
    public string folder = "";
    public Dictionary<string, Object> objectDictionary;

    [SerializeField] //Just to show in inspector to confirm it's working
    private List<Object> fbxObjs;

    public void FillWardrobe()
    {
        fbxObjs.Clear();

        objectDictionary = new Dictionary<string, Object>();

        string newPath = Application.dataPath + folder;

        foreach (string file in System.IO.Directory.GetFiles(newPath))
        {
            if (!file.Contains(".meta") && file.Contains(".fbx"))
            {
                string[] allWords = file.Split('/');
                string fileName = allWords[allWords.Length - 1];
                
                Object t = (Object)AssetDatabase.LoadAssetAtPath("Assets" + folder + fileName, typeof(Object));

                fbxObjs.Add(t);

                //Remove .fbx
                allWords = fileName.Split('.');
                fileName = allWords[0];

                Debug.Log(fileName);

                objectDictionary.Add(fileName, t);
            }
        }
    }

    public Object GetObject(string objName)
    {
        Object returnObj = null;

        //return random
        if (objName == "")
        {
            int randInt = Random.Range(0, fbxObjs.Count);

            returnObj = fbxObjs[randInt];
        }
        else
        {
            if (objectDictionary.ContainsKey(objName))
                returnObj = objectDictionary[objName];
        }

        return returnObj;
    }
}
