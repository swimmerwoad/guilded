﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NPCWardrobe))]
public class NPCWardrobeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Fill"))
        {
            Object temp = Selection.activeObject;

            NPCWardrobe wardrobe = (NPCWardrobe)temp;
            wardrobe.FillWardrobe();
        }
    }
}
