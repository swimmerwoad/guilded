﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCDressUp : MonoBehaviour
{
    public NPCWardrobe HeadObjs;
    public NPCWardrobe ChestObjs;
    public NPCWardrobe LegObjs;
    public NPCWardrobe HandObjs;

    // Start is called before the first frame update
    void Start()
    {
        SpawnRandom();
    }

    void SpawnRandom()
    {
        Instantiate(HeadObjs.GetObject(""), gameObject.transform.GetChild(0).transform);
        Instantiate(ChestObjs.GetObject(""), gameObject.transform.GetChild(0).transform);
        Instantiate(LegObjs.GetObject(""), gameObject.transform.GetChild(0).transform);
        Instantiate(HandObjs.GetObject(""), gameObject.transform.GetChild(0).transform);
    }
}
