﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownNPC : MonoBehaviour
{
    [HideInInspector]public NPCDataStorage.Guild_NPC thisNPC;
    
    public int index;
    public NPCDataStorage d;
    public bool hasQuest;

    // Start is called before the first frame update
    void Start()
    {
        //if (!hasQuest)
        //{
        //    gameObject.GetComponentInChildren<Canvas>().enabled = false;
        //}
     
    }

    // Update is called once per frame
    void Update()
    {

        
        
    }

  

    public void SetNPC(NPCDataStorage.Guild_NPC newNPC)
    {
        thisNPC = newNPC;
    }

    public string GetNPCData()
    {
        //return thisNPC.DisplayNPCData();
        return d.listOfNPCs[index].DisplayNPCData();
    }

    public string GetGreeting()
    {
        return d.listOfNPCs[index].GetRandomGreeting();
    }

    public NPCDataStorage.Guild_NPC getNPC()
    {
        return thisNPC;
    }


    private void OnMouseDown()
    {
        
    }
}
