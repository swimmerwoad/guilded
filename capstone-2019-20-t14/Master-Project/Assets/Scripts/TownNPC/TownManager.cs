﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownManager : MonoBehaviour
{
    public int NPCToGenerate = 8;
    public NPCDataStorage data;

    private static TownManager instance;
    private void Awake()
    {
        //if (instance == null)
        //    instance = this;
        //else
        //    DestroyImmediate(gameObject);

        //DontDestroyOnLoad(gameObject);
        data.Reset();
        GenerateNPCs();
        DistributeNPCs();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GenerateNPCs()
    {
        for(int i = 0; i < NPCToGenerate; ++i)
        {
            data.GenerateNewNPC();
        }
    }

    private void DistributeNPCs()
    {
        var NPCList = GameObject.FindGameObjectsWithTag("NPC");
        int counter = 0;
        foreach(var NPC in NPCList)
        {
            //NPC.GetComponent<TownNPC>().SetNPC(data.GetNPCFromPool());
            NPC.GetComponent<TownNPC>().index = counter;
            NPC.GetComponent<TownNPC>().SetNPC(data.GetNPCFromPool(NPC.GetComponent<TownNPC>().index));
            ++counter;
        }
    }
}
