﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
public class TownQuestManager : MonoBehaviour
{
    public QuestDataStorage qds;
    TextColors textColors;
    private List<NPCDataStorage.Guild_NPC> npcs = new List<NPCDataStorage.Guild_NPC>();
    public GameObject nextDayButton;
    public InformationUIManager manager;
    // Start is called before the first frame update
    void Start()
    {
        textColors = GetComponent<TextColors>();

        if (qds.CurQuest == null)
        {
            qds.GenerateNewQuest();
            GenerateTextColors();
        }

        manager = FindObjectOfType<InformationUIManager>();
        nextDayButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //temp
        if (Input.GetKeyDown(KeyCode.Space))
        {
            qds.GenerateNewQuest();
        }
    }

    public void GenerateQuest()
    {
        qds.GenerateNewQuest();
        GenerateTextColors();
    }

    public string GetQuestTitle()
    {
        return qds.CurQuest.Title;
    }

    public void RunQuest()
    {
        qds.RunQuest(npcs);
        nextDayButton.SetActive(true);
        npcs.Clear();
    }

    public bool AddNPC(int index)
    {
        bool relStatus = CheckRelations(index);
        if(relStatus)
            npcs.Add(GetComponent<TownManager>().data.listOfNPCs[index]);

        return relStatus;
    }

    private bool CheckRelations(int index)
    {
        var newNpc = GetComponent<TownManager>().data.listOfNPCs[index];
        foreach (var n in npcs)
        {
            if (n.RelationshipStatus[newNpc] <= -10 || newNpc.RelationshipStatus[n] <= -10)
            {
                StringBuilder b = new StringBuilder();
                b.Append(n.firstName + ": " + "\"I don't want to work with " + newNpc.firstName + "\"");
                manager.SetAngryUI(b.ToString());
                return false;
            }
        }

        return true;
    }

    public void RemoveNPC(int index)
    {
        //npcs.Remove(GetComponent<TownManager>().data.listOfNPCs[index]);
        var npcToFind = GetComponent<TownManager>().data.listOfNPCs[index];
        npcs.Remove(npcToFind);
    }

    public int GetCount()
    {
        return npcs.Count;
    }

    public List<string> GetActions()
    {
        return qds.LastQuest.QActions;
    }

    public void ClearNPCs()
    {
        npcs.Clear();
    }

    void GenerateTextColors()
    {
        string questTitle = qds.CurQuest.Title;

        for (int i = 0; i < textColors.ColorMap.Count; i++)
        {
            if (qds.CurQuest.QuestImportantTags.ContainsKey(textColors.ColorMap[i].name))
            {
                questTitle = questTitle.Replace(qds.CurQuest.QuestImportantTags[textColors.ColorMap[i].name], "<color=#" + ColorUtility.ToHtmlStringRGBA(textColors.ColorMap[i].col) + ">" + qds.CurQuest.QuestImportantTags[textColors.ColorMap[i].name] + "</color>");
            }
        }

        qds.CurQuest.Title = questTitle;
    }
}
