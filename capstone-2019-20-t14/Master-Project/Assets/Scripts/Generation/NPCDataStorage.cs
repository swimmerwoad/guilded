﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;
public partial class NPCPersonality
{
    public void GeneratePersonality()
    {
        for(int i = 0; i < NPCPersonalityStorage.npcPersonalitynodes.Count; ++i)
        {
            /*
            int next = Rand.GetRandom(-100, 100);
            PersonalityTrait t = new PersonalityTrait();
            t.TraitValue = next;
            t.TraitType = NPCPersonalityStorage.npcPersonalitynodes[i].PersonalityType;
            t.PersonalityID = NPCPersonalityStorage.npcPersonalitynodes[i].PersonalityId;
            //t.DisplayValue = NPCPersonalityStorage.npcPersonalitynodes[i].FindInDictionary(next);
            var tuple = NPCPersonalityStorage.npcPersonalitynodes[i].FindInDictionary(next);
            t.DisplayValue = tuple.Item1;
            personalityTraits.Add(t);
            greetings.Add(NPCPersonalityStorage.npcPersonalitynodes[i].nodeGreetings[tuple.Item2]);
            */
            PersonalityTrait t = new PersonalityTrait();
            var curNode = NPCPersonalityStorage.npcPersonalitynodes[i];
            int distrIndex = RollOnTraitDist(curNode);
            t.TraitValue = RollWithinDist(curNode, distrIndex);
            t.PersonalityID = curNode.PersonalityId;
            t.TraitType = curNode.PersonalityType;
            var tuple = curNode.FindInDictionary(t.TraitValue);
            t.DisplayValue = tuple.Item1;
            greetings.Add(curNode.nodeGreetings[tuple.Item2]);
            t.TheirHighRelationReactionToMe = curNode.HighRelationReaction;
            t.TheirMediumRelationReactionToMe = curNode.MediumRelationReaction;
            t.TheirLowRelationReactionToMe = curNode.LowRelationReaction;
            personalityTraits.Add(t);
        }

        greetings.AddRange(NPCPersonalityStorage.npcPossibleGreetings);
    }

    public int RollOnTraitDist(NPCPersonalityStorageNode n)
    {
        int next = Rand.GetRandom(0, 100);
        int i;
        for(i = 0; i < n.percents.Length - 1; ++i)
        {
            if (next < n.percents[i])
                return i;
            next -= n.percents[i];
        }

        return i;
    }

    public int RollWithinDist(NPCPersonalityStorageNode n, int index)
    {
        var range = n.FindInRange(index);
        return Rand.GetRandom(range.Item1, range.Item2);
    }

    public void DebugValues()
    {
        for(int i = 0; i < personalityTraits.Count; ++i)
        {
            Debug.Log("<color=green>" + personalityTraits[i].TraitType + ": " + personalityTraits[i].DisplayValue + " (" + personalityTraits[i].TraitValue + ")</color>");
        }

        Debug.Log("<color=purple>Says: " + greetings[Rand.GetRandom(0, greetings.Count)] + "</color>");
    }
};

[CreateAssetMenu(fileName = "NPCStorage", menuName = "ScriptableObjects/NPCData", order = 2)]
public class NPCDataStorage : ScriptableObject
{


    public class NPC_Stat
    {
        public string displayName { get; set; }
        public int statValue { get; set; }
        //public GeneratorData.NPC_STAT_ID statID { get; set; }
    }

    /// <summary>
    /// Skills are leveled, unlike stats. 
    /// Things like "gardening" "Fighting". So on.
    /// </summary>
    public class NPC_Skill
    {
        public NPC_Skill()
        {
            CurrentLevel = 0;
            CurrentExpPoints = 0;
            NextLevelPoints = 0;
            DisplayName = "";
        }
        public int CurrentLevel { get; set; }
        public int CurrentExpPoints { get; set; }
        public int NextLevelPoints { get; set; }
        public string DisplayName { get; set; }
    }

    public class NPC_Class_Data
    {
        public Dictionary<GeneratorData.NPC_STAT_ID, NPC_Stat> stats = new Dictionary<GeneratorData.NPC_STAT_ID, NPC_Stat>();
        public Dictionary<string, NPC_Skill> skills = new Dictionary<string, NPC_Skill>();
        public NPC_Stat personalityValue { get; set; }
        public NPC_Stat quirkValue { get; set; }
        public NPC_Stat traitValue { get; set; }

        public string DisplayStatData()
        {
            return "Strength: " + stats[GeneratorData.NPC_STAT_ID.STRENGTH].statValue +
                "\nDexterity: " + stats[GeneratorData.NPC_STAT_ID.DEXTERITY].statValue +
                "\nConstitution: " + stats[GeneratorData.NPC_STAT_ID.CONSTITUTION].statValue +
                "\nIntelligence: " + stats[GeneratorData.NPC_STAT_ID.INTELLIGENCE].statValue +
                "\nWisdom: " + stats[GeneratorData.NPC_STAT_ID.WISDOM].statValue;
        }

        public void DebugSkills()
        {
            foreach (KeyValuePair<string, NPC_Skill> kvp in skills)
                Debug.Log("<color=magenta>Skill: " + kvp.Key + " is level " + kvp.Value.CurrentLevel + "</color>");
        }

        public List<NPC_Skill> OutputSkillData()
        {
            List<NPC_Skill> nList = new List<NPC_Skill>();

            foreach (var kvp in skills)
                nList.Add(kvp.Value);

            return nList;
        }
    }

    public class NPC_Dialogue_Options
    {
        public string[] favoriteOptions = new string[2];
        public string[] likeOptions = new string[2];
        public string[] dislikeOptions = new string[5];
        public string[] hateOptions = new string[5];

        public string[] favoriteConvoVerbs = new string[3];
        
        public void GenerateOptions()
        {
            var totalLength = favoriteOptions.Length + likeOptions.Length + dislikeOptions.Length + hateOptions.Length;
            var pool = new List<string>();
            //for(int i =0; i < totalLength; ++i)
            //{

            //}

            var convoPool = new List<string>();

            int i = 0;
            while(totalLength > i)
            {
                var oneItem = DialogueDataHolder.townConvos.GetRandomSubject();
                if (!pool.Contains(oneItem))
                {
                    pool.Add(oneItem);
                    ++i;
                }
            }

            i = 0;
            while(convoPool.Count < favoriteConvoVerbs.Length)
            {
                var oneItem = DialogueDataHolder.townConvos.GetRandomVerbs();
                if (!convoPool.Contains(oneItem))
                {
                    convoPool.Add(oneItem);
                    ++i;
                }
            }

            AddOptions(ref favoriteOptions, pool);
            AddOptions(ref likeOptions, pool);
            AddOptions(ref dislikeOptions, pool);
            AddOptions(ref hateOptions, pool);
            AddOptions(ref favoriteConvoVerbs, convoPool);
        }

        private void AddOptions(ref string[] options, List<string> p)
        {
            for(int i = 0; i < options.Length; ++i)
            {
                options[i] = p[0];
                p.RemoveAt(0);
            }
        }

        public string[] GetLikesAndFavorites()
        {
            var a = new string[favoriteOptions.Length + likeOptions.Length];
            favoriteOptions.CopyTo(a, 0);
            likeOptions.CopyTo(a, favoriteOptions.Length);
            return a;
        }
        
        public string[] GetDislikesAndHates()
        {
            var a = new string[dislikeOptions.Length + hateOptions.Length];
            dislikeOptions.CopyTo(a, 0);
            hateOptions.CopyTo(a, dislikeOptions.Length);
            return a;
        }

        public string GetFavVerb()
        {
            return favoriteConvoVerbs[Rand.GetRandom(0, favoriteConvoVerbs.Length)];
        }
    }

    public class Guild_NPC
    {
        public Guild_NPC()
        {
            statData = new NPC_Class_Data();
            Personality = new NPCPersonality();
        }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public NPC_Class_Data statData {get;set;}
        public GeneratorData.NPC_CLASS_ID classId;
        public int Level { get; set; }
        public string displayClassName { get; set; } //****TODO STORE THIS ELSEWHERE
        // Other data like looks?
        public NPC_Dialogue_Options DialogueOptions { get; set; }

        public NPCPersonality Personality { get; set; }

        public Dictionary<Guild_NPC, int> RelationshipStatus = new Dictionary<Guild_NPC, int>();

        public string DisplayShortData()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(firstName + " " + lastName);
            s.AppendLine("level " + Level + " " + displayClassName);

            return s.ToString();
        }

        public string DisplayNPCData()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(DisplayShortData());
            s.AppendLine(statData.DisplayStatData());

            return s.ToString();
        }

        public void LevelUp()
        {
            ++Level;

            int points = 2;
            while(points > 0)
            {
                var statToLevel = (GeneratorData.NPC_STAT_ID)Rand.GetRandom(0, (int)GeneratorData.NPC_STAT_ID.NUM_STATS);
                ++statData.stats[statToLevel].statValue;
                --points;
            }
        }

        public string GetRandomGreeting()
        {
            return firstName + ": " + Personality.greetings[Rand.GetRandom(0, Personality.greetings.Count)];
        }

        public void GenerateFavorites()
        {
            if (DialogueOptions == null)
                DialogueOptions = new NPC_Dialogue_Options();
            DialogueOptions.GenerateOptions();
        }

        public void DebugFavorites()
        {
            for(int i = 0; i < DialogueOptions.favoriteOptions.Length; ++i)
                Debug.Log("<color=cyan>Favorite is: " + DialogueOptions.favoriteOptions[i] + "</color>");

            for (int i = 0; i < DialogueOptions.likeOptions.Length; ++i)
                Debug.Log("<color=green>Like is: " + DialogueOptions.likeOptions[i] + "</color>");

            for (int i = 0; i < DialogueOptions.dislikeOptions.Length; ++i)
                Debug.Log("<color=yellow>Dislike is: " + DialogueOptions.dislikeOptions[i] + "</color>");

            for (int i = 0; i < DialogueOptions.hateOptions.Length; ++i)
                Debug.Log("<color=red>Hate is: " + DialogueOptions.hateOptions[i] + "</color>");
        }

        public void ModifyRelationship(Guild_NPC rhs, int change)
        {
            RelationshipStatus[rhs] += change;
        }

        public string GetRandomTrustReaction(int value)
        {
            return Personality.personalityTraits[Rand.GetRandom(0, Personality.personalityTraits.Count)].GetRelationReaction(value);
        }

        public string GetRandomTrustReaction(Guild_NPC other)
        {
            return Personality.personalityTraits[Rand.GetRandom(0, Personality.personalityTraits.Count)].GetRelationReaction(RelationshipStatus[other]);
        }

        public string ActualGetRandomTrustReaction(Guild_NPC other)
        {
            return other.Personality.personalityTraits[Rand.GetRandom(0, Personality.personalityTraits.Count)].GetRelationReaction(RelationshipStatus[this]);
        }

    }

    public List<Guild_NPC> listOfNPCs = new List<Guild_NPC>();
    public List<Guild_NPC> NPC_Pool = new List<Guild_NPC>();
    public GeneratorData dat;
    public int maxInitPoints;
    private int tempInitPoints;
    public readonly int pointUpperLimit = 13, pointLowerLimit = 1;
    public void GenerateNewNPC()
    {
        Guild_NPC newNPC = new Guild_NPC();
        newNPC.firstName = GeneratorData.NPC_Names.GetFirstName();
        newNPC.lastName = GeneratorData.NPC_Names.GetLastName();

        // Generate initial distribution
        List<int> statPoints = new List<int>();
        //newNPC.classId = (GeneratorData.NPC_CLASS_ID)Random.Range(0, (int)GeneratorData.NPC_CLASS_ID.NUM_CLASSES);
        //newNPC.classId = (GeneratorData.NPC_CLASS_ID)Rand.GetRandom(0, (int)GeneratorData.NPC_CLASS_ID.NUM_CLASSES);
        var npcClass = dat.skillAndClassData[Rand.GetRandom(0, dat.skillAndClassData.Count)];
        newNPC.displayClassName = npcClass.Id;
        tempInitPoints = maxInitPoints;
        for (int i = 0; i < dat.skillMap.Count; ++i)
        {
            //int randomNext = Random.Range(5, tempInitPoints > 18 ? 18 : tempInitPoints);
            // Hack to make sure we don't end up going under by accident.
            if (tempInitPoints <= pointLowerLimit)
                tempInitPoints = pointLowerLimit;
            int randomNext = Rand.GetRandom(pointLowerLimit, tempInitPoints > pointUpperLimit ? pointUpperLimit : tempInitPoints);
            statPoints.Add(randomNext);
            tempInitPoints -= randomNext;
            newNPC.statData.skills.Add(dat.skillMap[i].Id, new NPC_Skill());
        }

        // Distribute any remaining points.
        while (tempInitPoints > 0)
        {
            int ind = Rand.GetRandom(0, statPoints.Count);
            if (statPoints[ind] < pointUpperLimit)
                ++statPoints[ind];

            --tempInitPoints;
        }
        /*
         * TODO:
         * Get the weighted values.
         * Distribute the highest points to the highest weighted ids.
         * Then, just do the skills.
         */

        var weightedIds = npcClass.weights;
        weightedIds.Sort((a, b) => a.SkillWeightValue.CompareTo(b.SkillWeightValue));

        statPoints.Sort();
        statPoints.Reverse();

        int index = weightedIds.Count - 1;
        for (; index >= 0; --index)
        {
            newNPC.statData.skills[weightedIds[index].SkillID].CurrentLevel = statPoints[index];
            newNPC.statData.skills[weightedIds[index].SkillID].DisplayName = weightedIds[index].SkillID;
            statPoints.RemoveAt(index);
        }
        index = 0;
        foreach(KeyValuePair<string, NPC_Skill> kvp in newNPC.statData.skills)
        {
            if(kvp.Value.CurrentLevel == 0)
            {
                kvp.Value.CurrentLevel = statPoints[index];
                kvp.Value.DisplayName = kvp.Key;
                ++index;
            }
        }

        // TODO: MAKE THIS OBSOLTE
        // ====================================

        //switch (newNPC.classId)
        //{
        //    case GeneratorData.NPC_CLASS_ID.ARCHER:
        //        newNPC.displayClassName = "Archer";
        //        break;
        //    case GeneratorData.NPC_CLASS_ID.CLERIC:
        //        newNPC.displayClassName = "Cleric";
        //        break;
        //    case GeneratorData.NPC_CLASS_ID.FIGHTER:
        //        newNPC.displayClassName = "Fighter";
        //        break;
        //    case GeneratorData.NPC_CLASS_ID.MAGE:
        //        newNPC.displayClassName = "Mage";
        //        break;
        //    case GeneratorData.NPC_CLASS_ID.PALADIN:
        //        newNPC.displayClassName = "Paladin";
        //        break;
        //}
        //tempInitPoints = maxInitPoints;


        //// Distribute any remaining points.

        //statPoints.Sort();
        //// Get the weighted values.
        //List<GeneratorData.NPC_Stat_Weight> weights = new List<GeneratorData.NPC_Stat_Weight>();
        //GeneratorData.NPC_Class_Generator gen = dat.classData.Find(c => c.id == newNPC.classId);
        //weights.Add(gen.strWeight);
        //weights.Add(gen.dexWeight);
        //weights.Add(gen.conWeight);
        //weights.Add(gen.intWeight);
        //weights.Add(gen.wisWeight);

        ////weights.Sort(delegate(GeneratorData.NPC_Stat_Weight s1, GeneratorData.NPC_Stat_Weight s2) { return s1.statID.CompareTo(s2.statID); });
        //weights.Sort((a, b) => a.statValue.CompareTo(b.statValue));
        ////var sortedList = weights.OrderByDescending(x => x.statValue).ToList();


        //// distribute.
        //for (int i = 0; i < (int)GeneratorData.NPC_STAT_ID.NUM_STATS; ++i)
        //{
        //    NPC_Stat newStat = new NPC_Stat();
        //    //newStat.statID = weights[i].statID;
        //    newStat.statValue = statPoints[i];
        //    // TODO: LOAD DISPLAY NAMES WITH THE WEIGHTS.
        //    switch (weights[i].statID)
        //    {
        //        case GeneratorData.NPC_STAT_ID.STRENGTH:
        //            newStat.displayName = "Strength";
        //            break;
        //        case GeneratorData.NPC_STAT_ID.DEXTERITY:
        //            newStat.displayName = "Dexterity";
        //            break;
        //        case GeneratorData.NPC_STAT_ID.CONSTITUTION:
        //            newStat.displayName = "Constitution";
        //            break;
        //        case GeneratorData.NPC_STAT_ID.INTELLIGENCE:
        //            newStat.displayName = "Intelligence";
        //            break;
        //        case GeneratorData.NPC_STAT_ID.WISDOM:
        //            newStat.displayName = "Wisdom";
        //            break;
        //    }

        //    newNPC.statData.stats.Add(weights[i].statID, newStat);
        //}
        var st = new NPC_Stat();
        st.statValue = 10;
        newNPC.statData.stats.Add(GeneratorData.NPC_STAT_ID.CONSTITUTION, st);
        newNPC.statData.stats.Add(GeneratorData.NPC_STAT_ID.STRENGTH, st);
        newNPC.statData.stats.Add(GeneratorData.NPC_STAT_ID.INTELLIGENCE, st);
        newNPC.statData.stats.Add(GeneratorData.NPC_STAT_ID.WISDOM, st);
        newNPC.statData.stats.Add(GeneratorData.NPC_STAT_ID.DEXTERITY, st);

        // ===================================


        newNPC.Level = 1;
        newNPC.Personality.GeneratePersonality();

        Debug.Log("<color=blue>" + newNPC.firstName + " " + newNPC.lastName + " is a " + newNPC.displayClassName + " and has these skills...</color>");
        //newNPC.Personality.DebugValues();
        newNPC.statData.DebugSkills();
        newNPC.GenerateFavorites();
        //newNPC.DebugFavorites();

        //AddRelationshipsToAll(newNPC);
        AddRelationshipsToAllDEBUG(newNPC);
        AddRelationshipsToThis(ref newNPC);

        listOfNPCs.Add(newNPC);
        NPC_Pool.Add(newNPC);
    }

    public Guild_NPC GetNPCFromPool()
    {
        return NPC_Pool[Rand.GetRandom(0, NPC_Pool.Count)];
    }

    public Guild_NPC GetNPCFromPool(int index)
    {
        return NPC_Pool[index];
    }

    public int GetNPCFromPoolI()
    {
        return Rand.GetRandom(0, NPC_Pool.Count);
    }

    private void AddRelationshipsToAll(Guild_NPC npc)
    {
        foreach (var n in listOfNPCs)
            n.RelationshipStatus.Add(npc, 50);
    }

    private void AddRelationshipsToAllDEBUG(Guild_NPC npc)
    {
        int counter = 0;
        foreach (var n in listOfNPCs)
        {
            n.RelationshipStatus.Add(npc, counter == 0 ? -50 : 50);
            ++counter;
        }
    }

    private void AddRelationshipsToThis(ref Guild_NPC npc)
    {
        foreach (var n in listOfNPCs)
            npc.RelationshipStatus.Add(n, 50);
    }

    public void Reset()
    {
        listOfNPCs.Clear();
        NPC_Pool.Clear();
    }
}
