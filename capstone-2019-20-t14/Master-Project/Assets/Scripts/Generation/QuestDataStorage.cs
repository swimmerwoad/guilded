﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "QuestData", menuName = "ScriptableObjects/QuestData", order = 3)]
public class QuestDataStorage : ScriptableObject
{
    //For the future when we're making the special text special colors
    public class QuestGenerationReturn
    {
        public string quest = "";
        public Dictionary<string, string> importantValues = new Dictionary<string, string>();
    }

    public class Quest_Chat
    {
        public Quest_Chat()
        {
            Chat = new List<string>();
            Objects = new List<string>();
        }
        public List<string> Chat { get; set; }
        public List<string> Objects { get; set; }

        public readonly string name1Tag = "NAME";
        public readonly string name2Tag = "NAME2";
        public readonly string objectTag = "OBJECT";
    }

    private class Quest_Check_Holder
    {
        public List<string> neededChecks = new List<string>();
        public Dictionary<string, int> checks = new Dictionary<string, int>();
    }

    public class Quest_LoadPool
    {
        public Quest_LoadPool()
        {
            QTemplate1 = new List<string>();
            QTemplate2 = new List<string>();
            QGiverUnnamed = new List<string>();
            QGiverNamed = new List<string>();
            Area = new List<string>();
            QTags = new List<string>();
            QScript1 = new List<string>();
            QScript2 = new List<string>();
            QMoreTags = new List<string>();
            QAction = new List<string>();
            EnemyActions = new List<string>();
            EscortRequester = new List<string>();
            MilitaryGroup = new List<string>();
            AnimalType = new List<string>();
            WildAnimalType = new List<string>();
            EnemyType = new List<string>();
            EnemyTypeHuman = new List<string>();
            AffectedArea = new List<string>();
            Checks = new List<Quest_Check>();
            SkChecks = new List<Quest_Skill_Check>();
            Chat = new Quest_Chat();
        }
        public List<string> QTemplate1 { get; set; }
        public List<string> QTemplate2 { get; set; }
        public List<string> QGiverUnnamed { get; set; }
        public List<string> QGiverNamed { get; set; }
        public List<string> Area { get; set; }
        public List<string> QTags { get; set; }
        public List<string> QScript1 { get; set; }
        public List<string> QScript2 { get; set; }
        public List<string> QMoreTags { get; set; }
        public List<string> QAction { get; set; }
        public List<string> EnemyActions { get; set; }
        public List<string> EscortRequester { get; set; }
        public List<string> MilitaryGroup { get; set; }
        public List<string> AnimalType { get; set; }
        public List<string> WildAnimalType { get; set; }
        public List<string> EnemyType { get; set; }
        public List<string> EnemyTypeHuman { get; set; }
        public List<string> AffectedArea { get; set; }
        public List<Quest_Check> Checks { get; set; }
        public List<Quest_Skill_Check> SkChecks { get; set; }
        public string PositiveCheck { get; set; }
        public string NegativeCheck { get; set; }
        public Quest_Chat Chat { get; set; }


        private readonly string giverTag = "QUEST_GIVER";
        private readonly string giverTagG = "QUEST_GIVER_U";
        private readonly string giverTagN = "QUEST_GIVER_N";
        private readonly string areaTag = "AREA_OF_ACTION";
        private readonly string enemyHumanTag = "ENEMY_TYPE_HUMAN";
        private readonly string enemyTag = "ENEMY_TYPE";
        private readonly string escortRequesterTag = "ESCORT_REQUESTER";
        private readonly string militaryGroupTag = "MILITARY_GROUP";
        private readonly string animalTag = "ANIMAL_TYPE";
        private readonly string wildAnimalTag = "WILD_ANIMAL_TYPE";
        private readonly string eActionTag = "ENEMY_ACTION";
        private readonly string affectedAreaTag = "AFFECTED_AREA";
        private readonly string rewardTag = "REWARD";
        //V2 stuff
        private readonly string questTag = "QUEST_TAGS";
        private readonly string moreTags = "MORE_TAGS";
        private readonly string qAction = "QUEST_ACTION";

        public (QuestGenerationReturn, List<string>) GetNewTitle()
        {
            QuestGenerationReturn finishedQuest = new QuestGenerationReturn(); //used to get both a string and a map

            // Snag the tags, since those are checks we'll have to generate 100%.
            List<string> checksWeKnow = new List<string>();

            //Rand.SeedRandom();
            string mainQuestTag = QTags[Rand.GetRandom(0, QTags.Count)]; //Get the tag of the current quest
            checksWeKnow.Add(mainQuestTag);
            finishedQuest.importantValues.Add("MainQuestTag", mainQuestTag);
            int baseTemplate = Rand.GetRandom(0, 2); //Change max based on how many base templates there are
            string s;
            string miniS = "";
            switch (baseTemplate) //Choose a base template to use and a coresponding subscript
            {
                case 0:
                    s = QTemplate1[Rand.GetRandom(0, QTemplate1.Count)];
                    while (!miniS.Contains("@" + mainQuestTag))
                        miniS = QScript1[Rand.GetRandom(0, QScript1.Count)];
                    s = s.Replace(questTag, miniS);
                    s = s.Replace("@" + mainQuestTag, "");
                    break;

                case 1:
                    s = QTemplate2[Rand.GetRandom(0, QTemplate2.Count)];
                    while (!miniS.Contains("@" + mainQuestTag))
                        miniS = QScript2[Rand.GetRandom(0, QScript2.Count)];
                    s = s.Replace(questTag, miniS);
                    s = s.Replace("@" + mainQuestTag, "");
                    break;

                default:
                    Debug.LogError("Hey bucko the template for quests isn't being set right. I fixed it for ya this one time, but be more careful!");
                    s = QTemplate1[Rand.GetRandom(0, QTemplate1.Count)];
                    while (!miniS.Contains("@" + mainQuestTag))
                        miniS = QScript1[Rand.GetRandom(0, QScript1.Count)];
                    s = s.Replace(questTag, miniS);
                    s = s.Replace("@" + mainQuestTag, "");
                    break;
            }

            int r = Rand.GetRandom(0, 2);
            if (s.Contains(giverTagG))
            {
                s = RepInStringWithMapAdd(s, giverTagG, QGiverUnnamed, finishedQuest, "QuestGiver");
            }
            else if (s.Contains(giverTagN))
            {
                s = RepInStringWithMapAdd(s, giverTagN, QGiverNamed, finishedQuest, "QuestGiver");
            }
            else
            {
                if (r == 0)
                    s = RepInStringWithMapAdd(s, giverTag, QGiverUnnamed, finishedQuest, "QuestGiver");
                else
                    s = RepInStringWithMapAdd(s, giverTag, QGiverNamed, finishedQuest, "QuestGiver");
            }
            s = s.Replace(finishedQuest.importantValues["QuestGiver"], finishedQuest.importantValues["QuestGiver"]);

            //Replace the QUEST_ACTION with a quest action relating to the current quest
            string tempAction = QAction[Rand.GetRandom(0, QAction.Count)];
            while (!tempAction.Contains("@" + mainQuestTag))
                tempAction = QAction[Rand.GetRandom(0, QAction.Count)];
            s = s.Replace(qAction, tempAction);
            s = s.Replace("@" + mainQuestTag, "");

            //Do the extra tag stuff, don't add another tag if rand is less than 5
            int amountOfExtraTags = Rand.GetRandom(0, 10);
            string ExtraTag1 = "";
            string ExtraTag2 = "";
            if (amountOfExtraTags < 5)
            {
                s = s.Replace(moreTags, "");
            }
            if (amountOfExtraTags > 4) //Add one more extra tag
            {
                ExtraTag1 = QTags[Rand.GetRandom(0, QTags.Count)];
                while (ExtraTag1 == mainQuestTag)
                    ExtraTag1 = QTags[Rand.GetRandom(0, QTags.Count)];

                checksWeKnow.Add(ExtraTag1);
                finishedQuest.importantValues.Add("ExtraTag1", ExtraTag1);

                string tempMiniS = QMoreTags[Rand.GetRandom(0, QMoreTags.Count)];
                while (!tempMiniS.Contains("@" + ExtraTag1))
                    tempMiniS = QMoreTags[Rand.GetRandom(0, QMoreTags.Count)];
                s = s.Replace(moreTags, "Also," + tempMiniS + "TEMP_END");
                s = s.Replace("@" + ExtraTag1, "");

                //Replace the QUEST_ACTION with a quest action relating to the Extra Tag 
                tempAction = QAction[Rand.GetRandom(0, QAction.Count)];
                while (!tempAction.Contains("@" + ExtraTag1))
                    tempAction = QAction[Rand.GetRandom(0, QAction.Count)];
                s = s.Replace(qAction, tempAction);
                s = s.Replace("@" + ExtraTag1, "");
            }
            if (amountOfExtraTags > 7)//Add two more extra tags (one more than above)
            {
                ExtraTag2 = QTags[Rand.GetRandom(0, QTags.Count)];
                while (ExtraTag2 == mainQuestTag)
                    ExtraTag2 = QTags[Rand.GetRandom(0, QTags.Count)];

                checksWeKnow.Add(ExtraTag2);
                finishedQuest.importantValues.Add("ExtraTag2", ExtraTag2);

                string tempMiniS = QMoreTags[Rand.GetRandom(0, QMoreTags.Count)];
                while (!tempMiniS.Contains("@" + ExtraTag2))
                    tempMiniS = QMoreTags[Rand.GetRandom(0, QMoreTags.Count)];
                s = s.Replace("TEMP_END", " and" + tempMiniS + ". ");
                s = s.Replace("@" + ExtraTag2, "");

                //Replace the QUEST_ACTION with a quest action relating to the Extra Tag 
                tempAction = QAction[Rand.GetRandom(0, QAction.Count)];
                while (!tempAction.Contains("@" + ExtraTag2))
                    tempAction = QAction[Rand.GetRandom(0, QAction.Count)];
                s = s.Replace(qAction, tempAction);
                s = s.Replace("@" + ExtraTag2, "");
            }

            if (s.Contains("TEMP_END"))
                s = s.Replace("TEMP_END", ". ");

            //Do the other jazz that doesn't require as much thinking
            if (s.Contains(areaTag))
            {
                s = RepInStringWithMapAdd(s, areaTag, Area, finishedQuest, "AreaOfAction");
                s = s.Replace(finishedQuest.importantValues["AreaOfAction"], finishedQuest.importantValues["AreaOfAction"]);
            }

            if (s.Contains(escortRequesterTag))
                s = RepInStringWithMapAdd(s, escortRequesterTag, EscortRequester, finishedQuest, "EscortRequester");

            if (s.Contains(militaryGroupTag))
                s = RepInStringWithMapAdd(s, militaryGroupTag, MilitaryGroup, finishedQuest, "MilitaryGroup");

            if (s.Contains(enemyHumanTag))
                s = RepInStringWithMapAdd(s, enemyHumanTag, EnemyTypeHuman, finishedQuest, "EnemyTypeHuman");

            if (s.Contains(enemyTag))
                s = RepInStringWithMapAdd(s, enemyTag, EnemyType, finishedQuest, "EnemyType");

            if (s.Contains(wildAnimalTag))
                s = RepInStringWithMapAdd(s, wildAnimalTag, WildAnimalType, finishedQuest, "WildAnimalType");

            if (s.Contains(animalTag))
                s = RepInStringWithMapAdd(s, animalTag, AnimalType, finishedQuest, "AnimalType");

            if (s.Contains(eActionTag))
                s = RepInStringWithMapAdd(s, eActionTag, EnemyActions, finishedQuest, "EnemyAction");

            if (s.Contains(affectedAreaTag))
                s = RepInStringWithMapAdd(s, affectedAreaTag, AffectedArea, finishedQuest, "AffectedPlace");

            string tempString = Rand.GetRandom(200, 10000) + " gold";
            finishedQuest.importantValues.Add("Reward", tempString);
            s = s.Replace(rewardTag, tempString);

            finishedQuest.quest = s;

            Debug.Log(finishedQuest.quest);

            return (finishedQuest, checksWeKnow);
        }

        private string RepInString(string s, string tag, List<string> sL)
        {
            return s.Replace(tag, sL[Rand.GetRandom(0, sL.Count)]);
        }

        //Same as RepInString, but this one records the random value before it is replaced
        private string RepInStringWithMapAdd(string s, string tag, List<string> sL, QuestGenerationReturn qcr, string mapKey)
        {
            string tempString = sL[Rand.GetRandom(0, sL.Count)];
            qcr.importantValues.Add(mapKey, tempString);
            return s.Replace(tag, tempString);
        }

        public List<Quest_Check> GenerateChecks(int difficultyDist, int numChecks)
        {
            List<Quest_Check> nQC = new List<Quest_Check>();
            for (int i = 0; i < numChecks; ++i)
            {
                Quest_Check qc = new Quest_Check();
                GeneratorData.NPC_STAT_CHECK type = (GeneratorData.NPC_STAT_CHECK)Rand.GetRandom(0, (int)GeneratorData.NPC_STAT_CHECK.NUM_CHECKS);
                // Check to make sure we have no duplicate checks.
                while (nQC.Any(t => t.Type == type))
                    type = (GeneratorData.NPC_STAT_CHECK)Rand.GetRandom(0, (int)GeneratorData.NPC_STAT_CHECK.NUM_CHECKS);

                var l = Checks.FindAll(c => c.Type == type);
                qc.CheckValue = l[Rand.GetRandom(0, l.Count)].CheckValue;
                qc.Type = type;
                nQC.Add(qc);
            }

            for (int i = 0; i < nQC.Count; ++i)
            {
                if (nQC[i].Type != GeneratorData.NPC_STAT_CHECK.LEVEL_CHECK)
                {
                    nQC[i].Difficulty = Rand.GetRandom(5, 15);
                    difficultyDist -= nQC[i].Difficulty;
                }
            }

            // Check to make sure we didn't go under or over the distribution by accident.
            while (difficultyDist != 0)
            {
                if (difficultyDist < 0)
                {
                    --nQC[Rand.GetRandom(0, nQC.Count)].Difficulty;
                    ++difficultyDist;
                }
                if (difficultyDist > 0)
                {
                    ++nQC[Rand.GetRandom(0, nQC.Count)].Difficulty;
                    --difficultyDist;
                }
            }

            return nQC;
        }

        public List<Quest_Skill_Check> GenerateChecks(int numChecks, int difficultyDist, List<string> reqChecks)
        {

            List<Quest_Skill_Check> allChecks = new List<Quest_Skill_Check>();
            List<string> fillerPool = new List<string>();
            Quest_Skill_Check nCheck;

            for (int i = 0; i < numChecks; ++i)
            {
                nCheck = new Quest_Skill_Check(); // I'm sowwy
                var newCheck = reqChecks[Rand.GetRandom(0, reqChecks.Count)];
                reqChecks.Remove(newCheck);
                fillerPool.Add(newCheck);
                if (reqChecks.Count == 0)
                {
                    reqChecks.AddRange(fillerPool);
                    fillerPool.Clear();
                }

                nCheck.Type = newCheck;
                nCheck.Difficulty = Random.Range(5, 15);
                difficultyDist -= nCheck.Difficulty;
                allChecks.Add(nCheck);
            }

            while (difficultyDist != 0)
            {
                if (difficultyDist < 0)
                {
                    --allChecks[Rand.GetRandom(0, allChecks.Count)].Difficulty;
                    ++difficultyDist;
                }

                if (difficultyDist > 0)
                {
                    ++allChecks[Rand.GetRandom(0, allChecks.Count)].Difficulty;
                    --difficultyDist;
                }
            }

            return allChecks;
        }
    }

    public class Quest_Check
    {
        public GeneratorData.NPC_STAT_CHECK Type { get; set; }
        public string CheckValue { get; set; }
        public int Difficulty { get; set; }
    }

    public class Quest_Skill_Check
    {
        public string Type { get; set; }
        public string DisplayCheck { get; set; }
        public int Difficulty { get; set; }
    }

    public class Quest
    {
        public Quest()
        {
            CurChecks = new List<Quest_Check>();
            QuestMonsters = new List<QuestMonster>();
            SkChecks = new List<Quest_Skill_Check>();
            QActions = new List<string>();
            Dictionary<string, string> QuestImportantTags = new Dictionary<string, string>();
        }
        public string Title { get; set; }

        public Dictionary<string, string> QuestImportantTags { get; set; }
        public List<QuestMonster> QuestMonsters { get; set; }
        public int NumDiffMonsters { get; set; }
        // Idea: specific rolls?
        public List<Quest_Check> CurChecks { get; set; }
        public List<Quest_Skill_Check> SkChecks { get; set; }
        // Idea: battles?
        // Idea: rewards?
        public List<string> QActions { get; set; }
    }

    public class QuestMonster
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public int BaseDamage { get; set; }
        public int DamageRange { get; set; }
        public int Speed { get; set; }
    }

    public Quest CurQuest { get; set; }
    public Quest LastQuest { get; set; }
    public Quest_LoadPool QuestPool { get; set; }
    public List<QuestMonster> QuestMonsters { get; set; }

    private Quest_Check_Holder qch = new Quest_Check_Holder();

    public int currentDiffDistribution;
    public int currentNumChecks;

    public void GenerateNewQuest()
    {
        if (CurQuest != null)
            LastQuest = CurQuest;
        Quest q = new Quest();
        var questItems = QuestPool.GetNewTitle();
        q.Title = questItems.Item1.quest;
        q.QuestImportantTags = questItems.Item1.importantValues;
        qch.neededChecks = questItems.Item2;
        q.CurChecks = QuestPool.GenerateChecks(currentDiffDistribution, currentNumChecks);
        q.SkChecks = QuestPool.GenerateChecks(currentNumChecks, currentDiffDistribution, qch.neededChecks);
        CurQuest = q;
    }

    public List<string> RunQuest(List<NPCDataStorage.Guild_NPC> npcsOnQuest)
    {
        List<string> questActions = new List<string>();
        List<bool> succFail = new List<bool>();
        foreach (var c in CurQuest.CurChecks)
        {
            // Get the highest stat of certain type, of the action, from the list of different NPCs. 
            // Roll d20, add 1/2 * stat to the roll, compare to difficulty, check succ / fail.

            // weights.Sort((a, b) => a.statValue.CompareTo(b.statValue));
            //npcsOnQuest.Sort((a,b) => a.statData.stats[]);
            if (CheckStat(c.Type))
            {
                var id = (GeneratorData.NPC_STAT_ID)c.Type;
                // WHAT IS THIS LINE DOING
                npcsOnQuest.Sort((a, b) => a.statData.stats[id].statValue.CompareTo(b.statData.stats[id].statValue));
                // roll.
                RollNonPartyCheck(npcsOnQuest, succFail, c, questActions, npcsOnQuest[npcsOnQuest.Count - 1].statData.stats[id].statValue);
            }
            else if (c.Type == GeneratorData.NPC_STAT_CHECK.LEVEL_CHECK)
            {
                npcsOnQuest.Sort((a, b) => a.Level.CompareTo(b.Level));
                RollNonPartyCheck(npcsOnQuest, succFail, c, questActions, npcsOnQuest[npcsOnQuest.Count - 1].Level);
            }
            else if (c.Type == GeneratorData.NPC_STAT_CHECK.PARTY_MAX_CHECK)
            {
                float diff = 1.0f;
                float succChance = npcsOnQuest.Count / npcsOnQuest.Count;
                float failChance = diff - succChance;
                float roll = Rand.GetFloat();
                succFail.Add(roll > failChance);
                AddCheckDisplay(questActions, "NAME", npcsOnQuest[npcsOnQuest.Count - 1].firstName, c, succFail[succFail.Count - 1]);
            }

            Debug.Log("I'm here!");
        }

        DoChats(questActions, npcsOnQuest);
        ShuffleActions(questActions);

        string s = "The party returned, ";
        bool success = CheckQuestSuccess(succFail);
        s += success ? "succeeding in their task!" : "failing the task.";
        questActions.Add(s);

        var rC = ModifyRelationships(npcsOnQuest, success);
        questActions.Add(rC);

        CurQuest.QActions = questActions;

        foreach (var n in npcsOnQuest)
            n.LevelUp();

        return questActions;
    }

    public List<string> RunQuestWSkills(List<NPCDataStorage.Guild_NPC> npcsOnQuest)
    {
        List<string> questActions = new List<string>();
        List<bool> succFail = new List<bool>();
        //foreach (var c in CurQuest.CurChecks)
        //{
        // Get the highest stat of certain type, of the action, from the list of different NPCs. 
        // Roll d20, add 1/2 * stat to the roll, compare to difficulty, check succ / fail.

        // weights.Sort((a, b) => a.statValue.CompareTo(b.statValue));
        //npcsOnQuest.Sort((a,b) => a.statData.stats[]);
        //    if (CheckStat(c.Type))
        //    {
        //        var id = (GeneratorData.NPC_STAT_ID)c.Type;
        //        // WHAT IS THIS LINE DOING
        //        npcsOnQuest.Sort((a, b) => a.statData.stats[id].statValue.CompareTo(b.statData.stats[id].statValue));
        //        // roll.
        //        RollNonPartyCheck(npcsOnQuest, succFail, c, questActions, npcsOnQuest[npcsOnQuest.Count - 1].statData.stats[id].statValue);
        //    }
        //    else if (c.Type == GeneratorData.NPC_STAT_CHECK.LEVEL_CHECK)
        //    {
        //        npcsOnQuest.Sort((a, b) => a.Level.CompareTo(b.Level));
        //        RollNonPartyCheck(npcsOnQuest, succFail, c, questActions, npcsOnQuest[npcsOnQuest.Count - 1].Level);
        //    }
        //    else if (c.Type == GeneratorData.NPC_STAT_CHECK.PARTY_MAX_CHECK)
        //    {
        //        float diff = 1.0f;
        //        float succChance = npcsOnQuest.Count / npcsOnQuest.Count;
        //        float failChance = diff - succChance;
        //        float roll = Rand.GetFloat();
        //        succFail.Add(roll > failChance);
        //        AddCheckDisplay(questActions, "NAME", npcsOnQuest[npcsOnQuest.Count - 1].firstName, c, succFail[succFail.Count - 1]);
        //    }
        //}

        foreach (var c in CurQuest.SkChecks)
        {
            var id = c.Type;
            if (id == "Animals")
                continue;
            if (id == "Escort")
                continue;
            npcsOnQuest.Sort((a, b) => a.statData.skills[id].CurrentLevel.CompareTo(b.statData.skills[id].CurrentLevel));
            RollSkillCheck(npcsOnQuest, succFail, c, questActions, npcsOnQuest[npcsOnQuest.Count - 1].statData.skills[id].CurrentLevel);
        }

        DoChats(questActions, npcsOnQuest);
        ShuffleActions(questActions);

        string s = "The party returned, ";
        bool success = CheckQuestSuccess(succFail);
        s += success ? "succeeding in their task!" : "failing the task.";
        questActions.Add(s);

        var rC = ModifyRelationships(npcsOnQuest, success);
        questActions.Add(rC);

        CurQuest.QActions = questActions;

        foreach (var n in npcsOnQuest)
            n.LevelUp();

        return questActions;
    }

    private bool CheckStat(GeneratorData.NPC_STAT_CHECK c)
    {
        return c == GeneratorData.NPC_STAT_CHECK.STRENGTH ||
            c == GeneratorData.NPC_STAT_CHECK.DEXTERITY ||
            c == GeneratorData.NPC_STAT_CHECK.CONSTITUTION ||
            c == GeneratorData.NPC_STAT_CHECK.INTELLIGENCE ||
            c == GeneratorData.NPC_STAT_CHECK.WISDOM;
    }

    private void RollNonPartyCheck(List<NPCDataStorage.Guild_NPC> npcsOnQuest, List<bool> succFail, Quest_Check c, List<string> questActions, int stat)
    {
        int roll = Rand.GetRandom(0, 21);
        roll += stat / 2;
        succFail.Add(roll >= c.Difficulty);
        AddCheckDisplay(questActions, "NAME", npcsOnQuest[npcsOnQuest.Count - 1].firstName, c, roll >= c.Difficulty);
    }

    private void RollSkillCheck(List<NPCDataStorage.Guild_NPC> npcsOnQuest, List<bool> succFail, Quest_Skill_Check c, List<string> questActions, int stat)
    {
        int roll = Rand.GetRandom(0, 21);
        roll += stat / 2;
        succFail.Add(roll >= c.Difficulty);
        //AddCheckDisplay(questActions, "NAME", npcsOnQuest[npcsOnQuest.Count - 1].firstName, c, roll >= c.Difficulty);
        AddCheckDsiplay(questActions, "NAME", npcsOnQuest[npcsOnQuest.Count - 1].firstName, c, roll >= c.Difficulty);
    }

    private void AddCheckDisplay(List<string> l, string tag, string name, Quest_Check c, bool pass)
    {
        string p = pass ? QuestPool.PositiveCheck : QuestPool.NegativeCheck;
        l.Add(c.CheckValue.Replace(tag, name) + p);
    }

    private void AddCheckDsiplay(List<string> l, string tag, string name, Quest_Skill_Check c, bool pass)
    {
        string p = pass ? QuestPool.PositiveCheck : QuestPool.NegativeCheck;
        if (c.DisplayCheck == null)
            return;
        l.Add(c.DisplayCheck.Replace(tag, name) + p);
    }

    private void DoChats(List<string> actions, List<NPCDataStorage.Guild_NPC> npcs)
    {
        int numChats = Rand.GetRandom(CurQuest.CurChecks.Count / 2, CurQuest.CurChecks.Count - 1);

        for (int i = 0; i < numChats; ++i)
        {
            string origChat = QuestPool.Chat.Chat[Rand.GetRandom(0, QuestPool.Chat.Chat.Count)];
            var npc1 = npcs[Rand.GetRandom(0, npcs.Count)];
            var npc2 = npcs[Rand.GetRandom(0, npcs.Count)];
            while (npc1 == npc2)
            {
                npc2 = npcs[Rand.GetRandom(0, npcs.Count)];
            }
            var @object = QuestPool.Chat.Objects[Rand.GetRandom(0, QuestPool.Chat.Objects.Count)];

            string s = RepChat(origChat, npc1.firstName, npc2.firstName, QuestPool.Chat.name1Tag, QuestPool.Chat.name2Tag, @object, QuestPool.Chat.objectTag);

            actions.Add(s);
        }

    }

    private string RepChat(string orig, string name1, string name2, string tag1, string tag2, string obj, string objTag)
    {
        orig = FuncLib.ExactReplace(orig, tag1, name1);
        orig = FuncLib.ExactReplace(orig, tag2, name2);
        orig = FuncLib.ExactReplace(orig, objTag, obj);
        return orig;
    }

    private void ShuffleActions(List<string> actions)
    {
        // https://stackoverflow.com/questions/273313/randomize-a-listt
        var n = actions.Count;
        while (n > 1)
        {
            --n;
            var k = Rand.GetRandom(0, n);
            var value = actions[k];
            actions[k] = actions[n];
            actions[n] = value;
        }
    }

    /// <summary>
    /// Roll to check whether the quest was a success or failure.
    /// </summary>
    /// <param name="successes">The amount of successful ability checks, basically.</param>
    private bool CheckQuestSuccess(List<bool> successes)
    {
        // Find how many successes there were.
        var succs = successes.FindAll(x => x == true);
        // Find how likely we are to fail.
        float succRate = (float)succs.Count / (float)successes.Count;
        float failRate = 1 - succRate;
        // Roll the dice, fire emblem style.
        var roll1 = Rand.GetFloat();
        var roll2 = Rand.GetFloat();
        var avgRoll = (roll1 + roll2) / 2.0f;
        return avgRoll > failRate;
    }

    private string ModifyRelationships(List<NPCDataStorage.Guild_NPC> npcs, bool success)
    {
        foreach (var n in npcs)
        {
            for (int i = 0; i < npcs.Count; ++i)
            {
                if (!n.Equals(npcs[i]))
                    n.RelationshipStatus[npcs[i]] += success ? 10 : -10;
            }
        }

        return success ? "The party's relationships with each other improved due to the success."
            : "The party is feeling unhappy with each other after the failure of the last quest.";
    }
}