﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;
using System;

public static partial class NPCPersonalityStorage
{
    public static List<NPCRanges> allRanges = new List<NPCRanges>();
}
public class XmlLoader : MonoBehaviour
{

    public GeneratorData genData;
    public NPCDataStorage d;
    public QuestDataStorage qds;
    public DialogueDataHolder ddh;
    private static bool loaded = false;
    // Start is called before the first frame update
    void Awake()
    {
        //ReadDataInit();
        if(!loaded)
        {
            ReadInitClass();
            LoadQuestXML();
            LoadPersonalities();
            LoadDialogues();
            loaded = true;
        }

        //GenerateNPC();
    }

    [ObsoleteAttribute("This function does old things. Use ReadInitAndClass instead.")]
    private void ReadDataInit()
    {
        // Seed random on startup. 
        Rand.SeedRandom();
        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/NPCStatGen.xml");

        // Load Stat values.
        var nodeList = doc.DocumentElement.SelectNodes("/root/npc_classes/npc_class");

        foreach(XmlNode n in nodeList)
        {
            GeneratorData.NPC_Class_Generator newClass = new GeneratorData.NPC_Class_Generator();
            switch(n.Attributes["type"].Value)
            {
                case "Paladin":
                    newClass.id = GeneratorData.NPC_CLASS_ID.PALADIN;
                    break;
                case "Fighter":
                    newClass.id = GeneratorData.NPC_CLASS_ID.FIGHTER;
                    break;
                case "Mage":
                    newClass.id = GeneratorData.NPC_CLASS_ID.MAGE;
                    break;
                case "Cleric":
                    newClass.id = GeneratorData.NPC_CLASS_ID.CLERIC;
                    break;
                case "Archer":
                    newClass.id = GeneratorData.NPC_CLASS_ID.ARCHER;
                    break;
            }

            newClass.strWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.strWeight.statValue = float.Parse(n.SelectSingleNode("strength_weight").Attributes["value"].Value);
            newClass.strWeight.statID = GeneratorData.NPC_STAT_ID.STRENGTH;
            newClass.dexWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.dexWeight.statValue = float.Parse(n.SelectSingleNode("dexterity_weight").Attributes["value"].Value);
            newClass.dexWeight.statID = GeneratorData.NPC_STAT_ID.DEXTERITY;
            newClass.conWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.conWeight.statValue = float.Parse(n.SelectSingleNode("constitution_weight").Attributes["value"].Value);
            newClass.conWeight.statID = GeneratorData.NPC_STAT_ID.CONSTITUTION;
            newClass.intWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.intWeight.statValue = float.Parse(n.SelectSingleNode("intelligence_weight").Attributes["value"].Value);
            newClass.intWeight.statID = GeneratorData.NPC_STAT_ID.INTELLIGENCE;
            newClass.wisWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.wisWeight.statValue = float.Parse(n.SelectSingleNode("wisdom_weight").Attributes["value"].Value);
            newClass.wisWeight.statID = GeneratorData.NPC_STAT_ID.WISDOM;

            genData.classData.Add(newClass);
        }

        // Load Names.
        GeneratorData.NPC_Names.Init();
        nodeList = doc.DocumentElement.SelectNodes("/root/npc_name_root/first_names");
        foreach(XmlNode n in nodeList)
        {
            foreach(XmlNode c in n)
            {
                GeneratorData.NPC_Names.firstNames.Add(c.Attributes["value"].Value);
            }
        }

        nodeList = doc.DocumentElement.SelectNodes("/root/npc_name_root/last_names");
        foreach (XmlNode n in nodeList)
        {
            foreach (XmlNode c in n)
            {
                GeneratorData.NPC_Names.lastNames.Add(c.Attributes["value"].Value);
            }
        }

        Debug.Log("loaded.");
    }

    private void ReadInitClass()
    {
        Rand.SeedRandom();
        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/NPCStatGenV2.xml");
        var nodeList = doc.DocumentElement.SelectNodes("/root/npc_skill_list/skill");
        foreach(XmlNode n in nodeList)
        {
            GeneratorData.NPC_Skill newSkill = new GeneratorData.NPC_Skill(n.Attributes[0].Value);
            genData.skillMap.Add(newSkill);
        }

        nodeList = doc.DocumentElement.SelectNodes("/root/npc_classes/npc_class");
        foreach(XmlNode n in nodeList)
        {
            GeneratorData.NPC_Skill_Class_Generator nscg = new GeneratorData.NPC_Skill_Class_Generator();
            var tags = n.SelectNodes("npc_skill_tags/skill");
            foreach(XmlNode t in tags)
            {
                GeneratorData.NPC_Skill_Weight w = new GeneratorData.NPC_Skill_Weight();
                w.SkillID = t.Attributes[0].Value;
                w.SkillWeightValue = float.Parse(t.Attributes[1].Value);
                nscg.weights.Add(w);
            }


            nscg.Id = n.Attributes[0].Value;
            genData.skillAndClassData.Add(nscg);
        }

        GeneratorData.NPC_Names.Init();
        nodeList = doc.DocumentElement.SelectNodes("/root/npc_name_root/first_names");
        foreach (XmlNode n in nodeList)
        {
            foreach (XmlNode c in n)
            {
                GeneratorData.NPC_Names.firstNames.Add(c.Attributes["value"].Value);
            }
        }

        nodeList = doc.DocumentElement.SelectNodes("/root/npc_name_root/last_names");
        foreach (XmlNode n in nodeList)
        {
            foreach (XmlNode c in n)
            {
                GeneratorData.NPC_Names.lastNames.Add(c.Attributes["value"].Value);
            }
        }

        Debug.Log("loaded.");
    }

    /// <summary>
    /// Load quest generation data from the XML.
    /// </summary>
    private void LoadQuestXML()
    {
        LoadQuestMonsters();

        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/QuestGenV2.xml");

        var nodeList = doc.DocumentElement.SelectNodes("/root/quest");
        qds.QuestPool = new QuestDataStorage.Quest_LoadPool();

        CreateQuestPool(doc);
        LoadChecks(doc);
        CreateChatPool(doc);
    }

    /// <summary>
    /// Create the quest pool using the below helper function.
    /// </summary>
    /// <param name="doc">The doc that we currently have open.</param>
    private void CreateQuestPool(XmlDocument doc)
    {
        AddToQuest(doc, qds.QuestPool.QTemplate1, "quest_template_1", "base");
        AddToQuest(doc, qds.QuestPool.QTemplate2, "quest_template_2", "base");
        AddToQuest(doc, qds.QuestPool.QGiverUnnamed, "quest_givers_u", "giver");
        AddToQuest(doc, qds.QuestPool.QGiverNamed, "quest_givers", "giver");
        AddToQuest(doc, qds.QuestPool.Area, "area_of_action", "area");
        AddToQuest(doc, qds.QuestPool.QTags, "quest_tags", "tags");
        AddToQuest(doc, qds.QuestPool.QScript1, "quest_script_1", "script");
        AddToQuest(doc, qds.QuestPool.QScript2, "quest_script_2", "script");
        AddToQuest(doc, qds.QuestPool.QMoreTags, "more_tags", "tags");
        AddToQuest(doc, qds.QuestPool.QAction, "quest_action", "action");
        AddToQuest(doc, qds.QuestPool.EscortRequester, "escort_requester", "type");
        AddToQuest(doc, qds.QuestPool.MilitaryGroup, "military_group", "type");
        AddToQuest(doc, qds.QuestPool.AnimalType, "animal_type", "type");
        AddToQuest(doc, qds.QuestPool.WildAnimalType, "wild_animal_type", "type");
        AddToQuest(doc, qds.QuestPool.EnemyType, "enemy_type", "type");
        AddToQuest(doc, qds.QuestPool.EnemyTypeHuman, "enemy_type_human", "type");
        AddToQuest(doc, qds.QuestPool.EnemyActions, "enemy_action", "action");
        AddToQuest(doc, qds.QuestPool.AffectedArea, "affected_area", "place");
    }

    private void CreateChatPool(XmlDocument doc)
    {
        AddToQuest(doc, qds.QuestPool.Chat.Chat, "chats", "chat");
        AddToQuest(doc, qds.QuestPool.Chat.Objects, "chat_objects", "object");
    }

    /// <summary>
    /// Generic helper function to load the different lists for the quests instead of having 80 foreach loops.
    /// </summary>
    /// <param name="doc">the xml document that's loaded.</param>
    /// <param name="values">list of string values we're adding from the XML doc.</param>
    /// <param name="nodeName">parent node name.</param>
    /// <param name="subNodeName">sub nodes within the parent node. There are a lot of child nodes per parent node.</param>
    private void AddToQuest(XmlDocument doc, List<string> values, string nodeName, string subNodeName)
    {
        var nodeList = doc.DocumentElement.SelectNodes("/root/" + nodeName + "/" + subNodeName);
        foreach(XmlNode n in nodeList)
        {
            values.Add(n.Attributes["value"].Value);
        }
    }

    /// <summary>
    /// Similar to above, load each check.
    /// </summary>
    /// <param name="doc">the XML document</param>
    private void LoadChecks(XmlDocument doc)
    {
        AddCheck(doc, qds.QuestPool.Checks, "strength_checks", "check", GeneratorData.NPC_STAT_CHECK.STRENGTH);
        AddCheck(doc, qds.QuestPool.Checks, "dexterity_checks", "check", GeneratorData.NPC_STAT_CHECK.DEXTERITY);
        AddCheck(doc, qds.QuestPool.Checks, "constitution_checks", "check", GeneratorData.NPC_STAT_CHECK.CONSTITUTION);
        AddCheck(doc, qds.QuestPool.Checks, "intelligence_checks", "check", GeneratorData.NPC_STAT_CHECK.INTELLIGENCE);
        AddCheck(doc, qds.QuestPool.Checks, "wisdom_checks", "check", GeneratorData.NPC_STAT_CHECK.WISDOM);
        AddCheck(doc, qds.QuestPool.Checks, "party_checks", "check", GeneratorData.NPC_STAT_CHECK.PARTY_MAX_CHECK);
        AddCheck(doc, qds.QuestPool.Checks, "level_checks", "check", GeneratorData.NPC_STAT_CHECK.LEVEL_CHECK);

        qds.QuestPool.PositiveCheck = doc.DocumentElement.SelectSingleNode("/root/success_fail/check_success").Attributes["value"].Value;
        qds.QuestPool.NegativeCheck = doc.DocumentElement.SelectSingleNode("/root/success_fail/check_fail").Attributes["value"].Value;
    }

    /// <summary>
    /// Helper function to load information from the XML document
    /// </summary>
    /// <param name="doc">XML document.</param>
    /// <param name="values">The list of checks, since it's a list, it's always ref.</param>
    /// <param name="nodeName">Parent node name that contains multiple subnodes.</param>
    /// <param name="subNodeName">subnodes that we're looping through.</param>
    /// <param name="c">The kind of check we're adding.</param>
    private void AddCheck(XmlDocument doc, List<QuestDataStorage.Quest_Check> values, string nodeName, string subNodeName, GeneratorData.NPC_STAT_CHECK c)
    {
        var nodeList = doc.DocumentElement.SelectNodes("/root/" + nodeName + "/" + subNodeName);
        foreach(XmlNode n in nodeList)
        {
            var nQCL = new QuestDataStorage.Quest_Check();
            nQCL.Type = c;
            nQCL.CheckValue = n.Attributes["value"].Value;

            values.Add(nQCL);
        }
    }

    /// <summary>
    /// Loads data for different monsters heroes will encounter in quests. 
    /// It's stored in a different XML for readability.
    /// </summary>
    private void LoadQuestMonsters()
    {
        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/QuestMonster.xml");

        var nodeList = doc.DocumentElement.SelectNodes("/root/monster");
        qds.QuestMonsters = new List<QuestDataStorage.QuestMonster>();

        foreach(XmlNode n in nodeList)
        {
            var nm = new QuestDataStorage.QuestMonster();
            nm.Name = n.SelectSingleNode("name").Attributes["value"].Value;
            nm.Health = int.Parse(n.SelectSingleNode("health").Attributes["value"].Value);
            nm.BaseDamage = int.Parse(n.SelectSingleNode("base_damage").Attributes["value"].Value);
            nm.DamageRange = int.Parse(n.SelectSingleNode("damage_range").Attributes["value"].Value);
            nm.Speed = int.Parse(n.SelectSingleNode("speed").Attributes["value"].Value);
            qds.QuestMonsters.Add(nm);
        }

    }

    private void LoadPersonalities()
    {
        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/NPCPersonalityType.xml");

        var node = doc.DocumentElement.SelectSingleNode("/root/allRanges");
        foreach(XmlAttribute attr in node.Attributes)
        {
            string attrV = attr.Value;
            var vals = attrV.Split(',');
            int upperValue = int.Parse(vals[1]);
            int lowerValue = int.Parse(vals[0]);
            NPCPersonalityStorage.allRanges.Add(new NPCRanges(lowerValue, upperValue));
        }

        var nodes = doc.DocumentElement.SelectNodes("/root/personality");

        int idC = 0;

        foreach(XmlNode n in nodes)
        {
            var newNPCNode = new NPCPersonalityStorageNode();
            newNPCNode.PersonalityId = (PERSONALITY_TYPE)idC;

            newNPCNode.PersonalityType = n.Attributes[0].Value;

            var ranges = n.SelectNodes("range");
            int rCounter = 0;
            int counter = 0;
            foreach (XmlNode range in ranges)
            {
                newNPCNode.percents[rCounter] = int.Parse(range.Attributes[0].Value);

                var displayChild = range.SelectSingleNode("display");
                newNPCNode.rangeDisplayValues.Add(NPCPersonalityStorage.allRanges[counter], displayChild.Attributes[0].Value);
                ++counter;

                var children = range.SelectNodes("greeting");
                foreach (XmlNode c in children)
                {
                    newNPCNode.nodeGreetings.Add(c.Attributes[0].Value);
                }

                ++rCounter;
            }
            newNPCNode.HighRelationReaction = n.SelectSingleNode("relation_high").Attributes[0].Value;
            newNPCNode.MediumRelationReaction = n.SelectSingleNode("relation_middle").Attributes[0].Value;
            newNPCNode.LowRelationReaction = n.SelectSingleNode("relation_low").Attributes[0].Value;
            NPCPersonalityStorage.npcPersonalitynodes.Add(newNPCNode);

            ++idC;
        }

        nodes = doc.DocumentElement.SelectNodes("/root/base_greeting");

        foreach(XmlNode n in nodes)
        {
            NPCPersonalityStorage.npcPossibleGreetings.Add(n.Attributes[0].Value);
        }
        
    }

    public void LoadDialogues()
    {
        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/NPCDialogue.xml");

        LoadTownDialogues(doc);
    }

    private void LoadTownDialogues(XmlDocument doc)
    {
        var node = doc.DocumentElement.SelectSingleNode("/root/town_convos");
        LoadGreetings(node);
    }

    private void LoadGreetings(XmlNode node)
    {
        DialogueDataHolder.townGreetings = new DialogueDataHolder.TownNPCGreeting("NPC1", "NPC2","VERB", "GREETING");
        var greetingParent = node.SelectSingleNode("greetings");
        DialogueDataHolder.townGreetings = (DialogueDataHolder.TownNPCGreeting)DialogueLoad(greetingParent, "greeting_verbs/verb", "greeting_vals/g_value", DialogueDataHolder.townGreetings);
        var convoParent = node.SelectSingleNode("conversations");
        DialogueDataHolder.townConvos = new DialogueDataHolder.TownNPCConversation("NPC1", "NPC2", "VERB", "SUBJECT");
        DialogueDataHolder.townConvos = (DialogueDataHolder.TownNPCConversation)DialogueLoad(convoParent, "convo_verbs/verb", "subjects/subject", DialogueDataHolder.townConvos);
        var reactParent = node.SelectSingleNode("responses");
        DialogueDataHolder.townReactions = new DialogueDataHolder.TownNPCReaction("NPC1", "NPC2", "REACTION", "REACTION");
        DialogueDataHolder.townReactions = LoadReacts(reactParent, "pos_reactions/reaction", "neg_reactions/reaction","neutral_reactions/reaction" ,DialogueDataHolder.townReactions);
    }

    private DialogueDataHolder.DialogueStructure DialogueLoad(XmlNode parent, string verbs_str, 
        string values, DialogueDataHolder.DialogueStructure @struct)
    {
        var structures = parent.SelectNodes("structure");

        foreach (XmlNode n in structures)
        {
            @struct.AddStructure(n.Attributes[0].Value);
        }

        var verbs = parent.SelectNodes(verbs_str);
        foreach (XmlNode n in verbs)
        {
            @struct.AddVerb(n.Attributes[0].Value);
        }

        var subjects = parent.SelectNodes(values);
        foreach (XmlNode n in subjects)
        {
            @struct.AddSubject(n.Attributes[0].Value);
        }

        return @struct;
    }

    private DialogueDataHolder.TownNPCReaction LoadReacts(XmlNode parent, string verbs_str,
        string values, string neutral, DialogueDataHolder.TownNPCReaction @struct)
    {
        var structures = parent.SelectNodes("structure");

        foreach (XmlNode n in structures)
        {
            @struct.AddStructure(n.Attributes[0].Value);
        }

        var verbs = parent.SelectNodes(verbs_str);
        foreach (XmlNode n in verbs)
        {
            @struct.AddVerb(n.Attributes[0].Value);
        }

        var subjects = parent.SelectNodes(values);
        foreach (XmlNode n in subjects)
        {
            @struct.AddSubject(n.Attributes[0].Value);
        }

        var neutrals = parent.SelectNodes(neutral);
        foreach (XmlNode n in neutrals)
        {
            @struct.AddNeutralResp(n.Attributes[0].Value);
        }

        return @struct;
    }

    private void GenerateNPC()
    {
        d.GenerateNewNPC();
    }

}
