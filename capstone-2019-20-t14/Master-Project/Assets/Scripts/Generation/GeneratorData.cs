﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/GeneratorData", order = 1)]
public class GeneratorData : ScriptableObject
{
    public enum NPC_CLASS_ID
    {
        PALADIN,
        FIGHTER,
        MAGE,
        CLERIC,
        ARCHER,
        NUM_CLASSES
    }
    public enum NPC_STAT_ID
    {
        STRENGTH = 0,
        DEXTERITY,
        CONSTITUTION,
        INTELLIGENCE,
        WISDOM,
        NUM_STATS
    }

    public enum NPC_SKILL_ID
    {
        FIGHTING = 0,
        GARDENING,
        ANIMALS,
        TAILORING,
        SMITHING,
        TALKING
    }

    public enum NPC_STAT_CHECK
    {
        STRENGTH = NPC_STAT_ID.STRENGTH,
        DEXTERITY = NPC_STAT_ID.DEXTERITY,
        CONSTITUTION = NPC_STAT_ID.CONSTITUTION,
        INTELLIGENCE = NPC_STAT_ID.INTELLIGENCE,
        WISDOM = NPC_STAT_ID.WISDOM,
        PARTY_MAX_CHECK,
        LEVEL_CHECK,
        NUM_CHECKS
    }

    public class NPC_Skill
    {
        public NPC_Skill(string newSkill)
        {
            Id = newSkill;
        }
        public string Id { get;}
    }

    public class NPC_Stat_Weight
    {
        public NPC_STAT_ID statID { get; set; }
        public float statValue { get; set; }
    }

    public class NPC_Skill_Weight
    {
        public string SkillID { get; set; }
        public float SkillWeightValue { get; set; }
    }
    /// <summary>
    /// Weights used to generate new classes.
    /// WARNING, THIS WILL BE DEPRECIATED AT SOME POINT.
    /// </summary>
    public class NPC_Class_Generator
    {
        public NPC_CLASS_ID id { get; set; }
        public NPC_Stat_Weight strWeight { get; set; }
        public NPC_Stat_Weight dexWeight { get; set; }
        public NPC_Stat_Weight conWeight { get; set; }
        public NPC_Stat_Weight intWeight { get; set; }
        public NPC_Stat_Weight wisWeight { get; set; }
    }

    public class NPC_Skill_Class_Generator
    {
        public string Id { get; set; } // Class ID
        public List<NPC_Skill_Weight> weights = new List<NPC_Skill_Weight>(); // weights for that class.
    }

    /// <summary>
    /// Simple helper class for name values.
    /// </summary>
    public static class NPC_Names
    {
        public static List<string> firstNames { get; set; }
        public static List<string> lastNames { get; set; }

        public static string GetFirstName()
        {
            //return firstNames[Random.Range(0, firstNames.Count)];
            return firstNames[Rand.GetRandom(0, firstNames.Count)];
        }

        public static string GetLastName()
        {
            return lastNames[Rand.GetRandom(0, lastNames.Count)];
        }

        public static void Init()
        {
            firstNames = new List<string>();
            lastNames = new List<string>();
        }
    }

    public List<NPC_Skill> skillMap = new List<NPC_Skill>();
    public List<NPC_Class_Generator> classData = new List<NPC_Class_Generator>();
    public List<NPC_Skill_Class_Generator> skillAndClassData = new List<NPC_Skill_Class_Generator>();
}
