﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PERSONALITY_TYPE
{
    Caution = 0,
    Determination,
    Leadership,
    Bravery,
    Loyalty,
    Empathy
}

public partial class NPCPersonality
{
    public List<PersonalityTrait> personalityTraits = new List<PersonalityTrait>();
    public List<string> greetings = new List<string>();
}

public class PersonalityTrait
{
    public int TraitValue { get; set; }
    public string TraitType { get; set; }
    public string DisplayValue { get; set; }
    public PERSONALITY_TYPE PersonalityID { get; set; }
    public string TheirHighRelationReactionToMe { get; set; }
    public string TheirMediumRelationReactionToMe { get; set; }
    public string TheirLowRelationReactionToMe { get; set; }

    public string GetRelationReaction(int trustValue)
    {
        if (trustValue >= 50)
            return TheirHighRelationReactionToMe;
        else if (trustValue > -10 && trustValue < 50)
            return TheirMediumRelationReactionToMe;
        else
            return TheirLowRelationReactionToMe;
    }
}

public static partial class NPCPersonalityStorage
{
    public static List<NPCPersonalityStorageNode> npcPersonalitynodes = new List<NPCPersonalityStorageNode>();
    public static List<string> npcPossibleGreetings = new List<string>();
}

public partial class NPCPersonalityStorageNode
{
    public string PersonalityType { get; set; }
    public PERSONALITY_TYPE PersonalityId { get; set; }

    public Dictionary<NPCRanges, string> rangeDisplayValues = new Dictionary<NPCRanges, string>();

    public List<string> nodeGreetings = new List<string>();
    public int[] percents = new int[4];

    public string HighRelationReaction { get; set; }
    public string MediumRelationReaction { get; set; }
    public string LowRelationReaction { get; set; }

    public (string, int) FindInDictionary(int numInRange)
    {
        string savedString = "";
        int indexer = 0;
        foreach(KeyValuePair<NPCRanges, string> kvp in rangeDisplayValues)
        {
            if (numInRange >= kvp.Key.LowerValue && numInRange <= kvp.Key.UpperValue)
            {
                savedString = kvp.Value;
                break;
            }
            ++indexer;
        }

        if(savedString == "")
            Debug.LogError("Could not find in the thing!");

        return (savedString, indexer);
    }

    public (int, int) FindInRange(int index)
    {
        --index;
        foreach (KeyValuePair<NPCRanges, string> kvp in rangeDisplayValues)
        {
            if(index <= 0)
            {
                return (kvp.Key.LowerValue, kvp.Key.UpperValue);
            }

            --index;
        }

        return (0, 0);
    }

}

public partial class NPCRanges
{
    public NPCRanges(int lower, int upper)
    {
        LowerValue = lower;
        UpperValue = upper;
    }
    public int LowerValue { get; set; }
    public int UpperValue { get; set; }
    //public int PercentChance { get; set; }
}