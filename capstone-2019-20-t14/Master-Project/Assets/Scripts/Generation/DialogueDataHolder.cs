﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;

public class DialogueDataHolder : ScriptableObject
{
    public class DialogueStructure
    {
        protected List<string> structures = new List<string>();
        protected List<string> verbs = new List<string>();
        protected List<string> subjects = new List<string>();

        protected readonly string npc_tag_1;
        protected readonly string npc_tag_2;
        protected readonly string verb_tag;
        protected readonly string subject_tag;

        protected DialogueStructure(string tag1, string tag2, string vTag, string sTag)
        {
            npc_tag_1 = tag1;
            npc_tag_2 = tag2;
            verb_tag = vTag;
            subject_tag = sTag;
        }

        public void AddStructure(string s)
        {
            structures.Add(s);
        }

        public void AddVerb(string s)
        {
            verbs.Add(s);
        }

        public void AddSubject(string s)
        {
            subjects.Add(s);
        }

        public string GetRandomSubject()
        {
            return subjects[Rand.GetRandom(0, subjects.Count)];
        }

        public string GetRandomVerbs()
        {
            return verbs[Rand.GetRandom(0, verbs.Count)];
        }
    }

    public class TownNPCGreeting : DialogueStructure
    {
        public TownNPCGreeting(string tag1, string tag2, string vTag, string sTag) : base(tag1, tag2, vTag, sTag)
        {

        }

        public string ConstructGreeting(string name1, string name2)
        {
            string structure = structures[Rand.GetRandom(0, structures.Count)];
            structure = structure.Replace(npc_tag_1, name1);
            structure = structure.Replace(npc_tag_2, name2);
            structure = structure.Replace(verb_tag,  verbs[Rand.GetRandom(0, verbs.Count)]);
            structure = structure.Replace(subject_tag, "\"" + subjects[Rand.GetRandom(0, subjects.Count)] + "\"");

            return structure;
        }
    }

    public class TownNPCConversation : DialogueStructure
    {
        public TownNPCConversation(string tag1, string tag2, string vTag, string sTag) : base(tag1, tag2, vTag, sTag)
        {

        }

        public string ConstructConversation(string name1, string name2)
        {
            string structure = structures[Rand.GetRandom(0, structures.Count)];
            structure = structure.Replace(npc_tag_1, name1);
            structure = structure.Replace(npc_tag_2, name2);
            structure = structure.Replace(verb_tag, verbs[Rand.GetRandom(0, verbs.Count)]);
            structure = structure.Replace(subject_tag, subjects[Rand.GetRandom(0, subjects.Count)]);

            return structure;
        }

        public string ConstructConversation(string subject, string name1, string name2)
        {
            string structure = structures[Rand.GetRandom(0, structures.Count)];
            structure = structure.Replace(npc_tag_1, name1);
            structure = structure.Replace(npc_tag_2, name2);
            structure = structure.Replace(verb_tag, verbs[Rand.GetRandom(0, verbs.Count)]);

            structure = structure.Replace(subject_tag, subject);

            return structure;

        }

        public string ConstructConversation(string subject, string verb, string name1, string name2)
        {
            string structure = structures[Rand.GetRandom(0, structures.Count)];
            structure = structure.Replace(npc_tag_1, name1);
            structure = structure.Replace(npc_tag_2, name2);
            structure = structure.Replace(verb_tag, verb);

            structure = structure.Replace(subject_tag, subject);

            return structure;
        }

        public (string, string) ConstructConversation(string[] badSubjs, string name1, string name2)
        {
            string structure = structures[Rand.GetRandom(0, structures.Count)];
            structure = structure.Replace(npc_tag_1, name1);
            structure = structure.Replace(npc_tag_2, name2);
            structure = structure.Replace(verb_tag, verbs[Rand.GetRandom(0, verbs.Count)]);

            string subj;
            do
            {
                subj = subjects[Rand.GetRandom(0, subjects.Count)];
            } while (badSubjs.Contains(subj));

            structure = structure.Replace(subject_tag, subj);

            return (structure, subj);
        }
    }

    public class TownNPCReaction : DialogueStructure
    {
        private List<string> neutralResps = new List<string>();
        // Really stupid, but I can make the positive reactions the "verbs", and the negative reactions the "subjects".
        public TownNPCReaction(string tag1, string tag2, string vTag, string sTag) : base(tag1, tag2, vTag, sTag)
        {

        }

        public void AddNeutralResp(string s)
        {
            neutralResps.Add(s);
        }

        public string ConstructNegativeResponse(string name2)
        {
            string structure = structures[Rand.GetRandom(0, structures.Count)];
            structure = structure.Replace(npc_tag_2, name2);
            structure = structure.Replace(subject_tag, subjects[Rand.GetRandom(0, subjects.Count)]);

            return structure;
        }

        public string ConstructPositiveResponse(string name2)
        {
            string structure = structures[Rand.GetRandom(0, structures.Count)];
            structure = structure.Replace(npc_tag_2, name2);
            structure = structure.Replace(verb_tag, verbs[Rand.GetRandom(0, verbs.Count)]);

            return structure;
        }

        public string ConstructNeutralResponse(string name2)
        {
            string structure = structures[Rand.GetRandom(0, structures.Count)];
            structure = structure.Replace(npc_tag_2, name2);
            structure = structure.Replace(verb_tag, neutralResps[Rand.GetRandom(0, neutralResps.Count)]);

            return structure;
        }
    }



    public static TownNPCGreeting townGreetings;
    public static TownNPCConversation townConvos;
    public static TownNPCReaction townReactions;
}
public class TownNPCDialogue
{
    public NPCDataStorage.Guild_NPC NPC1, NPC2;

    public List<string> conversationParts = new List<string>();
    private int count = 1;

    public TownNPCDialogue(NPCDataStorage.Guild_NPC n1, NPCDataStorage.Guild_NPC n2)
    {
        NPC1 = n1;
        NPC2 = n2;
    }

    public void ConstructConversation()
    {
        // Get the greeting first.
        conversationParts.Add("" + count + ": " + DialogueDataHolder.townGreetings.ConstructGreeting(NPC1.firstName, NPC2.firstName));
        ++count;

        int timesThrough = Rand.GetRandom(5, 10);
        do
        {
            //conversationParts.Add("" + count + ": " + DialogueDataHolder.townConvos.ConstructConversation(NPC1.firstName, NPC2.firstName));
            //++count;

            //// Get a positive or negative response
            //int decision = Rand.GetRandom(0, 2);
            //if (decision < 1)
            //    conversationParts.Add("" + count + ": " + DialogueDataHolder.townReactions.ConstructPositiveResponse(NPC2.firstName));
            //else
            //    conversationParts.Add("" + count + ": " + DialogueDataHolder.townReactions.ConstructNegativeResponse(NPC2.firstName));

            //++count;
            RunConversationAndResponse(NPC1, NPC2);
            --timesThrough;
            SwapNPCS();
        } while (timesThrough > 0);
    }

    private void RunConversationAndResponse(NPCDataStorage.Guild_NPC npc1, NPCDataStorage.Guild_NPC npc2)
    {
        var percentOfFavoriteOrLike = Rand.GetRandom(0, 100);
        if(percentOfFavoriteOrLike > 10)
        {
            //Debug.Log("Doing a favorite");
            var laf = npc1.DialogueOptions.GetLikesAndFavorites();
            string foundSubj = laf[Rand.GetRandom(0, laf.Length)];
            string structure = DialogueDataHolder.townConvos.ConstructConversation(foundSubj, npc1.DialogueOptions.GetFavVerb(), 
                npc1.firstName, npc2.firstName);
            AddConvPart(structure);

            var choice = RunLikeDislike(foundSubj);
            npc1.ModifyRelationship(npc2, choice);
            //if (choice >= 0)
            //    AddConvPart(DialogueDataHolder.townReactions.ConstructPositiveResponse(npc2.firstName));
            //else
            //    AddConvPart(DialogueDataHolder.townReactions.ConstructNegativeResponse(npc2.firstName));

            AddConvPart(RunReaction(choice, npc2.firstName));
        }
        else
        {
            //Debug.Log("not doing a favorite");
            var dah = npc1.DialogueOptions.GetDislikesAndHates();
            var struc_subj = DialogueDataHolder.townConvos.ConstructConversation(dah, npc1.firstName, npc2.firstName);
            AddConvPart(struc_subj.Item1);

            var choice = RunLikeDislike(struc_subj.Item2);
            npc1.ModifyRelationship(npc2, choice);
            AddConvPart(RunReaction(choice, npc2.firstName));
            //if (choice >= 0)
            //    AddConvPart(DialogueDataHolder.townReactions.ConstructPositiveResponse(npc2.firstName));
            //else
            //    AddConvPart(DialogueDataHolder.townReactions.ConstructNegativeResponse(npc2.firstName));
        }
    }

    private void AddConvPart(string newPart)
    {
        conversationParts.Add("" + count + ": " + newPart);
        ++count;
    }

    private string RunReaction(int choice, string npc2name)
    {
        if (choice == 0)
            return DialogueDataHolder.townReactions.ConstructNeutralResponse(npc2name);
        else if (choice > 0)
            return DialogueDataHolder.townReactions.ConstructPositiveResponse(npc2name);
        else
            return DialogueDataHolder.townReactions.ConstructNegativeResponse(npc2name);
    }

    private int RunLikeDislike(string subj)
    {
        if (NPC2.DialogueOptions.favoriteOptions.Contains(subj))
            return 3;
        else if (NPC2.DialogueOptions.likeOptions.Contains(subj))
            return 2;
        else if (NPC2.DialogueOptions.dislikeOptions.Contains(subj))
            return -1;
        else if (NPC2.DialogueOptions.hateOptions.Contains(subj))
            return -2;

        return 0;
    }

    private void SwapNPCS()
    {
        var swapper = NPC1;
        NPC1 = NPC2;
        NPC2 = swapper;
    }

    public void DebugConversation()
    {
        for(int i = 0; i < conversationParts.Count; ++i)
        {
            Debug.Log(conversationParts[i]);
        }
    }
}