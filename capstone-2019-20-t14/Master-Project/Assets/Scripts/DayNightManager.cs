﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DayNightManager : MonoBehaviour
{
    public GameObject mainDirectionalLight;
    Transform mdl_Trans;

    List<GameObject> nightLightList;
    List<Light> fadeLightsList;

    [Header("Sun Settings")]
    [Tooltip("Length of day in minutes")]
    public float lengthOfDay = 5;
    float movementPerTick = 0;
    bool rotateLightObj = true;
    public float morningXRotation;
    public float nightXRotation;
    float timeTick = 0;

    [Header("Night Light Settings")]
    public float fadeSpeed = 2f;
    public float nightLightIntensityTarget;
    [Tooltip("Distance from end goal when the lights turn on")]
    public float nightTurnOnTime = 50f;
    float intensityTargetBuffer = .1f;    
    bool fadeUpLights = false;

    //y, z should always be at -25, 0
    float recordedX = 0;//needed because Euler Angles don't calculate after 90
    float yRot = -25;
    float zRot = 0;

    // Start is called before the first frame update
    void Start()
    {
        nightLightList = new List<GameObject>();
        fadeLightsList = new List<Light>();

        mdl_Trans = mainDirectionalLight.transform;
        recordedX = morningXRotation;
        mdl_Trans.eulerAngles = new Vector3(recordedX, yRot, zRot);

        lengthOfDay *= 60;

        movementPerTick = (nightXRotation - morningXRotation) / lengthOfDay;

        timeTick = recordedX;

        GetAllNightLights();

        TurnOffNightObjects();

        RotateSun();
    }

    // Update is called once per frame
    void Update()
    {
        if (rotateLightObj)
            RotateSun();

        if (fadeUpLights)
        {
            FadeUpLightObjects();   
        }
    }

    void RotateSun()
    {
        timeTick += movementPerTick * Time.deltaTime;

        Vector3 newRot = new Vector3(timeTick, yRot, zRot);

        mdl_Trans.eulerAngles = newRot;

        if ((recordedX + movementPerTick) - timeTick < .01f)
        {
            recordedX += movementPerTick;
            timeTick = recordedX;

            if (nightXRotation - recordedX <= nightTurnOnTime)
            {
                foreach (GameObject go in nightLightList)
                {
                    go.SetActive(true);
                }

                fadeUpLights = true;
            }
        }

        if (nightXRotation - recordedX <= 1f)
            rotateLightObj = false;
    }

    void FadeUpLightObjects()
    {
        foreach(Light li in fadeLightsList)
        {
            li.intensity = Mathf.Lerp(li.intensity, nightLightIntensityTarget, Time.deltaTime / fadeSpeed);

            if (nightLightIntensityTarget - li.intensity < intensityTargetBuffer)
                fadeUpLights = false;
        }
    }

    void TurnOffNightObjects()
    {
        foreach (Light li in fadeLightsList)
        {
            li.intensity = 0;
        }

        foreach (GameObject go in nightLightList)
        {
            go.SetActive(false);
        }

        fadeUpLights = false;        
    }

    void GetAllNightLights()
    {
        nightLightList.Clear();

        GameObject[] lightArray = GameObject.FindGameObjectsWithTag("TurnOnAtNight");

        foreach(GameObject go in lightArray)
        {
            nightLightList.Add(go);
            go.SetActive(false);

            if (go.GetComponent<Light>())
            {
                fadeLightsList.Add(go.GetComponent<Light>());
                go.GetComponent<Light>().intensity = 0;
            }
        }
    }

    public void NewDay()
    {
        TurnOffNightObjects();

        recordedX = morningXRotation;
        mdl_Trans.eulerAngles = new Vector3(recordedX, yRot, zRot);

        timeTick = recordedX;

        rotateLightObj = true;
        fadeUpLights = false;
    }
}
