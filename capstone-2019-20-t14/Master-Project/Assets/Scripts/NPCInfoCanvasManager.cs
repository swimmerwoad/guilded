﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class NPCInfoCanvasManager : MonoBehaviour
{

    public GameObject NPCInfoCanvas;
    public TMP_Text NPCNameText;
    public GameObject NPCStatNamePanel;
    public GameObject NPCStatSliderPanel;
    private bool isPanelOpen = false;
    private TownNPC selectedNPC; //the npc whose info is shown in the panel
    private TownNPC opinionNPC; //when an npc is active in the panel, they talk about THIS NPC
    private void Start()
    {
        NPCInfoCanvas.SetActive(false);
    }

    public void InitPanel(string charName, List<NPCDataStorage.NPC_Skill> skills)
    {
        NPCInfoCanvas.SetActive(true);

        NPCNameText.text = charName;

        int i = 0;

        foreach(NPCDataStorage.NPC_Skill skill in skills)
        {
            NPCStatNamePanel.transform.GetChild(i).GetComponent<TMP_Text>().text = skill.DisplayName;
            NPCStatSliderPanel.transform.GetChild(i).GetComponent<Slider>().value = skill.CurrentLevel;
            i++;
        }
    }

    private void Update()
    {

        if(Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            Debug.DrawRay(ray.origin, ray.direction * 5000000.0f, Color.cyan);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.gameObject.GetComponent<TownNPC>() != null)
                {
                    Debug.Log("Clicked on someone.");
                }
            }
        }
        if (Input.GetMouseButtonDown(0))
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                Debug.DrawRay(ray.origin, ray.direction * 5000000.0f, Color.cyan);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    if (hit.collider.gameObject.GetComponent<TownNPC>() != null)
                    {
                        TownNPC hitNPC = hit.collider.gameObject.GetComponent<TownNPC>();
                        Debug.Log("clicked on " + hitNPC.thisNPC.firstName + " " + hitNPC.thisNPC.lastName);

                        if (!isPanelOpen)
                        {
                            InitPanel(hitNPC.thisNPC.firstName + " " + hitNPC.thisNPC.lastName, hitNPC.thisNPC.statData.OutputSkillData());
                            selectedNPC = hitNPC;
                        
                            isPanelOpen = true;
                        }
                        else
                        {
                        //SHOW NPC RELATIONSHIP BLURB
                        opinionNPC = hitNPC;

                        if(selectedNPC != opinionNPC)
                        {
                            Debug.Log(selectedNPC.thisNPC.firstName + " has an opinion of " + opinionNPC.thisNPC.firstName);
                        }
                      
                        }
                    }

                }

            }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ClosePanel();
        }
        
        
    }
    public void ClosePanel()
    {
        NPCInfoCanvas.SetActive(false);
        isPanelOpen = false;
    }
}
