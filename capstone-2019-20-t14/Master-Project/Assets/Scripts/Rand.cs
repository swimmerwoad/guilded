﻿using System.Collections;
using System.Collections.Generic;


public static class Rand
{
    public static System.Random rand;

    // https://stackoverflow.com/questions/1785744/how-do-i-seed-a-random-class-to-avoid-getting-duplicate-random-values
    public static void SeedRandom()
    {
        rand = new System.Random(System.Guid.NewGuid().GetHashCode());
    }

    public static int GetRandom(int min, int max)
    {
        // Within min, up to, not including max.
        if (rand == null)
            SeedRandom();
        return rand.Next(min, max);
    }

    public static double GetDouble()
    {
        if (rand == null)
            SeedRandom();
        return rand.NextDouble();
    }

    public static float GetFloat()
    {
        if (rand == null)
            SeedRandom();
        return (float)rand.NextDouble();
    }

    public static float GetFloatRange(float min, float max)
    {
        if (rand == null)
            SeedRandom();
        return (float)rand.NextDouble() * (max - min) + min;
    }
}
