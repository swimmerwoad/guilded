﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugNPCMove : MonoBehaviour
{
    public bool forward;

    public bool wantsToTalk = true;
    public float talkCd = 10.0f;
    private DiageticUIManager man;

    private void Start()
    {
        man = FindObjectOfType<DiageticUIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(forward ? Vector3.forward * 0.1f : Vector3.forward * -0.1f);
    }

    public void DoDialogue(NPCDataStorage.Guild_NPC npc)
    {
        var dialogue = new TownNPCDialogue(GetComponent<TownNPC>().getNPC(), npc);
        dialogue.ConstructConversation();
        man.GenerateDialogueButton(dialogue, transform.position);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "NPC")
        {
            var otherNPC = other.gameObject.GetComponent<TownNPC>().getNPC();
            if (wantsToTalk && other.gameObject.GetComponent<DebugNPCMove>().wantsToTalk)
            {
                wantsToTalk = false;
                other.gameObject.GetComponent<DebugNPCMove>().wantsToTalk = false;
                DoDialogue(otherNPC);
                StartCoroutine(TalkCooldown(talkCd));
                other.gameObject.GetComponent<DebugNPCMove>().StartCoroutine(TalkCooldown(talkCd));
            }
        }
    }

    public IEnumerator TalkCooldown(float t)
    {
        yield return new WaitForSeconds(t);
        wantsToTalk = true;
    }
}
