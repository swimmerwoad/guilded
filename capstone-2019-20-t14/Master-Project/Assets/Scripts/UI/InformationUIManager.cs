﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using TMPro;
// TODO: make a cursor for the UI that can be controlled by both cursor & mouse.
public class InformationUIManager : MonoBehaviour
{
    public enum PanelID
    {
        NPC_DATA,
        CURRENT_QUEST,
        QUEST_ACTION,
        ANGRY_PANEL
    }

    public GameObject nPCDataPanel, currentQuestPanel, questActionPanel, angryPanel; // Put all the panels here.
    public float fadeSpeed; // How fast or slow something fades.
    [Header("Prefabs")]
    public GameObject registryButton; // Anything that must be created by the manager.
    [Header("Scriptable Objects & others")]
    public NPCDataStorage npcStorage; // SO that has NPC data.
    public TownQuestManager townManager; // Town manager thing that interfaces a lot of stuff.
    public GameObject nextDayButton; // Because I'm an idiot. Unsure how to fix this at the current moment.
    public GameObject embarkButton; // so we can disable the embark button when there isn't any quest available.
    public Calendar calendarScript; // Used for the next day reset

    private void Update()
    {
        if (embarkButton.activeInHierarchy)
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                RunQuest();
            }
        }


        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (currentQuestPanel.activeInHierarchy)
            {
                CloseQuest();
            }
            else if (questActionPanel.activeInHierarchy)
            {
                questActionPanel.GetComponent<QuestActionDisplayControl>().Close();
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (currentQuestPanel.activeInHierarchy)
            {
                currentQuestPanel.SetActive(false);
                
            }
            else if (!currentQuestPanel.activeInHierarchy)
            {
                currentQuestPanel.SetActive(true);
            }
        }
    }


    /// <summary>
    /// Structure to store some stuff for each panel, as well as some helper & extension functionality.
    /// </summary>
    class UIPanel
    {
        //public PanelID id;
        public GameObject obj; // The actual panel.
        Image i; // The image of the panel.
        InformationUIManager daddy; // A monobehaviour so that we can run coroutines.

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="o">The panel object to pass in.</param>
        /// <param name="m">monobehaviour to pass in.</param>
        public UIPanel(GameObject o, InformationUIManager m)
        {
            //id = i;
            obj = o;
            i = obj.GetComponent<Image>();
            daddy = m;
        }

        /// <summary>
        /// Initialize the object by fading it down & setting it false.
        /// </summary>
        public void Init()
        {
            i.canvasRenderer.SetAlpha(0.0f);
            obj.SetActive(false);
        }

        /// <summary>
        /// Fade up the panel image from 0 alpha to maximum alpha with a certain speed.
        /// </summary>
        /// <param name="fadeSpeed">How quickly the fade happens.</param>
        public void FadeUpPanel(float fadeSpeed)
        {
            obj.SetActive(true);
            i.canvasRenderer.SetAlpha(0.0f);
            i.CrossFadeAlpha(1.0f, fadeSpeed, true);
        }

        /// <summary>
        /// Fades down the panel by interpolating from maximum alpha (1.0) to zero (0.0)
        /// </summary>
        /// <param name="fadeSpeed">How quickly the panel fades.</param>
        public void FadeDownPanel(float fadeSpeed)
        {
            obj.SetActive(true);
            i.canvasRenderer.SetAlpha(1.0f);
            i.CrossFadeAlpha(0.0f, fadeSpeed, true);
            daddy.StartCoroutine(WaitInFade(fadeSpeed));
        }

        /// <summary>
        /// Enumerator to wait on setting things false until the fade is completed.
        /// </summary>
        /// <param name="t">How long to wait (in seconds)</param>
        /// <returns>Nothing.</returns>
        private IEnumerator WaitInFade(float t)
        {
            yield return new WaitForSeconds(t);
            obj.SetActive(false);
        }
    }

    private Dictionary<PanelID, UIPanel> panels = new Dictionary<PanelID, UIPanel>(); // Dictionary to quickly access panel things.

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    /// <summary>
    /// Initialize the UI by adding it to the dictionary, and then calling each panel's Init function.
    /// </summary>
    private void Init()
    {
        //panels.Add(new UIPanel(PanelID.CURRENT_QUEST, currentQuestPanel));
        //panels.Add(new UIPanel(PanelID.NPC_DATA, nPCDataPanel));
        //panels.Add(new UIPanel(PanelID.QUEST_ACTION, questActionPanel));

        panels.Add(PanelID.CURRENT_QUEST, new UIPanel(currentQuestPanel, this));
        panels.Add(PanelID.NPC_DATA, new UIPanel(nPCDataPanel, this));
        panels.Add(PanelID.QUEST_ACTION, new UIPanel(questActionPanel, this));
        panels.Add(PanelID.ANGRY_PANEL, new UIPanel(angryPanel, this));

        foreach (KeyValuePair<PanelID, UIPanel> pair in panels)
        {
            pair.Value.Init();
        }
    }

    /// <summary>
    /// Activate quest UI and functionality.
    /// </summary>
    public void ShowQuest()
    {
        var panel = panels[PanelID.CURRENT_QUEST];
        panel.FadeUpPanel(fadeSpeed);

        if (townManager.qds.CurQuest != null)
        {
            panel.obj.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = townManager.GetQuestTitle();

            Transform grid = panel.obj.transform.Find("RosterHolder").GetChild(0);

            foreach (var npc in npcStorage.listOfNPCs)
            {
                GameObject b = Instantiate(registryButton);
                b.transform.SetParent(grid.transform);
                b.transform.GetChild(0).gameObject.GetComponent<TMPro.TMP_Text>().text = npc.DisplayShortData();
                b.GetComponent<ButtonScript>().ResetToNormal();
            }
            embarkButton.SetActive(true);
        }
        else
        {
            panel.obj.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = "No quests logged.";
            embarkButton.SetActive(false);
        }
            
    }
    
    

    /// <summary>
    /// Close quest UI.
    /// </summary>
    public void CloseQuest()
    {
        var panel = panels[PanelID.CURRENT_QUEST];
        panel.FadeDownPanel(fadeSpeed);

        Transform grid = panel.obj.transform.Find("RosterHolder").GetChild(0);

        foreach (Transform c in grid)
            Destroy(c.gameObject);

        townManager.ClearNPCs();
    }

    /// <summary>
    /// I need something else that can close quest, but not fuck it up.
    /// </summary>
    public void CloseQuestAlt()
    {
        var panel = panels[PanelID.CURRENT_QUEST];
        Transform grid = panel.obj.transform.Find("RosterHolder").GetChild(0);

        foreach (Transform c in grid)
            Destroy(c.gameObject);

        townManager.ClearNPCs();
    }

    /// <summary>
    /// Display the stats of the NPC when you click on them in the world.
    /// </summary>
    /// <param name="newStats">The stats value it displays. (Gotten from the town NPC script)</param>
    public void DisplayNPCStats(string newStats, string greeting)
    {
        var panel = panels[PanelID.NPC_DATA];
        panel.FadeUpPanel(fadeSpeed);

        TMPro.TMP_Text t = panel.obj.transform.GetChild(0).gameObject.GetComponent<TMPro.TMP_Text>();
        t.text = newStats;

        TownNPCGreeting(greeting, panel);
    }

    /// <summary>
    /// Close the stats window.
    /// </summary>
    public void CloseNPCStats()
    {
        var panel = panels[PanelID.NPC_DATA];
        panel.FadeDownPanel(fadeSpeed);
    }

    /// <summary>
    /// Run the quest when embark is hit.
    /// </summary>
    public void RunQuest()
    {
        townManager.RunQuest();
        nextDayButton.SetActive(true);
    }

    /// <summary>
    /// Activate the Next day button.
    /// </summary>
    public void NextDayButtonClick()
    {
        // I think I'll want a general "Next day" structure or something in the quest manager to run things.
        townManager.GenerateQuest(); // DEBUG. THIS MUST BE MOVED ELSEWHERE. Generates the next quest.
        // Make sure to get only the quest feedback panel up.
        panels[PanelID.CURRENT_QUEST].FadeDownPanel(fadeSpeed);
        panels[PanelID.NPC_DATA].FadeDownPanel(fadeSpeed);
        panels[PanelID.QUEST_ACTION].FadeUpPanel(fadeSpeed);

        CloseQuestAlt();

        // We'll need to get the actions of what happened on the quest. StringBuilder is better than string.
        StringBuilder b = new StringBuilder();

        // Get the things that happened on the quest.
        var l = townManager.GetActions();
        for (int i = 0; i < l.Count; ++i)
            b.AppendLine((i + 1) + ": " + l[i]);

        panels[PanelID.QUEST_ACTION].obj.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = b.ToString(); // output.
        nextDayButton.SetActive(false);

        calendarScript.GoNextDay();
    }

    private void TownNPCGreeting(string s, UIPanel panel)
    {
        //var panel = panels[PanelID.NPC_DATA];
        //panel.FadeUpPanel(fadeSpeed);
        TMPro.TMP_Text t = panel.obj.transform.GetChild(2).gameObject.GetComponent<TMPro.TMP_Text>();
        t.text = s;

    }

    public void SetAngryUI(string s)
    {
        var instRef = panels[PanelID.ANGRY_PANEL];
        instRef.FadeUpPanel(0.1f);
        instRef.obj.transform.GetChild(0).GetComponent<TMP_Text>().text = s;
        StartCoroutine(WaitToFadeDown(0.8f, instRef));
    }

    private IEnumerator WaitToFadeDown(float t, UIPanel p)
    {
        yield return new WaitForSeconds(t);
        p.FadeDownPanel(t);
    }
}
