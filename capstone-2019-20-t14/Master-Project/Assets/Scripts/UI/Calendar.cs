﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Calendar : MonoBehaviour
{
    [System.Serializable]
    public struct Month
    {
        public string name;
        public int days;
    }

    [Header("Month and Season Info")]
    public List<Month> calendarYear;
    public List<string> seasons;

    [Header("Dependencies")]
    public DayNightManager dnM;
    public TextMeshProUGUI calendarText;

    float seasonSplit = 0;
    int currentDay = 0;
    int daysInYear = 0;

    private void Start()
    {
        foreach (Month mo in calendarYear)
            daysInYear += mo.days;

        seasonSplit = (int)(daysInYear / seasons.Count);

        currentDay = 1;

        calendarText.text = GetDayAndMonth() + "\n" + GetSeason();

        ////

        if (!dnM)
            dnM = GameObject.Find("DayNightManager").GetComponent<DayNightManager>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GoNextDay();
        }
    }

    public string GetCurrentMonth()
    {
        string temp = "";

        return temp;
    }

    public void GoNextDay()
    {
        currentDay++;
        calendarText.text = GetDayAndMonth() + "\n" + GetSeason();
        dnM.NewDay();
    }

    public string GetDayAndMonth()
    {
        string temp = "Error";
        int dayOn = currentDay;
        bool found = false;

        while (dayOn > daysInYear)
            dayOn -= daysInYear;

        for (int i = 0; i < calendarYear.Count; i++)
        {
            if (!found)
            {
                if (dayOn <= calendarYear[i].days)
                {
                    temp = calendarYear[i].name + " " + dayOn;
                    found = true;
                }
                else
                    dayOn -= calendarYear[i].days;
            }
        }
        
        //Add sufix
        int tempDay = dayOn;

        while (tempDay > 100)
            tempDay -= 100;

        if ((tempDay >= 4 && tempDay <= 20))
            temp += "th";
        else
        {
            string dayString = tempDay.ToString();

            char[] charArr = dayString.ToCharArray();

            if (charArr.Length == 1)
            {
                if (tempDay == 1)
                    temp += "st";

                else if (tempDay == 2)
                    temp += "nd";

                else if (tempDay == 3)
                    temp += "rd";
            }
            else
            {
                if (charArr[charArr.Length - 1] == '1')
                    temp += "st";

                else if (charArr[charArr.Length - 1] == '2')
                    temp += "nd";

                else if (charArr[charArr.Length - 1] == '3')
                    temp += "rd";
                else
                    temp += "th";
            }
        }


        return temp;
    }

    public string GetSeason()
    {
        string temp = "Error";
        int dayOn = currentDay;

        while (dayOn > daysInYear)
            dayOn -= daysInYear;

        for (int i = 0; i < seasons.Count; i++)
        {
            if (dayOn <= seasonSplit)
            {
                temp = seasons[i];
                return temp;
            }
            else
                dayOn -= (int)seasonSplit;
        }

        return temp;
    }
}
