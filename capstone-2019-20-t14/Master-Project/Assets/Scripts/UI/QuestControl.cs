﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestControl : MonoBehaviour
{
    public GameObject gridRosterHolder;
    private Image image;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        gameObject.SetActive(false);    
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Close()
    {
        image.canvasRenderer.SetAlpha(1.0f);
        //image.CrossFadeAlpha(0.0f, 0.3f, true);
        //for(int i = 0; i < gridRosterHolder.transform.GetChild(0).childCount; ++i)
        //{
        //    Destroy(gridRosterHolder.transform.GetChild(0).GetChild(0).gameObject);
        //}
        GameObject gridHolder = gridRosterHolder.transform.GetChild(0).gameObject;
        foreach(Transform c in gridHolder.transform)
        {
            Destroy(c.gameObject);
        }
        StartCoroutine(WaitAndFade(0.3f));
    }

    private IEnumerator WaitAndFade(float t)
    {
        image.CrossFadeAlpha(0.0f, t, true);
        yield return new WaitForSeconds(t);

        gameObject.SetActive(false);
    }
}
