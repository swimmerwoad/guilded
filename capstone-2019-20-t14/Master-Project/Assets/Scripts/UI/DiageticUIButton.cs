﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiageticUIButton : MonoBehaviour
{
    private DiageticUIManager mngr;
    public DiageticUIManager Manager { set { mngr = value; } }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
        mngr.RemoveFromList(this);
    }
}
