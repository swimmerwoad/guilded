﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DiageticUIManager : MonoBehaviour
{
    public GameObject bubblePrefab;
    public GameObject textPrefab;
    //public List<TownNPCDialogue> dialogues;

    public GameObject dialogueMenu;

    public Transform scrollContent;

    public GameObject textObj;
    public float timeToDestroy = 4.0f;

    class DialogueAndButton
    {
        public Button buttRef;
        public TownNPCDialogue dialogue;
        public Vector3 worldLoc;
    }

    List<DialogueAndButton> q = new List<DialogueAndButton>();

    // Start is called before the first frame update
    void Start()
    {
        scrollContent = FuncLib.FindInAll(transform, "ScrollContent");        
    }

    // Update is called once per frame
    void Update()
    {
        foreach(DialogueAndButton dab in q)
        {
            dab.buttRef.transform.position = Camera.main.WorldToScreenPoint(dab.worldLoc);
        }
    }

    public void GenerateDialogueButton(TownNPCDialogue d, Vector3 location)
    {
        var newButton = Instantiate(bubblePrefab);
        newButton.transform.SetParent(transform);

        newButton.transform.position = Camera.main.WorldToScreenPoint(location);
        newButton.AddComponent<DiageticUIButton>();
        newButton.GetComponent<DiageticUIButton>().Manager = this;
        var newPair = new DialogueAndButton();
        newPair.buttRef = newButton.GetComponent<Button>();
        newPair.dialogue = d;
        newPair.worldLoc = location;

        newButton.GetComponent<Button>().onClick.AddListener(() => BringUpDialogueMenu(d, newButton));
        newButton.GetComponent<Image>().CrossFadeAlpha(0.0f, timeToDestroy, true);
        q.Add(newPair);
        Destroy(newButton, timeToDestroy);
    }

    public void BringUpDialogueMenu(TownNPCDialogue d, GameObject @ref)
    {
        dialogueMenu.SetActive(true);
        foreach(var t in d.conversationParts)
        {
            /*
            var tp = Instantiate(textPrefab);
            tp.transform.SetParent(scrollContent);
            tp.GetComponent<Text>().text = t;
            */
            textObj.GetComponent<Text>().text += t + "\n";
        }

        //q.Remove()
        //var item = q.Find(x => x.buttRef.Equals( @ref));
        var item = q.Find(x => ReferenceEquals(x.buttRef.gameObject, @ref));
        if (item != null)
            q.Remove(item);

        DestroyImmediate(@ref);
    }

    public void BringDownDialogueMenu()
    {
        dialogueMenu.SetActive(false);

        Debug.Log("Calling bring down menu.");

        //while(scrollContent.childCount > 0)
        //{
        //    var child = scrollContent.GetChild(0).gameObject;
        //    child.GetComponent<Text>().text = "";
        //}

        var child = scrollContent.GetChild(0).gameObject;
        child.GetComponent<Text>().text = "";

    }

    public void RemoveFromList(DiageticUIButton b)
    {
        var item = q.Find(x => ReferenceEquals(x.buttRef.gameObject, b.gameObject));
        if (item != null)
            q.Remove(item);
    }
}
