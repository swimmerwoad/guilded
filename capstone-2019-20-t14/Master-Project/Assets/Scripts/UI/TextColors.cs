﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextColors : MonoBehaviour
{
    
    public List<ColorAndName> ColorMap;

    [System.Serializable]
    public struct ColorAndName
    {
        public string name;
        public Color col;
    }
}
