*******************
***EZPZ MultiTag***
*******************
A EZPZ Tool by Zach Phillips

Thank you for downloading the EZPZ MultiTag tool! I hope that this can help you do exactly what you want. If you want to see a feature or report a bug, please email ezpztools@gmail.com.

Table of Contents:
	How To Use
		One Item
		MultiObject Editing
		EZPZMultiTagMaster
	Example Scenes
		TagExampleScene
		SearchExampleScene
	Please Note
	Functions
		MultiTag
		MultiTagSearchMaster

How To Use
---------------------------
One Item:
1) Tag the item you want to add EZPZ MultiTag to with "MultiTag"
2) Add Component -> MultiTag
3) Use the + and - buttons to add items to the list (One at a time)
4) Click on the drop down menu to select a tag
5) Drag on the left side of each item to reorder it. First priority is the highest item

MultiObject Editing:
1) Select multiple objects
2) If all the selected objects are tagged with "MultiTag" and have the MultiTag component, then a list of all the common tags appears
3) Use the + and - buttons to add items to the list (One at a time)
4) Click on the drop down menu to select a tag
5) You can reorder the tags, but this is discouraged since the selected objects might not have the same tags or amount of tags, leading to messy lists once you return to single object editing
6) Click on a single object in the hierarchy, clearing the MultiObject editing stack and ensuring that your changes are saved. This will cause your selected object to jump around between the objects that were selected. This is normal and required to save objects that are prefabs.

EZPZMultiTagMaster:
This prefab deals with all objects that have the MultiTag tag. Its scripting is shown off in the SearchExampleScene, and you can read about all the available functions in Functions.
1) Add this prefab to your scene
2) Use the Global Tag Priority button to change the priority of a tag for all MultiTag'ed GameObjects that have the selected tag. Priority 0 would change it to the highest priority. A higher priority than the MultiTag list allows would default to the last position.

Scenes
---------------------------
TagExampleScene:
This scene has three objects, each tagged with two unique tags. Play the scene and add, remove, and change tags to see them update live on the canvas.

SearchExampleScene:
This scene shows off the speed and ease that EZPZ MultiTag provides. Play the scene and use the UI buttons to search for the different objects. There are three armies, each with their own color, and three units; Knights, Swordsmen, and Archers. There is also the EZPZMultiTagMaster in this scene. Use this in combination with MultiObject Editing to get a good feel for how this tool works. 

Please Note
---------------------------
- To improve understanding of your actions when using this tool, especially when using MultiObject editing, Undo and Redo functionality have been disabled for MultiTag items.
- As mentioned in the MultiObject Editing section, after deselecting multiple MultiTag objects your selected object will jump around between the objects that were selected. This is normal and required to save objects that are prefabs.

Functions
---------------------------
MultiTag:
bool ContainsTag(string tagToFind)
- Returns true if the tag list contains the searched tag

string[] GetTagsAsArray()
- Returns the list of tags as a string array, sorted in terms of priority with 0 being highest

List<string> GetTagsAsList()
- Returns the list of tags as a string list, sorted in terms of priority with 0 being highest

string GetTagAt(int index)
- Returns the string value of the tag at the selected index. Returns "-1" if index is out of range

int GetTagIndex(string tagToFind)
- Returns the index of a tag. Returns -1 if tag isn't found.

int GetTagPriority(string tagToFind)
- Same function as GetTagIndex, just reowrded. Returns the priority of a tag. Returns -1 if tag isn't found.

int GetTagCount()
- Retuns the amount of tags in the list.

void AddTag(string tag)
- Add a tag to the end of the list.

void AddTag(string tag, int index)
- Add a tag to a specified priority index. If index is larger than the list count it will be added to the end. 0 is the first index.

bool RemoveTag(string tag)
- Removes a tag from the list by string value. Returns false if tag doesn't exist.

bool RemoveTagAt(int index)
- Removes a tag from the list at the specified index. Returns false if index is out of bounds.

bool ChangeTagPriority(string tag, int index)
- Change the priority of a tag. Returns false if index is out of bounds or tag doesn't exist.

bool ChangeTagPriorityToFirst(string tag)
- Change the priority of a tag to the first index. Returns false if index is out of bounds or tag doesn't exist.

bool ChangeTagPriorityToLast(string tag)
- Change the priority of a tag to the last index. Returns false if index is out of bounds or tag doesn't exist.

bool ChangeTagPriority(int searchIndex, int newIndex)
- Change the priority of a tag. Returns false if index is out of bounds, tag doesn't exist, searchIndex and newIndex are either one apart (and not the last index), or searchIndex.

MultiTagSearchMaster:
void RefreshMultiTagGameObjectList()
- If an object is given the MultiTag tag through code, you should call this in order to have it be included in searches.

List<GameObject> GetAllGameObjectsWithTag(string tagToSearch)
- Returns all the GameObjects in a list that contain the searched for tag. Returns null if none exist.

List<string> GetAllCommonTags(GameObject[] gameObjectArray)
- Returns a string list of all the common tags in a group of GameObjects.

List<string> GetAllCommonTags(List<GameObject> gameObjectList)
- Returns a string list of all the common tags in a group of GameObjects

List<string> GetAllCommonTags(MultiTag[] MultiTagArray)
- Returns a string list of all the common tags in a group of GameObjects

List<string> GetAllCommonTags(List<MultiTag> MultiTagList)
- Returns a string list of all the common tags in a group of GameObjects

void SetGlobalPriority(string tagToSet, int priority)
- Sets the priority of a tag to the selected priority for every object tagged with MultiTag.

Copyright 2019, Zach Phillips, all rights reserved
