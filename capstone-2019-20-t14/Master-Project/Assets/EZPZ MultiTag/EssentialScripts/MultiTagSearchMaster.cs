﻿//Copyright 2019, Zach Phillips, all rights reserved
//Created as part of the EZPZ MultiTag Unity Asset
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiTagSearchMaster : MonoBehaviour
{
    List<GameObject> gameObjectsTaggedWithMultiTag = new List<GameObject>();

    private void Awake()
    {
        RefreshMultiTagGameObjectList();
    }

    /// <summary>
    /// If an object is given the MultiTag tag through code, you should call this in order to have it be included in searches.
    /// </summary>
    [ExecuteInEditMode]
    public void RefreshMultiTagGameObjectList()
    {
        gameObjectsTaggedWithMultiTag.Clear();

        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("MultiTag"))
        {
            gameObjectsTaggedWithMultiTag.Add(obj);
        }
    }

    [ExecuteInEditMode]
    private void RefreshIfNeeded()
    {
        if (gameObjectsTaggedWithMultiTag == null || gameObjectsTaggedWithMultiTag.Count == 0)
            RefreshMultiTagGameObjectList();
    }

    /// <summary>
    /// Returns all the GameObjects in a list that contain the searched for tag.
    /// </summary>
    /// <param name="tagToSearch"></param>
    /// <returns></returns>
    public List<GameObject> GetAllGameObjectsWithTag(string tagToSearch)
    {
        List<GameObject> tempList = new List<GameObject>();
        MultiTag tempMT;

        RefreshIfNeeded();
        if (gameObjectsTaggedWithMultiTag != null)
        {
            foreach (GameObject obj in gameObjectsTaggedWithMultiTag)
            {
                tempMT = obj.GetComponent<MultiTag>();
                //Make sure the object has a MultiTagScript on it
                if (tempMT)
                {
                    if (tempMT.ContainsTag(tagToSearch))
                    {
                        tempList.Add(obj);
                    }
                }
            }
        }

        return tempList;
    }

    /// <summary>
    /// Returns a string list of all the common tags in a group of GameObjects.
    /// </summary>
    /// <param name="gameObjectArray">The GameObject array to search</param>
    /// <returns></returns>
    public List<string> GetAllCommonTags(GameObject[] gameObjectArray)
    {
        List<string> tempList = new List<string>();

        if (gameObjectArray.Length > 0)
        {
            MultiTag tempMT = gameObjectArray[0].GetComponent<MultiTag>();

            //Can't use a dictionary because I can't iterate through the Keys :(
            string[,] safeTagList = new string[1, 1];
            List<string> list = new List<string>();

            RefreshIfNeeded();
            if (gameObjectsTaggedWithMultiTag != null)
            {
                //Make sure the MultiTag exists
                if (tempMT)
                {
                    safeTagList = new string[tempMT.GetTagCount(), 2];

                    list = tempMT.GetTagsAsList();
                    for (int i = 0; i < list.Count; i++)
                    {
                        safeTagList[i, 0] = list[i];
                        safeTagList[i, 1] = "t";
                    }

                    foreach (GameObject obj in gameObjectArray)
                    {
                        if (obj != gameObjectArray[0])
                        {
                            tempMT = obj.GetComponent<MultiTag>();

                            if (tempMT)
                            {
                                List<string> getTagsTempList = new List<string>();
                                getTagsTempList = tempMT.GetTagsAsList();

                                for (int i = 0; i < list.Count; i++)
                                {
                                    bool test = false;

                                    foreach (string str in getTagsTempList)
                                    {
                                        if (safeTagList[i, 0] == str)
                                            test = true;
                                    }

                                    if (!test)
                                        safeTagList[i, 1] = "f";
                                }
                            }
                            else
                            {
                                Debug.Log("The GameObject (" + obj.name + ") does not have a MultiTag component and therefor was not included in the results for the function GetAllCommonTags");
                            }
                        }
                    }
                }

                for (int i = 0; i < list.Count; i++)
                {
                    if (safeTagList[i, 1] == "t")
                        tempList.Add(safeTagList[i, 0]);
                }
            }
        }

        return tempList;
    }

    /// <summary>
    /// Returns a string list of all the common tags in a group of GameObjects
    /// </summary>
    /// <param name="gameObjectList">The GameObject list to search</param>
    /// <returns></returns>
    public List<string> GetAllCommonTags(List<GameObject> gameObjectList)
    {
        List<string> tempList = new List<string>();

        if (gameObjectList.Count > 0)
        {
            MultiTag tempMT = gameObjectList[0].GetComponent<MultiTag>();

            //Can't use a dictionary because I can't iterate through the Keys :(
            string[,] safeTagList = new string[1, 1];
            List<string> list = new List<string>();

            RefreshIfNeeded();
            if (gameObjectsTaggedWithMultiTag != null)
            {
                //Make sure the MultiTag exists
                if (tempMT)
                {
                    safeTagList = new string[tempMT.GetTagCount(), 2];

                    list = tempMT.GetTagsAsList();
                    for (int i = 0; i < list.Count; i++)
                    {
                        safeTagList[i, 0] = list[i];
                        safeTagList[i, 1] = "t";
                    }

                    foreach (GameObject obj in gameObjectList)
                    {
                        if (obj != gameObjectList[0])
                        {
                            tempMT = obj.GetComponent<MultiTag>();

                            if (tempMT)
                            {
                                List<string> getTagsTempList = new List<string>();
                                getTagsTempList = tempMT.GetTagsAsList();

                                for (int i = 0; i < list.Count; i++)
                                {
                                    bool test = false;

                                    foreach (string str in getTagsTempList)
                                    {
                                        if (safeTagList[i, 0] == str)
                                            test = true;
                                    }

                                    if (!test)
                                        safeTagList[i, 1] = "f";
                                }
                            }
                            else
                            {
                                Debug.Log("The GameObject (" + obj.name + ") does not have a MultiTag component and therefor was not included in the results for the function GetAllCommonTags");
                            }
                        }
                    }
                }

                for (int i = 0; i < list.Count; i++)
                {
                    if (safeTagList[i, 1] == "t")
                        tempList.Add(safeTagList[i, 0]);
                }
            }
        }

        return tempList;
    }

    /// <summary>
    /// Returns a string list of all the common tags in a group of GameObjects
    /// </summary>
    /// <param name="MultiTagArray">The MultiTag array to search</param>
    /// <returns></returns>
    public List<string> GetAllCommonTags(MultiTag[] MultiTagArray)
    {
        List<string> tempList = new List<string>();

        if (MultiTagArray.Length > 0)
        {
            MultiTag tempMT = MultiTagArray[0];

            //Can't use a dictionary because I can't iterate through the Keys :(
            string[,] safeTagList = new string[1, 1];
            List<string> list = new List<string>();

            RefreshIfNeeded();
            if (gameObjectsTaggedWithMultiTag != null)
            {
                //Make sure the MultiTag exists
                if (tempMT)
                {
                    safeTagList = new string[tempMT.GetTagCount(), 2];

                    list = tempMT.GetTagsAsList();
                    for (int i = 0; i < list.Count; i++)
                    {
                        safeTagList[i, 0] = list[i];
                        safeTagList[i, 1] = "t";
                    }

                    foreach (MultiTag obj in MultiTagArray)
                    {
                        if (obj != MultiTagArray[0])
                        {
                            tempMT = obj.GetComponent<MultiTag>();

                            if (tempMT)
                            {
                                List<string> getTagsTempList = new List<string>();
                                getTagsTempList = tempMT.GetTagsAsList();

                                for (int i = 0; i < list.Count; i++)
                                {
                                    bool test = false;

                                    foreach (string str in getTagsTempList)
                                    {
                                        if (safeTagList[i, 0] == str)
                                            test = true;
                                    }

                                    if (!test)
                                        safeTagList[i, 1] = "f";
                                }
                            }
                            else
                            {
                                Debug.Log("The GameObject (" + obj.gameObject.name + ") does not have a MultiTag component and therefor was not included in the results for the function GetAllCommonTags");
                            }
                        }
                    }
                }

                for (int i = 0; i < list.Count; i++)
                {
                    if (safeTagList[i, 1] == "t")
                        tempList.Add(safeTagList[i, 0]);
                }
            }
        }

        return tempList;
    }

    /// <summary>
    /// Returns a string list of all the common tags in a group of GameObjects
    /// </summary>
    /// <param name="MultiTagList">The MultiTag list to search</param>
    /// <returns></returns>
    public List<string> GetAllCommonTags(List<MultiTag> MultiTagList)
    {
        List<string> tempList = new List<string>();

        if (MultiTagList.Count > 0)
        {
            MultiTag tempMT = MultiTagList[0];

            //Can't use a dictionary because I can't iterate through the Keys :(
            string[,] safeTagList = new string[1, 1];
            List<string> list = new List<string>();

            RefreshIfNeeded();
            if (gameObjectsTaggedWithMultiTag != null)
            {
                //Make sure the MultiTag exists
                if (tempMT)
                {
                    safeTagList = new string[tempMT.GetTagCount(), 2];

                    list = tempMT.GetTagsAsList();
                    for (int i = 0; i < list.Count; i++)
                    {
                        safeTagList[i, 0] = list[i];
                        safeTagList[i, 1] = "t";
                    }

                    foreach (MultiTag obj in MultiTagList)
                    {
                        if (obj != MultiTagList[0])
                        {
                            tempMT = obj.GetComponent<MultiTag>();

                            if (tempMT)
                            {
                                List<string> getTagsTempList = new List<string>();
                                getTagsTempList = tempMT.GetTagsAsList();

                                for (int i = 0; i < list.Count; i++)
                                {
                                    bool test = false;

                                    foreach (string str in getTagsTempList)
                                    {
                                        if (safeTagList[i, 0] == str)
                                            test = true;
                                    }

                                    if (!test)
                                        safeTagList[i, 1] = "f";
                                }
                            }
                            else
                            {
                                Debug.Log("The GameObject (" + obj.gameObject.name + ") does not have a MultiTag component and therefor was not included in the results for the function GetAllCommonTags");
                            }
                        }
                    }
                }

                for (int i = 0; i < list.Count; i++)
                {
                    if (safeTagList[i, 1] == "t")
                        tempList.Add(safeTagList[i, 0]);
                }
            }
        }

        return tempList;
    }

    /// <summary>
    /// Sets the priority of a tag to the selected priority for every object tagged with MultiTag.
    /// </summary>
    /// <param name="tagToSet"></param>
    /// <param name="priority"></param>
    [ExecuteInEditMode]
    public void SetGlobalPriority(string tagToSet, int priority)
    {
        int count = 0;

        RefreshIfNeeded();
        if (gameObjectsTaggedWithMultiTag != null)
        {
            MultiTag tempMT;

            foreach (GameObject obj in gameObjectsTaggedWithMultiTag)
            {
                tempMT = obj.GetComponent<MultiTag>();

                //Make sure the object has a MultiTagScript on it
                if (tempMT)
                {
                    bool test = tempMT.ChangeTagPriority(tagToSet, priority);

                    if (!test)
                        test = tempMT.ChangeTagPriorityToLast(tagToSet);

                    if (test)
                        count++;
                }
            }
        }

        if (!UnityEditor.EditorApplication.isPlaying)
            Debug.Log("Changed the tag \"" + tagToSet + "\" to priority " + priority + " for " + count + " objects");
    }
}
