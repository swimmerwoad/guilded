﻿//Copyright 2019, Zach Phillips, all rights reserved
//Created as part of the EZPZ MultiTag Unity Asset
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[System.Serializable]
public class MultiTag : MonoBehaviour
{
    [HideInInspector]
    [SerializeField]
    public List<string> TagList = new List<string>();

    private void Awake()
    {
        if (TagList.Count == 0)
            TagList = new List<string>();
    }

    /// <summary>
    /// Returns true if the tag list contains the searched tag
    /// </summary>
    /// <param name="tagToFind"></param>
    /// <returns></returns>
    public bool ContainsTag(string tagToFind)
    {

        for (int i = 0; i < TagList.Count; i++)
        {
            if (TagList[i] == tagToFind)
            {
                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Returns the list of tags as a string array, sorted in terms of priority with 0 being highest
    /// </summary>
    /// <returns></returns>
    public string[] GetTagsAsArray()
    {
        string[] tempList = new string[TagList.Count];

        for (int i = 0; i < TagList.Count; i++)
        {
            tempList[i] = TagList[i];
        }

        return tempList;
    }

    /// <summary>
    /// Returns the list of tags as a string list, sorted in terms of priority with 0 being highest
    /// </summary>
    /// <returns></returns>
    public List<string> GetTagsAsList()
    {
        List<string> tempList = new List<string>();

        foreach (string ti in TagList)
        {
            tempList.Add(ti);
        }

        return tempList;
    }

    /// <summary>
    /// Returns the string value of the tag at the selected index. Returns "-1" if index is out of range
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public string GetTagAt(int index)
    {

        if (TagList.Count - 1 >= index && index >= 0)
            return TagList[index];
        else
            return "-1";
    }

    /// <summary>
    /// Returns the index of a tag. Returns -1 if tag isn't found.
    /// </summary>
    /// <param name="tagToFind"></param>
    /// <returns></returns>
    public int GetTagIndex(string tagToFind)
    {
        for (int i = 0; i < TagList.Count; i++)
        {
            if (TagList[i] == tagToFind)
            {
                return i;
            }
        }

        return -1;
    }

    /// <summary>
    /// Same function as GetTagIndex, just reowrded. Returns the priority of a tag. Returns -1 if tag isn't found.
    /// </summary>
    /// <param name="tagToFind"></param>
    /// <returns></returns>
    public int GetTagPriority(string tagToFind)
    {
        for (int i = 0; i < TagList.Count; i++)
        {
            if (TagList[i] == tagToFind)
            {
                return i;
            }
        }

        return -1;
    }

    /// <summary>
    /// Retuns the amount of tags in the list. 
    /// </summary>
    /// <returns></returns>
    public int GetTagCount()
    {
        return TagList.Count;
    }

    /// <summary>
    /// Add a tag to the end of the list.
    /// </summary>
    /// <param name="tag">The tag to add.</param>
    public void AddTag(string tag)
    {
        foreach (string str in UnityEditorInternal.InternalEditorUtility.tags)
        {
            if (str == tag)
            {
                TagList.Add(tag);
            }
        }
    }

    /// <summary>
    /// Add a tag to a specified priority index. If index is larger than the list count it will be added to the end. 0 is the first index.
    /// </summary>
    /// <param name="tag">The tag to add.</param>
    /// <param name="index">The index to add the tag to.</param>
    public void AddTag(string tag, int index)
    {
        if (TagList.Count - 1 >= index && index >= 0)
        {
            foreach (string str in UnityEditorInternal.InternalEditorUtility.tags)
            {
                if (str == tag)
                {
                    TagList.Insert(index, tag);
                }
            }
        }
        else
        {
            foreach (string str in UnityEditorInternal.InternalEditorUtility.tags)
            {
                if (str == tag)
                {
                    TagList.Add(tag);
                }
            }
        }
    }

    /// <summary>
    /// Removes a tag from the list by string value. Returns false if tag doesn't exist.
    /// </summary>
    /// <param name="tag">The tag to remove.</param>
    /// <returns></returns>
    public bool RemoveTag(string tag)
    {
        for (int i = 0; i < TagList.Count; i++)
        {
            if (TagList[i] == tag)
            {
                TagList.RemoveAt(i);

                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Removes a tag from the list at the specified index. Returns false if index is out of bounds.
    /// </summary>
    /// <param name="index">The index to search.</param>
    /// <returns></returns>
    public bool RemoveTagAt(int index)
    {
        if (TagList.Count - 1 >= index && index >= 0)
        {
            TagList.RemoveAt(index);

            return true;
        }
        else
            return false;
    }

    /// <summary>
    /// Change the priority of a tag. Returns false if index is out of bounds or tag doesn't exist.
    /// </summary>
    /// <param name="tag">The tag to change.</param>
    /// <param name="index">The index to set the tag to. 0 is the first index.</param>
    /// <returns></returns>
    public bool ChangeTagPriority(string tag, int index)
    {
        if (TagList.Count - 1 >= index && index >= 0)
        {
            for (int i = 0; i < TagList.Count; i++)
            {
                if (TagList[i] == tag)
                {
                    if (TagList.Count - 1 != index)
                    {
                        TagList.Insert(index, tag);

                        if (index <= i)
                        {
                            TagList.RemoveAt(i + 1);
                        }
                        else
                        {
                            TagList.RemoveAt(i - 1);
                        }
                    }
                    else
                    {
                        TagList.Add(tag);
                    }

                    return true;
                }
            }
        }

        return false;
    }

    /// <summary>
    /// Change the priority of a tag to the first index. Returns false if index is out of bounds or tag doesn't exist.
    /// </summary>
    /// <param name="tag">The index (tag) to change.</param>
    /// <returns></returns>
    public bool ChangeTagPriorityToFirst(string tag)
    {
        for (int i = 0; i < TagList.Count; i++)
        {
            if (TagList[i] == tag)
            {
                TagList.RemoveAt(i);

                TagList.Insert(0, tag);

                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Change the priority of a tag to the last index. Returns false if index is out of bounds or tag doesn't exist.
    /// </summary>
    /// <param name="tag">The index (tag) to change.</param>
    /// <returns></returns>
    public bool ChangeTagPriorityToLast(string tag)
    {
        for (int i = 0; i < TagList.Count; i++)
        {
            if (TagList[i] == tag)
            {
                TagList.RemoveAt(i);

                TagList.Add(tag);

                return true;
            }
        }

        return false;
    }

    /// <summary>
    /// Change the priority of a tag. Returns false if index is out of bounds, tag doesn't exist, searchIndex and newIndex are either one apart (and not the last index), or searchIndex and newIndex are the same number.
    /// </summary>
    /// <param name="searchIndex">The index (tag) to change.</param>
    /// <param name="newIndex">The index to set the tag to. 0 is the first index.</param>
    /// <returns></returns>
    public bool ChangeTagPriority(int searchIndex, int newIndex)
    {
        //Make sure that the indexes are not one apart or the same number, unless one of them is the last index
        if ((Mathf.Abs(searchIndex - newIndex) != 1 && searchIndex - newIndex != 0) || (newIndex == TagList.Count - 1 || searchIndex == TagList.Count - 1))
        {
            if ((TagList.Count - 1 >= newIndex && newIndex >= 0) && ((TagList.Count - 1 >= searchIndex && searchIndex >= 0)))
            {
                string tagToTransfer = TagList[searchIndex];

                if (newIndex != TagList.Count - 1)
                {
                    TagList.Insert(newIndex, tagToTransfer);

                    if (newIndex <= searchIndex)
                    {
                        TagList.RemoveAt(searchIndex + 1);
                    }
                    else
                    {
                        if (searchIndex != 0)
                        {
                            TagList.RemoveAt(searchIndex - 1);
                        }
                        else
                        {
                            TagList.RemoveAt(0);
                        }
                    }
                }
                else
                {
                    TagList.Add(TagList[searchIndex]);
                }

                return true;
            }

            return false;
        }

        return false;
    }

    public override string ToString()
    {
        string tempString = "";

        for (int i = 0; i < TagList.Count; i++)
        {
            if (i != TagList.Count - 1)
                tempString += TagList[i] + ", ";
            else
                tempString += TagList[i];
        }

        return tempString;
    }
}
