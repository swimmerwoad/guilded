﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class MultiObjectSelectionEditor : Editor
{
    List<Object> multiObjList = new List<Object>();

    [ExecuteInEditMode]
    public void CleanUpMultiObject(Object[] passObj)
    {
        multiObjList.Clear();

        for (int i = 0; i < passObj.Length; i++)
        {
            multiObjList.Add(passObj[i]);
        }

        IterateThroughObjects();
    }

    [ExecuteInEditMode]
    IEnumerator IterateThroughObjects()
    {
        if (multiObjList.Count > 0)
        {
            yield return new WaitForSeconds(.01f);

            Selection.activeObject = multiObjList[multiObjList.Count - 1];

            multiObjList.RemoveAt(multiObjList.Count - 1);

            IterateThroughObjects();
        }
    }
}
