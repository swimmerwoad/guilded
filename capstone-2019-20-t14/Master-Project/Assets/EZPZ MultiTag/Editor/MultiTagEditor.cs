﻿//Copyright 2019, Zach Phillips, all rights reserved
//Created as part of the EZPZ MultiTag Unity Asset
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEditorInternal;
using UnityEngine;

[CustomEditor(typeof(MultiTag))]
[CanEditMultipleObjects]
public class MultiTagEditor : Editor
{
    //Create two ReordableLists, one for single object and another for multi object editing
    private ReorderableList reoList;
    private ReorderableList reoMultiList;

    //Create stealalized object to assign target to
    private SerializedObject so;

    //Need for multi object editing in order to keep values OnDisable
    private GameObject[] previousSelectedGameObjs;
    private GameObject previousSelectedObj;
    private GameObject multiEditSelectedObj;

    //The list for all multi object editing
    private List<string> multiTagList;

    //Two int counters for multi object editing
    private int tagAddedCounter = 0;
    private int tagRemovedCounter = 0;

    //If one object is selected or multiple
    private bool multiActivated = false;

    //Vars needed for Undo functionality
    private bool firstFrame = true;
    List<string> DisplayCheckListOld = new List<string>();
    List<string> DisplayCheckListCurrent = new List<string>();

    //GUI style which enables the header to become Rich Text
    private GUIStyle headerStyle = new GUIStyle();

    bool iterateThrough = false;
    bool updateNeeded = false;
    List<Object> multiObjList = new List<Object>();

    private MultiTag ListExample
    {
        get
        {
            return target as MultiTag;
        }
    }

    //Two string lists needed for multi object editing
    private List<string> SharedValuesListExampleDisplayList;
    private List<string> SharedValuesListExampleTagList;

    /// <summary>
    /// OnEnable is called one time, when the heirarchy focus is given to the object.
    /// </summary>
    void OnEnable()
    {
        EditorApplication.update += Update;

        firstFrame = true;        

        //This is needed to make sure that the objects are saved correctly, even
        //if the user exits the scene without manually saving the object
        if (!EditorApplication.isPlaying)
            EditorSceneManager.MarkAllScenesDirty();

        //Needed for the header to look nice
        headerStyle.richText = true;
        headerStyle.wordWrap = true;

        // Fetch the objects from the MyScript script to display in the inspector
        so = new SerializedObject(target);

        //If only one object is selected
        if (Selection.objects.Length == 1)
        {
            //Make sure the object has the MultiTag tag
            if (Selection.activeGameObject.tag == "MultiTag")
            {
                //Now editing a single object
                multiActivated = false;

                //make sure everything is initialized and set to use
                var inventory = (Selection.activeGameObject != null) ? Selection.activeGameObject.GetComponent<MultiTag>() : null;
                if (inventory != null)
                {
                    //create the single object ReordableList
                    reoList = new ReorderableList(ListExample.TagList, typeof(MultiTag), true, true, true, true);

                    //Set the callbacks for these events
                    reoList.drawHeaderCallback += DrawHeader;
                    reoList.drawElementCallback += DrawElement;
                    reoList.onAddCallback += AddItem;
                    reoList.onRemoveCallback += RemoveItem;
                }
            }
            else
            {
                Debug.LogError("Error: Selected object (" + Selection.activeGameObject.name + ") is not tagged with \"MultiTag\". This is required for EZPZ MultiTag to function. Please set the selected object tag to \"MultiTag\"");
            }
        }
        //If more than one object is selected
        else if (Selection.objects.Length > 1)
        {
            //Check to make sure all selected objs have a MultiTag
            bool allHaveScript = true;
            foreach (GameObject obj in Selection.gameObjects)
            {
                if (!obj.GetComponent<MultiTag>())
                    allHaveScript = false;
            }

            if (allHaveScript)
            {
                //Make sure all selected objects have the MultiTag tag on them
                bool allHaveTag = true;
                List<string> affectedObjs = new List<string>();
                foreach (GameObject obj in Selection.gameObjects)
                {
                    if (obj.tag != "MultiTag")
                    {
                        allHaveTag = false;
                        affectedObjs.Add(obj.name);
                    }
                }

                if (allHaveTag)
                {
                    //Set up the object pool and shared tag values
                    previousSelectedGameObjs = Selection.gameObjects;
                    SharedValuesListExampleDisplayList = new List<string>();
                    SharedValuesListExampleTagList = new List<string>();

                    //Now editing multiple objects
                    multiActivated = true;

                    //Reset the counters
                    tagAddedCounter = 0;
                    tagRemovedCounter = 0;

                    //Set the one specific object from target
                    previousSelectedObj = Selection.activeGameObject;

                    //Make sure we're starting fresh
                    multiTagList = new List<string>();

                    //Need to set up a new list with nothing expect for the shared values in it                
                    reoMultiList = new ReorderableList(multiTagList, typeof(MultiTag), true, true, true, true);

                    //Set the callbacks for these events
                    reoMultiList.drawHeaderCallback += DrawHeader;
                    reoMultiList.drawElementCallback += DrawElement;
                    reoMultiList.onAddCallback += AddItem;
                    reoMultiList.onRemoveCallback += RemoveItem;

                    //Add values to the list that are shared between all selected objects
                    MultiTag tempMT = (MultiTag)target;
                    for (int i = 0; i < tempMT.TagList.Count; i++)
                    {
                        bool tagIsShared = true;

                        foreach (GameObject go in Selection.gameObjects)
                        {
                            List<string> tempItemList = new List<string>();
                            for (int j = 0; j < go.GetComponent<MultiTag>().TagList.Count; j++)
                            {
                                tempItemList.Add(go.GetComponent<MultiTag>().TagList[j]);
                            }

                            bool containsTag = false;

                            foreach (string ti in tempItemList)
                            {
                                if (ti == tempMT.TagList[i])
                                    containsTag = true;
                            }

                            if (!containsTag)
                            {
                                tagIsShared = false;
                            }
                        }

                        if (tagIsShared)
                        {
                            //Tag is on all objects selected, add it to the shared list
                            multiTagList.Add(tempMT.TagList[i]);
                        }
                    }

                    //Make sure we're starting fresh
                    SharedValuesListExampleDisplayList.Clear();

                    //Add initial common tags to a new list for later use
                    foreach (string str in multiTagList)
                    {
                        SharedValuesListExampleDisplayList.Add(str);
                    }
                }
                else
                {
                    string allNames = "";

                    if (affectedObjs.Count > 1)
                    {
                        for (int i = 0; i < affectedObjs.Count; i++)
                        {
                            if (i != affectedObjs.Count - 1)
                                allNames += affectedObjs[i] + ", ";
                            else
                                allNames += affectedObjs[i];
                        }
                    }
                    else
                    {
                        allNames += affectedObjs[0];
                    }

                    Debug.LogError("Error: One or more selected objects (" + allNames + ") are not tagged with \"MultiTag\". This is required for EZPZ MultiTag to function. Please set the selected objects tag to \"MultiTag\"");
                }
            }
        }
    }

    /// <summary>
    /// Used to output Debug.Log messages about an object's lists status.
    /// </summary>
    /// <param name="obj">GameObject to be read.</param>
    private void DebugOutputList(GameObject obj)
    {
        Debug.Log("Outputting info for " + obj);
        foreach(string str in obj.GetComponent<MultiTag>().TagList)
        {
            Debug.Log("TagList: " + str);
        }

        Debug.Log("~~~~~~~~~~~~~");
    }

    /// <summary>
    /// Used to output Debug.Log messages about an object's lists status.
    /// </summary>
    /// <param name="mt">MultiTag to be read.</param>
    /// <param name="name">Name of the MultiTag object.</param>
    private void DebugOutputList(MultiTag mt, string name)
    {
        Debug.Log("Outputting info for " + name);

        foreach (string str in mt.TagList)
        {
            Debug.Log("TagList: " + str);
        }

        Debug.Log("<color=red>~~~~~~~~~~~~~</color>");
    }

    /// <summary>
    /// OnDisable is called one time, when the heirarchy focus is taken away from the object.
    /// </summary>
    private void OnDisable()
    {
        if (!multiActivated)
        {
            if (!EditorApplication.isPlaying && target != null)
                PrefabUtility.RecordPrefabInstancePropertyModifications(target);

            //SaveListValues();
        }
        else
        {
            //Need to save afterwards or else it screws up the rest of the objs
            //Also putting selected object last for same reason
            foreach (GameObject obj in previousSelectedGameObjs)
            {
                if (obj != previousSelectedObj)
                {
                    SaveListValuesFromMulti(obj);
                }
            }
            SaveListValuesFromMulti(previousSelectedObj);

            if (!EditorApplication.isPlaying)
                EditorSceneManager.SaveScene(Selection.activeGameObject.scene);

            if (Selection.gameObjects.Length <= 1)
            {
                multiEditSelectedObj = Selection.activeGameObject;

                Debug.Log("Saving all edited MultiTag objects, please wait.");

                multiObjList.Clear();

                for (int i = 0; i < previousSelectedGameObjs.Length; i++)
                {
                    multiObjList.Add(previousSelectedGameObjs[i]);
                }

                iterateThrough = true;
                updateNeeded = true;
            }
        }
    }

    /// <summary>
    /// Draws the header label of the list.
    /// </summary>
    /// <param name="rect">Used to set position modifiers.</param>
    private void DrawHeader(Rect rect)
    {
        if (!multiActivated)
        {
            GUI.Label(rect, "<b>Priority:</b> <color=red>Don't Undo, Use Add/Remove Buttons Instead</color>", headerStyle);
        }
        else
            GUI.Label(rect, "<b>Common Tags:</b> <color=red>Don't Undo, Use Add/Remove Buttons Instead</color>", headerStyle);
    }

    /// <summary>
    /// Draws one item of the Reorderable List.
    /// </summary>
    /// <param name="rect">Used to set position modifiers.</param>
    /// <param name="index">The index of the selected list item.</param>
    /// <param name="active">If the list item is active.</param>
    /// <param name="focused">If the list item is focused.</param>
    private void DrawElement(Rect rect, int index, bool active, bool focused)
    {
        if (!multiActivated)
        {
            string item = ListExample.TagList[index];

            //Set up the display for the list
            //Space left at the end for easier item selecting
            ListExample.TagList[index] = (EditorGUI.TagField(new Rect(rect.x, rect.y, rect.width - 15, rect.height), item));
        }
        else
        {
            string item = multiTagList[index];

            //Set up the display for the list
            //Space left at the end for easier item selecting
            multiTagList[index] = (EditorGUI.TagField(new Rect(rect.x, rect.y, rect.width - 15, rect.height), item));
        }
    }

    /// <summary>
    /// Adds an item to a Reorderable List.
    /// </summary>
    /// <param name="list">The list to use. Currently not used.</param>
    private void AddItem(ReorderableList list)
    {
        if (!EditorApplication.isPlaying && Selection.gameObjects.Length > 0)
            EditorSceneManager.MarkSceneDirty(Selection.activeGameObject.scene);

        if (!multiActivated)
        {
            ListExample.TagList.Add("");

            if (!EditorApplication.isPlaying)
            {
                PrefabUtility.RecordPrefabInstancePropertyModifications(target);
            }

            RemoveDuplicateTags(ListExample);
        }
        else
        {
            multiTagList.Add("");

            //Increase the counter for tags added in this multi edit session
            tagAddedCounter++;
        }
    }

    /// <summary>
    /// Remove an item from a Reorderable List.
    /// </summary>
    /// <param name="list">The list to use. Currently not used.</param>
    private void RemoveItem(ReorderableList list)
    {
        if (!EditorApplication.isPlaying && Selection.gameObjects.Length > 0)
            EditorSceneManager.MarkSceneDirty(Selection.activeGameObject.scene);

        if (!multiActivated)
        {
            ListExample.TagList.RemoveAt(list.index);

            RemoveDuplicateTags(ListExample);

            if (!EditorApplication.isPlaying)
            {
                PrefabUtility.RecordPrefabInstancePropertyModifications(target);
            }
        }
        else
        {
            string tagToRemove = multiTagList[list.index];

            //Search each object in the selection and remove the tag item to be removed
            foreach (GameObject obj in Selection.gameObjects)
            {
                if (obj != Selection.activeObject)
                {
                    MultiTag tempMT = obj.GetComponent<MultiTag>();

                    if (tempMT.TagList.Count > 0)
                    {
                        for (int i = 0; i < tempMT.TagList.Count; i++)
                        {
                            if (tempMT.TagList[i] == tagToRemove)
                                tempMT.TagList.RemoveAt(i);
                        }
                    }
                }

                //If the object is the MultiListExample(previousSelectedObject), remove the tag from the list
                else
                {
                    MultiTag tempMT = obj.GetComponent<MultiTag>();

                    for (int i = 0; i < tempMT.TagList.Count; i++)
                    {
                        if (multiTagList[i] == tagToRemove)
                            multiTagList.RemoveAt(i);
                    }

                    for (int i = 0; i < tempMT.TagList.Count; i++)
                    {
                        if (tempMT.TagList[i] == tagToRemove)
                            tempMT.TagList.RemoveAt(i);
                    }
                }
            }

            //Increase the counter for tags removed in this multi edit session
            tagRemovedCounter++;
        }
    }

    private void CheckIfTagChanged(MultiTag mt)
    {
        if (!firstFrame)
        {
            //check to see if all the previous match the current
            if (DisplayCheckListOld != null && DisplayCheckListOld.Count > 0)
            {
                DisplayCheckListCurrent.Clear();
                foreach (string ti in mt.TagList)
                {
                    DisplayCheckListCurrent.Add(ti);
                }

                bool changeSinceLastFrame = false;

                //If the lists are the same size, showing a change in the tag string value only
                if (DisplayCheckListCurrent.Count == DisplayCheckListOld.Count)
                {
                    for (int i = 0; i < DisplayCheckListCurrent.Count; i++)
                    {
                        if (DisplayCheckListCurrent[i] != DisplayCheckListOld[i])
                        {
                            changeSinceLastFrame = true;
                        }
                    }
                }

                if (changeSinceLastFrame)
                {
                    RemoveDuplicateTags(mt);
                    if (!EditorApplication.isPlaying && Selection.gameObjects.Length > 0)
                        EditorSceneManager.MarkSceneDirty(Selection.activeGameObject.scene);

                    if (!EditorApplication.isPlaying)
                    {
                        PrefabUtility.RecordPrefabInstancePropertyModifications(target);
                    }
                }

                //Last, update the old list
                DisplayCheckListOld.Clear();
                foreach (string str in DisplayCheckListCurrent)
                {
                    DisplayCheckListOld.Add(str);
                }
            }
        }
        else
        {
            DisplayCheckListOld.Clear();
            foreach (string ti in mt.TagList)
            {
                DisplayCheckListOld.Add(ti);
            }
        }
    }

    private void CheckIfTagChanged(List<string> mt)
    {
        if (!firstFrame)
        {
            //check to see if all the previous match the current
            if (DisplayCheckListOld != null && DisplayCheckListOld.Count > 0)
            {
                DisplayCheckListCurrent.Clear();
                foreach (string ti in mt)
                {
                    DisplayCheckListCurrent.Add(ti);
                }

                bool changeSinceLastFrame = false;

                //If the lists are the same size, showing a change in the tag string value only
                if (DisplayCheckListCurrent.Count == DisplayCheckListOld.Count)
                {
                    for (int i = 0; i < DisplayCheckListCurrent.Count; i++)
                    {
                        if (DisplayCheckListCurrent[i] != DisplayCheckListOld[i])
                        {
                            changeSinceLastFrame = true;
                        }
                    }
                }

                if (changeSinceLastFrame)
                {
                    //Remove dupes
                    //Remove the tags from TagList and then update DisplayList accordingly
                    for (int i = 0; i < mt.Count; i++)
                    {
                        string currentTag = mt[i];

                        for (int j = 0; j < mt.Count; j++)
                        {
                            //If we are not looking at the exact tag index as the search tag
                            if (j != i)
                            {
                                if (mt[j] == currentTag)
                                {
                                    mt.RemoveAt(j);
                                    j = 0;
                                }
                            }
                        }
                    }

                    if (!EditorApplication.isPlaying && Selection.gameObjects.Length > 0)
                        EditorSceneManager.MarkSceneDirty(Selection.activeGameObject.scene);

                    if (!EditorApplication.isPlaying)
                    {
                        PrefabUtility.RecordPrefabInstancePropertyModifications(target);
                    }
                }

                //Last, update the old list
                DisplayCheckListOld.Clear();
                foreach (string str in DisplayCheckListCurrent)
                {
                    DisplayCheckListOld.Add(str);
                }
            }
        }
        else
        {
            DisplayCheckListOld.Clear();
            foreach (string ti in mt)
            {
                DisplayCheckListOld.Add(ti);
            }
        }
    }

    private void SaveMultiObjects()
    {
        GameObject obj;

        for (int gameObj = 0; gameObj < previousSelectedGameObjs.Length; gameObj++)
        {
            if (previousSelectedGameObjs[gameObj] != previousSelectedObj)
            {
                obj = previousSelectedGameObjs[gameObj];

                //Set up copies of the shared lists
                List<string> SharedValuesListExampleDisplayListCOPY = new List<string>();
                SharedValuesListExampleDisplayListCOPY.Clear();
                foreach (string str in SharedValuesListExampleDisplayList)
                {
                    SharedValuesListExampleDisplayListCOPY.Add(str);
                }

                List<string> SharedValuesListExampleTagListCOPY = new List<string>();
                SharedValuesListExampleTagListCOPY.Clear();

                //If the object exists (in case of deletion)
                if (obj != null)
                {
                    var mt = obj.GetComponent<MultiTag>();

                    //Add all values from the DisplayList to the TagList
                    foreach (string te in multiTagList)
                    {
                        mt.TagList.Add(te);
                    }

                    //Get counter for all tag values that are the same
                    int sameValues = 0;

                    for (int i = 0; i < multiTagList.Count; i++)
                    {
                        for (int j = 0; j < SharedValuesListExampleDisplayListCOPY.Count; j++)
                        {
                            if (multiTagList[i] == SharedValuesListExampleDisplayListCOPY[j])
                            {
                                sameValues++;
                            }
                        }
                    }

                    //If need to replace old tag values
                    if (SharedValuesListExampleDisplayListCOPY.Count > sameValues + tagRemovedCounter && SharedValuesListExampleDisplayListCOPY.Count != 0)
                    {
                        //Remove all shared values still in the list and add them to a seperate one
                        for (int i = 0; i < multiTagList.Count; i++)
                        {
                            string te = multiTagList[i];
                            for (int j = 0; j < SharedValuesListExampleDisplayListCOPY.Count; j++)
                            {
                                string newTE = SharedValuesListExampleDisplayListCOPY[j];
                                if (te == newTE)
                                {
                                    SharedValuesListExampleTagListCOPY.Add(te);
                                    SharedValuesListExampleDisplayListCOPY.Remove(newTE);
                                }
                            }
                        }

                        //At this point, all old (to be replaced) tags are in SharedValuesListExampleDisplayListCOPY

                        //Find all of those tag indexes in mt.TagList
                        List<int> multiChangeArray = new List<int>();
                        multiChangeArray.Clear();

                        //Search for old tags and store their array number
                        for (int i = 0; i < mt.TagList.Count; i++)
                        {
                            bool isOnList = true;
                            string multiListString = mt.TagList[i];

                            for (int j = 0; j < SharedValuesListExampleDisplayListCOPY.Count; j++)
                            {
                                //If on the old tag list
                                if (multiListString != SharedValuesListExampleDisplayListCOPY[j])
                                {
                                    isOnList = false;
                                }
                            }

                            if (isOnList)
                            {
                                //This means that the tag at the 'i' index isn't on the Shared DisplayList
                                //Save it to change later
                                multiChangeArray.Add(i);
                            }
                        }

                        //All tag values that should be replaced have their indexes stored in multiChangeArray

                        //Iterate through the multiChangeArray
                        for (int i = 0; i < multiChangeArray.Count; i++)
                        {
                            //Go through all the tags in the multiLineExample, excluding newly added ones
                            //This should work even if things are moved around...I hope
                            for (int j = 0; j < multiTagList.Count - tagAddedCounter; j++)
                            {
                                bool isShared = false;
                                //Check to make sure it isn't on the 'not to touch' list
                                for (int x = 0; x < SharedValuesListExampleTagListCOPY.Count; x++)
                                {
                                    if (multiTagList[j] == SharedValuesListExampleTagListCOPY[x])
                                    {
                                        isShared = true;
                                    }
                                }

                                if (!isShared)
                                {
                                    //Replace the offending tag value and add new value to the 'not to touch' list
                                    mt.TagList[multiChangeArray[i]] = multiTagList[j];
                                    SharedValuesListExampleTagListCOPY.Add(multiTagList[j]);
                                }
                            }
                        }
                    }

                    //Check and remove duplicate tags, keeping the highest priority one
                    RemoveDuplicateTags(mt);

                    //Needed if modifying a prefab
                    //if (PrefabUtility.GetCorrespondingObjectFromSource(obj))
                    PrefabUtility.RecordPrefabInstancePropertyModifications(obj);
                }
            }
        }

        SaveListValuesFromMulti(previousSelectedObj);
    }

    /// <summary>
    /// Save the values from the Common Tag list for each object in the multi object selection.
    /// </summary>
    /// <param name="obj">The object to be saved.</param>
    private void SaveListValuesFromMulti(GameObject obj)
    {
        //EditorUtility.InstanceIDToObject(selectedObjInstanceID) as MultiTag;
        //Set up copies of the shared lists
        List<string> SharedValuesListExampleDisplayListCOPY = new List<string>();
        SharedValuesListExampleDisplayListCOPY.Clear();
        foreach (string str in SharedValuesListExampleDisplayList)
        {
            SharedValuesListExampleDisplayListCOPY.Add(str);
        }

        List<string> SharedValuesListExampleTagListCOPY = new List<string>();
        SharedValuesListExampleTagListCOPY.Clear();

        //If the object exists (in case of deletion)
        if (obj != null)
        {
            var mt = obj.GetComponent<MultiTag>();

            //Add all values from the DisplayList to the TagList
            foreach (string te in multiTagList)
            {
                mt.TagList.Add(te);
            }

            //Get counter for all tag values that are the same
            int sameValues = 0;

            for (int i = 0; i < multiTagList.Count; i++)
            {
                for (int j = 0; j < SharedValuesListExampleDisplayListCOPY.Count; j++)
                {
                    if (multiTagList[i] == SharedValuesListExampleDisplayListCOPY[j])
                    {
                        sameValues++;
                    }
                }
            }

            //If need to replace old tag values
            if (SharedValuesListExampleDisplayListCOPY.Count > sameValues + tagRemovedCounter && SharedValuesListExampleDisplayListCOPY.Count != 0)
            {
                //Remove all shared values still in the list and add them to a seperate one
                for (int i = 0; i < multiTagList.Count; i++)
                {
                    string te = multiTagList[i];
                    for (int j = 0; j < SharedValuesListExampleDisplayListCOPY.Count; j++)
                    {
                        string newTE = SharedValuesListExampleDisplayListCOPY[j];
                        if (te == newTE)
                        {
                            SharedValuesListExampleTagListCOPY.Add(te);
                            SharedValuesListExampleDisplayListCOPY.Remove(newTE);
                        }
                    }
                }

                //At this point, all old (to be replaced) tags are in SharedValuesListExampleDisplayListCOPY

                //Find all of those tag indexes in mt.TagList
                List<int> multiChangeArray = new List<int>();
                multiChangeArray.Clear();

                //Search for old tags and store their array number
                for (int i = 0; i < mt.TagList.Count; i++)
                {
                    bool isOnList = true;
                    string multiListString = mt.TagList[i];

                    for (int j = 0; j < SharedValuesListExampleDisplayListCOPY.Count; j++)
                    {
                        //If on the old tag list
                        if (multiListString != SharedValuesListExampleDisplayListCOPY[j])
                        {
                            isOnList = false;
                        }
                    }

                    if (isOnList)
                    {
                        //This means that the tag at the 'i' index isn't on the Shared DisplayList
                        //Save it to change later
                        multiChangeArray.Add(i);
                    }
                }

                //All tag values that should be replaced have their indexes stored in multiChangeArray

                //Iterate through the multiChangeArray
                for (int i = 0; i < multiChangeArray.Count; i++)
                {
                    //Go through all the tags in the multiLineExample, excluding newly added ones
                    //This should work even if things are moved around...I hope
                    for (int j = 0; j < multiTagList.Count - tagAddedCounter; j++)
                    {
                        bool isShared = false;
                        //Check to make sure it isn't on the 'not to touch' list
                        for (int x = 0; x < SharedValuesListExampleTagListCOPY.Count; x++)
                        {
                            if (multiTagList[j] == SharedValuesListExampleTagListCOPY[x])
                            {
                                isShared = true;
                            }
                        }

                        if (!isShared)
                        {
                            //Replace the offending tag value and add new value to the 'not to touch' list
                            mt.TagList[multiChangeArray[i]] = multiTagList[j];
                            SharedValuesListExampleTagListCOPY.Add(multiTagList[j]);
                        }
                    }
                }
            }

            //Check and remove duplicate tags, keeping the highest priority one
            RemoveDuplicateTags(mt);

            //Needed if modifying a prefab
            //if (PrefabUtility.GetCorrespondingObjectFromSource(obj))
            PrefabUtility.RecordPrefabInstancePropertyModifications(obj);
        }
    }

    /// <summary>
    /// Removes duplicate tag items from a list, keeping the highest priority.
    /// </summary>
    /// <param name="mt">The MultiTag to fix</param>
    private void RemoveDuplicateTags(MultiTag mt)
    {
        //Remove the tags from TagList and then update DisplayList accordingly
        for (int i = 0; i < mt.TagList.Count; i++)
        {
            string currentTag = mt.TagList[i];

            for (int j = 0; j < mt.TagList.Count; j++)
            {
                //If we are not looking at the exact tag index as the search tag
                if (j != i)
                {
                    if (mt.TagList[j] == currentTag)
                    {
                        mt.TagList.RemoveAt(j);
                        j = 0;
                    }
                }
            }
        }
    }

    /// <summary>
    /// Records changes to an object to add to the Undo stack. Currently unused.
    /// </summary>
    /// <param name="obj">Object to save</param>
    /// <param name="description">Description to appear in the 'Edit' window</param>
    private void RecordObjectForUndo(Object obj, string description)
    {
        Undo.RecordObject(obj, description);
        //Formerly "EditorUtility.SetDirty(target);"
        //However, this is outdated, and marking the scene as modified saves me a HUGE headache
        //Read more on it here: https://docs.unity3d.com/ScriptReference/EditorUtility.SetDirty.html

        //Must be called according to the docs if it's a prefab
        //if (PrefabUtility.GetCorrespondingObjectFromSource(obj))
            PrefabUtility.RecordPrefabInstancePropertyModifications(obj);
    }

    /// <summary>
    /// OnInspectorGUI is like the Update function of this editor.
    /// </summary>
    public override void OnInspectorGUI()
    {
        if (!multiActivated)
        {
            //If you see these errors try reseting the component
            if (so == null)
            {
                Debug.LogError("ScriptableObject is null");
            }
            else if (reoList == null)
            {
                OnEnable();
                //Debug.LogError("ReorderableList is null");
            }
            else if (reoList.list == null)
            {
                Debug.LogError("ReorderableList points to a null list");
            }
            else
            {
                CheckIfTagChanged((MultiTag)target);

                so.Update();
                reoList.DoLayoutList();
                so.ApplyModifiedProperties();
            }
        }
        else
        {
            //If you see these errors try reseting the component
            if (so == null)
            {
                Debug.LogError("ScriptableObject is null");
            }
            else if (reoMultiList == null)
            {
                OnEnable();
                //Debug.LogError("ReorderableMultiList is null");
            }
            else if (reoMultiList.list == null)
            {
                Debug.LogError("ReorderableMultiList points to a null list");
            }
            else
            {
                CheckIfTagChanged(multiTagList);

                so.Update();
                reoMultiList.DoLayoutList();
                so.ApplyModifiedProperties();
            }
        }

        if (firstFrame)
            firstFrame = false;
    }

    /// <summary>
    /// Needed to properly save after any MultiObject edit due to prefabs
    /// </summary>
    private void Update()
    {
        if (updateNeeded)
        {
            if (iterateThrough)
            {
                if (multiObjList.Count > 0)
                {
                    Selection.activeObject = multiObjList[multiObjList.Count - 1];

                    multiObjList.RemoveAt(multiObjList.Count - 1);
                }
                else
                {
                    Selection.activeGameObject = multiEditSelectedObj;

                    iterateThrough = false;
                    updateNeeded = false;

                    if (!EditorApplication.isPlaying)
                        EditorSceneManager.SaveScene(Selection.activeGameObject.scene);

                    Debug.Log("All objects saved, thank you for waiting.");
                }
            }
        }
    }
}
