﻿//Copyright 2019, Zach Phillips, all rights reserved
//Created as part of the EZPZ MultiTag Unity Asset
using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(MultiTagSearchMaster))]
public class MultiTagSearchMasterEditor : Editor
{
    int priority = 0;
    string tag = "";

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var temp = GUI.skin.GetStyle("Label");
        temp.alignment = TextAnchor.UpperCenter;
        temp.richText = true;

        EditorGUILayout.LabelField("<b>Change Global Tag Priority</b>", temp);

        priority = EditorGUILayout.IntField("Priority:", priority);
        tag = EditorGUILayout.TagField("Tag:", tag);

        MultiTagSearchMaster myScript = (MultiTagSearchMaster)target;

        if (GUILayout.Button("Change Priority"))
        {
            myScript.SetGlobalPriority(tag, priority);

            if (!EditorApplication.isPlaying)
                EditorSceneManager.MarkAllScenesDirty();
        }
    }
}
