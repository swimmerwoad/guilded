﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Xml;

public class XmlLoader : MonoBehaviour
{
    public GeneratorData genData;
    public NPCDataStorage d;
    public QuestDataStorage qds;
    // Start is called before the first frame update
    void Awake()
    {
        ReadDataInit();
        LoadQuestXML();
        GenerateNPC();
    }

    private void ReadDataInit()
    {
        // Seed random on startup. 
        Rand.SeedRandom();
        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/NPCStatGen.xml");

        // Load Stat values.
        var nodeList = doc.DocumentElement.SelectNodes("/root/npc_classes/npc_class");

        foreach(XmlNode n in nodeList)
        {
            GeneratorData.NPC_Class_Generator newClass = new GeneratorData.NPC_Class_Generator();
            switch(n.Attributes["type"].Value)
            {
                case "Paladin":
                    newClass.id = GeneratorData.NPC_CLASS_ID.PALADIN;
                    break;
                case "Fighter":
                    newClass.id = GeneratorData.NPC_CLASS_ID.FIGHTER;
                    break;
                case "Mage":
                    newClass.id = GeneratorData.NPC_CLASS_ID.MAGE;
                    break;
                case "Cleric":
                    newClass.id = GeneratorData.NPC_CLASS_ID.CLERIC;
                    break;
                case "Archer":
                    newClass.id = GeneratorData.NPC_CLASS_ID.ARCHER;
                    break;
            }

            newClass.strWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.strWeight.statValue = float.Parse(n.SelectSingleNode("strength_weight").Attributes["value"].Value);
            newClass.strWeight.statID = GeneratorData.NPC_STAT_ID.STRENGTH;
            newClass.dexWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.dexWeight.statValue = float.Parse(n.SelectSingleNode("dexterity_weight").Attributes["value"].Value);
            newClass.dexWeight.statID = GeneratorData.NPC_STAT_ID.DEXTERITY;
            newClass.conWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.conWeight.statValue = float.Parse(n.SelectSingleNode("constitution_weight").Attributes["value"].Value);
            newClass.conWeight.statID = GeneratorData.NPC_STAT_ID.CONSTITUTION;
            newClass.intWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.intWeight.statValue = float.Parse(n.SelectSingleNode("intelligence_weight").Attributes["value"].Value);
            newClass.intWeight.statID = GeneratorData.NPC_STAT_ID.INTELLIGENCE;
            newClass.wisWeight = new GeneratorData.NPC_Stat_Weight();
            newClass.wisWeight.statValue = float.Parse(n.SelectSingleNode("wisdom_weight").Attributes["value"].Value);
            newClass.wisWeight.statID = GeneratorData.NPC_STAT_ID.WISDOM;

            genData.classData.Add(newClass);
        }

        // Load Names.
        GeneratorData.NPC_Names.Init();
        nodeList = doc.DocumentElement.SelectNodes("/root/npc_name_root/first_names");
        foreach(XmlNode n in nodeList)
        {
            foreach(XmlNode c in n)
            {
                GeneratorData.NPC_Names.firstNames.Add(c.Attributes["value"].Value);
            }
        }

        nodeList = doc.DocumentElement.SelectNodes("/root/npc_name_root/last_names");
        foreach (XmlNode n in nodeList)
        {
            foreach (XmlNode c in n)
            {
                GeneratorData.NPC_Names.lastNames.Add(c.Attributes["value"].Value);
            }
        }

        Debug.Log("loaded.");
    }

    /// <summary>
    /// Load quest generation data from the XML.
    /// </summary>
    private void LoadQuestXML()
    {
        LoadQuestMonsters();

        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/QuestGen.xml");

        var nodeList = doc.DocumentElement.SelectNodes("/root/quest");
        qds.QuestPool = new QuestDataStorage.Quest_LoadPool();

        CreateQuestPool(doc);
        LoadChecks(doc);
        CreateChatPool(doc);
    }

    /// <summary>
    /// Create the quest pool using the below helper function.
    /// </summary>
    /// <param name="doc">The doc that we currently have open.</param>
    private void CreateQuestPool(XmlDocument doc)
    {
        AddToQuest(doc, qds.QuestPool.Bases, "quest", "base");
        AddToQuest(doc, qds.QuestPool.QGiverUnnamed, "quest_givers_u", "giver");
        AddToQuest(doc, qds.QuestPool.QGiverNamed, "quest_givers", "giver");
        AddToQuest(doc, qds.QuestPool.Area, "area_of_action", "area");
        AddToQuest(doc, qds.QuestPool.EnemyType, "enemy_type", "type");
        AddToQuest(doc, qds.QuestPool.EnemyActions, "enemy_action", "action");
        AddToQuest(doc, qds.QuestPool.AffectedPlace, "affected_place", "place");
    }

    private void CreateChatPool(XmlDocument doc)
    {
        AddToQuest(doc, qds.QuestPool.Chat.Chat, "chats", "chat");
        AddToQuest(doc, qds.QuestPool.Chat.Objects, "chat_objects", "object");
    }

    /// <summary>
    /// Generic helper function to load the different lists for the quests instead of having 80 foreach loops.
    /// </summary>
    /// <param name="doc">the xml document that's loaded.</param>
    /// <param name="values">list of string values we're adding from the XML doc.</param>
    /// <param name="nodeName">parent node name.</param>
    /// <param name="subNodeName">sub nodes within the parent node. There are a lot of child nodes per parent node.</param>
    private void AddToQuest(XmlDocument doc, List<string> values, string nodeName, string subNodeName)
    {
        var nodeList = doc.DocumentElement.SelectNodes("/root/" + nodeName + "/" + subNodeName);
        foreach(XmlNode n in nodeList)
        {
            values.Add(n.Attributes["value"].Value);
        }
    }

    /// <summary>
    /// Similar to above, load each check.
    /// </summary>
    /// <param name="doc">the XML document</param>
    private void LoadChecks(XmlDocument doc)
    {
        AddCheck(doc, qds.QuestPool.Checks, "strength_checks", "check", GeneratorData.NPC_STAT_CHECK.STRENGTH);
        AddCheck(doc, qds.QuestPool.Checks, "dexterity_checks", "check", GeneratorData.NPC_STAT_CHECK.DEXTERITY);
        AddCheck(doc, qds.QuestPool.Checks, "constitution_checks", "check", GeneratorData.NPC_STAT_CHECK.CONSTITUTION);
        AddCheck(doc, qds.QuestPool.Checks, "intelligence_checks", "check", GeneratorData.NPC_STAT_CHECK.INTELLIGENCE);
        AddCheck(doc, qds.QuestPool.Checks, "wisdom_checks", "check", GeneratorData.NPC_STAT_CHECK.WISDOM);
        AddCheck(doc, qds.QuestPool.Checks, "party_checks", "check", GeneratorData.NPC_STAT_CHECK.PARTY_MAX_CHECK);
        AddCheck(doc, qds.QuestPool.Checks, "level_checks", "check", GeneratorData.NPC_STAT_CHECK.LEVEL_CHECK);

        qds.QuestPool.PositiveCheck = doc.DocumentElement.SelectSingleNode("/root/success_fail/check_success").Attributes["value"].Value;
        qds.QuestPool.NegativeCheck = doc.DocumentElement.SelectSingleNode("/root/success_fail/check_fail").Attributes["value"].Value;
    }

    /// <summary>
    /// Helper function to load information from the XML document
    /// </summary>
    /// <param name="doc">XML document.</param>
    /// <param name="values">The list of checks, since it's a list, it's always ref.</param>
    /// <param name="nodeName">Parent node name that contains multiple subnodes.</param>
    /// <param name="subNodeName">subnodes that we're looping through.</param>
    /// <param name="c">The kind of check we're adding.</param>
    private void AddCheck(XmlDocument doc, List<QuestDataStorage.Quest_Check> values, string nodeName, string subNodeName, GeneratorData.NPC_STAT_CHECK c)
    {
        var nodeList = doc.DocumentElement.SelectNodes("/root/" + nodeName + "/" + subNodeName);
        foreach(XmlNode n in nodeList)
        {
            var nQCL = new QuestDataStorage.Quest_Check();
            nQCL.Type = c;
            nQCL.CheckValue = n.Attributes["value"].Value;

            values.Add(nQCL);
        }
    }

    /// <summary>
    /// Loads data for different monsters heroes will encounter in quests. 
    /// It's stored in a different XML for readability.
    /// </summary>
    private void LoadQuestMonsters()
    {
        var doc = new XmlDocument();
        doc.Load(Application.streamingAssetsPath + "/QuestMonster.xml");

        var nodeList = doc.DocumentElement.SelectNodes("/root/monster");
        qds.QuestMonsters = new List<QuestDataStorage.QuestMonster>();

        foreach(XmlNode n in nodeList)
        {
            var nm = new QuestDataStorage.QuestMonster();
            nm.Name = n.SelectSingleNode("name").Attributes["value"].Value;
            nm.Health = int.Parse(n.SelectSingleNode("health").Attributes["value"].Value);
            nm.BaseDamage = int.Parse(n.SelectSingleNode("base_damage").Attributes["value"].Value);
            nm.DamageRange = int.Parse(n.SelectSingleNode("damage_range").Attributes["value"].Value);
            nm.Speed = int.Parse(n.SelectSingleNode("speed").Attributes["value"].Value);
            qds.QuestMonsters.Add(nm);
        }

    }

    private void GenerateNPC()
    {
        d.GenerateNewNPC();
    }

}
