﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Text;

[CreateAssetMenu(fileName = "NPCStorage", menuName = "ScriptableObjects/NPCData", order = 2)]
public class NPCDataStorage : ScriptableObject
{
    public class NPC_Stat
    {
        public string displayName { get; set; }
        public int statValue { get; set; }
        //public GeneratorData.NPC_STAT_ID statID { get; set; }
    }

    /// <summary>
    /// Skills are leveled, unlike stats. 
    /// Things like "gardening" "Fighting". So on.
    /// </summary>
    public class NPC_Skill
    {
        public int CurrentLevel { get; set; }
        public int CurrentExpPoints { get; set; }
        public int NextLevelPoints { get; set; }
    }

    public class NPC_Class_Data
    {
        public Dictionary<GeneratorData.NPC_STAT_ID, NPC_Stat> stats = new Dictionary<GeneratorData.NPC_STAT_ID, NPC_Stat>();
        public Dictionary<GeneratorData.NPC_SKILL_ID, NPC_Skill> skills = new Dictionary<GeneratorData.NPC_SKILL_ID, NPC_Skill>();
        public NPC_Stat personalityValue { get; set; }
        public NPC_Stat quirkValue { get; set; }
        public NPC_Stat traitValue { get; set; }

        public string DisplayStatData()
        {
            return "Strength: " + stats[GeneratorData.NPC_STAT_ID.STRENGTH].statValue +
                "\nDexterity: " + stats[GeneratorData.NPC_STAT_ID.DEXTERITY].statValue +
                "\nConstitution: " + stats[GeneratorData.NPC_STAT_ID.CONSTITUTION].statValue +
                "\nIntelligence: " + stats[GeneratorData.NPC_STAT_ID.INTELLIGENCE].statValue +
                "\nWisdom: " + stats[GeneratorData.NPC_STAT_ID.WISDOM].statValue;
        }
    }

    public class Guild_NPC
    {
        public Guild_NPC()
        {
            statData = new NPC_Class_Data();
        }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public NPC_Class_Data statData {get;set;}
        public GeneratorData.NPC_CLASS_ID classId;
        public int Level { get; set; }
        public string displayClassName { get; set; } //****TODO STORE THIS ELSEWHERE
        // Other data like looks?

        public string DisplayShortData()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(firstName + " " + lastName);
            s.AppendLine("level " + Level + " " + displayClassName);

            return s.ToString();
        }

        public string DisplayNPCData()
        {
            StringBuilder s = new StringBuilder();

            s.AppendLine(DisplayShortData());
            s.AppendLine(statData.DisplayStatData());

            return s.ToString();
        }

        public void LevelUp()
        {
            ++Level;

            int points = 2;
            while(points > 0)
            {
                var statToLevel = (GeneratorData.NPC_STAT_ID)Rand.GetRandom(0, (int)GeneratorData.NPC_STAT_ID.NUM_STATS);
                ++statData.stats[statToLevel].statValue;
                --points;
            }
        }
    }

    public List<Guild_NPC> listOfNPCs = new List<Guild_NPC>();
    public List<Guild_NPC> NPC_Pool = new List<Guild_NPC>();
    public GeneratorData dat;
    public int maxInitPoints;
    private int tempInitPoints;
    public void GenerateNewNPC()
    {
        Guild_NPC newNPC = new Guild_NPC();
        newNPC.firstName = GeneratorData.NPC_Names.GetFirstName();
        newNPC.lastName = GeneratorData.NPC_Names.GetLastName();

        // Generate initial distribution
        List<int> statPoints = new List<int>();
        //newNPC.classId = (GeneratorData.NPC_CLASS_ID)Random.Range(0, (int)GeneratorData.NPC_CLASS_ID.NUM_CLASSES);
        newNPC.classId = (GeneratorData.NPC_CLASS_ID)Rand.GetRandom(0, (int)GeneratorData.NPC_CLASS_ID.NUM_CLASSES);
        switch(newNPC.classId)
        {
            case GeneratorData.NPC_CLASS_ID.ARCHER:
                newNPC.displayClassName = "Archer";
                break;
            case GeneratorData.NPC_CLASS_ID.CLERIC:
                newNPC.displayClassName = "Cleric";
                break;
            case GeneratorData.NPC_CLASS_ID.FIGHTER:
                newNPC.displayClassName = "Fighter";
                break;
            case GeneratorData.NPC_CLASS_ID.MAGE:
                newNPC.displayClassName = "Mage";
                break;
            case GeneratorData.NPC_CLASS_ID.PALADIN:
                newNPC.displayClassName = "Paladin";
                break;
        }
        tempInitPoints = maxInitPoints;
        for(int i = 0; i < (int)GeneratorData.NPC_STAT_ID.NUM_STATS; ++i)
        {
            //int randomNext = Random.Range(5, tempInitPoints > 18 ? 18 : tempInitPoints);
            // Hack to make sure we don't end up going under by accident.
            if (tempInitPoints <= 5)
                tempInitPoints = 6;
            int randomNext = Rand.GetRandom(5, tempInitPoints > 18 ? 18 : tempInitPoints);
            statPoints.Add(randomNext);
            tempInitPoints -= randomNext;
        }

        // Distribute any remaining points.
        while(tempInitPoints > 0)
        {
            int index = Rand.GetRandom(0, statPoints.Count);
            if (statPoints[index] < 18)
                ++statPoints[index];

            --tempInitPoints;
        }
        statPoints.Sort();
        // Get the weighted values.
        List<GeneratorData.NPC_Stat_Weight> weights = new List<GeneratorData.NPC_Stat_Weight>();
        GeneratorData.NPC_Class_Generator gen = dat.classData.Find(c => c.id == newNPC.classId);
        weights.Add(gen.strWeight);
        weights.Add(gen.dexWeight);
        weights.Add(gen.conWeight);
        weights.Add(gen.intWeight);
        weights.Add(gen.wisWeight);

        //weights.Sort(delegate(GeneratorData.NPC_Stat_Weight s1, GeneratorData.NPC_Stat_Weight s2) { return s1.statID.CompareTo(s2.statID); });
        weights.Sort((a, b) => a.statValue.CompareTo(b.statValue));
        //var sortedList = weights.OrderByDescending(x => x.statValue).ToList();
        

        // distribute.
        for(int i = 0; i < (int)GeneratorData.NPC_STAT_ID.NUM_STATS; ++i)
        {
            NPC_Stat newStat = new NPC_Stat();
            //newStat.statID = weights[i].statID;
            newStat.statValue = statPoints[i];
            // TODO: LOAD DISPLAY NAMES WITH THE WEIGHTS.
            switch(weights[i].statID)
            {
                case GeneratorData.NPC_STAT_ID.STRENGTH:
                    newStat.displayName = "Strength";
                    break;
                case GeneratorData.NPC_STAT_ID.DEXTERITY:
                    newStat.displayName = "Dexterity";
                    break;
                case GeneratorData.NPC_STAT_ID.CONSTITUTION:
                    newStat.displayName = "Constitution";
                    break;
                case GeneratorData.NPC_STAT_ID.INTELLIGENCE:
                    newStat.displayName = "Intelligence";
                    break;
                case GeneratorData.NPC_STAT_ID.WISDOM:
                    newStat.displayName = "Wisdom";
                    break;
            }

            newNPC.statData.stats.Add(weights[i].statID, newStat);
        }

        newNPC.Level = 1;

        listOfNPCs.Add(newNPC);
        NPC_Pool.Add(newNPC);
    }

    public Guild_NPC GetNPCFromPool()
    {
        return NPC_Pool[Rand.GetRandom(0, NPC_Pool.Count)];
    }

    public int GetNPCFromPoolI()
    {
        return Rand.GetRandom(0, NPC_Pool.Count);
    }
}
