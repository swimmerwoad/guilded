﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "QuestData", menuName = "ScriptableObjects/QuestData", order = 3)]
public class QuestDataStorage : ScriptableObject
{
    public class Quest_Chat
    {
        public Quest_Chat()
        {
            Chat = new List<string>();
            Objects = new List<string>();
        }
        public List<string> Chat { get; set; }
        public List<string> Objects { get; set; }

        public readonly string name1Tag = "NAME";
        public readonly string name2Tag = "NAME2";
        public readonly string objectTag = "OBJECT";
    }

    public class Quest_LoadPool
    {
        public Quest_LoadPool()
        {
            Bases = new List<string>();
            QGiverUnnamed = new List<string>();
            QGiverNamed = new List<string>();
            Area = new List<string>();
            EnemyActions = new List<string>();
            EnemyType = new List<string>();
            AffectedPlace = new List<string>();
            Checks = new List<Quest_Check>();
            Chat = new Quest_Chat();
        }
        public List<string> Bases { get; set; }
        public List<string> QGiverUnnamed { get; set; }
        public List<string> QGiverNamed { get; set; }
        public List<string> Area { get; set; }
        public List<string> EnemyActions { get; set; }
        public List<string> EnemyType { get; set; }
        public List<string> AffectedPlace { get; set; }
        public List<Quest_Check> Checks { get; set; }
        public string PositiveCheck { get; set; }
        public string NegativeCheck { get; set; }
        public Quest_Chat Chat { get; set; }


        private readonly string giverTag = "QUEST_GIVER";
        private readonly string giverTagG = "QUEST_GIVER_U";
        private readonly string giverTagN = "QUEST_GIVER_N";
        private readonly string areaTag = "AREA_OF_ACTION";
        private readonly string enemyTag = "ENEMY_TYPE";
        private readonly string eActionTag = "ENEMY_ACTION";
        private readonly string placeTag = "AFFECTED_PLACE";
        private readonly string rewardTag = "REWARD";

        public string GetNewTitle()
        {
            string s = Bases[Rand.GetRandom(0, Bases.Count)];
            int r = Rand.GetRandom(0, 2);
            if(s.Contains(giverTagG))
            {
                s = RepInString(s, giverTagG, QGiverUnnamed);
            }
            else if (s.Contains(giverTagN))
            {
                s = RepInString(s, giverTagN, QGiverNamed);
            }
            else
            {
                if (r == 0)
                    s = RepInString(s, giverTag, QGiverUnnamed);
                else
                    s = RepInString(s, giverTag, QGiverNamed);
            }
            s = RepInString(s, areaTag, Area);
            s = RepInString(s, enemyTag, EnemyType);
            s = RepInString(s, eActionTag, EnemyActions);
            s = RepInString(s, placeTag, AffectedPlace);

            s = s.Replace(rewardTag, "" + Rand.GetRandom(200, 10000) + " gold");

            return s;
        }

        private string RepInString(string s, string tag, List<string> sL)
        {
            return s.Replace(tag, sL[Rand.GetRandom(0, sL.Count)]);
        }

        public List<Quest_Check> GenerateChecks(int difficultyDist, int numChecks)
        {
            List<Quest_Check> nQC = new List<Quest_Check>();
            for(int i = 0; i < numChecks; ++i)
            {
                Quest_Check qc = new Quest_Check();
                GeneratorData.NPC_STAT_CHECK type = (GeneratorData.NPC_STAT_CHECK)Rand.GetRandom(0, (int)GeneratorData.NPC_STAT_CHECK.NUM_CHECKS);
                // Check to make sure we have no duplicate checks.
                while(nQC.Any(t => t.Type == type))
                    type = (GeneratorData.NPC_STAT_CHECK)Rand.GetRandom(0, (int)GeneratorData.NPC_STAT_CHECK.NUM_CHECKS);

                var l = Checks.FindAll(c => c.Type == type);
                qc.CheckValue = l[Rand.GetRandom(0, l.Count)].CheckValue;
                qc.Type = type;
                nQC.Add(qc);
            }

            for(int i = 0; i < nQC.Count; ++i)
            {
                if(nQC[i].Type != GeneratorData.NPC_STAT_CHECK.LEVEL_CHECK)
                {
                    nQC[i].Difficulty = Rand.GetRandom(5, 15);
                    difficultyDist -= nQC[i].Difficulty;
                }
            }

            // Check to make sure we didn't go under or over the distribution by accident.
            while(difficultyDist != 0)
            {
                if(difficultyDist < 0)
                {
                    --nQC[Rand.GetRandom(0, nQC.Count)].Difficulty;
                    ++difficultyDist;
                }
                if(difficultyDist > 0)
                {
                    ++nQC[Rand.GetRandom(0, nQC.Count)].Difficulty;
                    --difficultyDist;
                }
            }

            return nQC;
        }
    }

    public class Quest_Check
    {
        public GeneratorData.NPC_STAT_CHECK Type { get; set; }
        public string CheckValue { get; set; }
        public int Difficulty { get; set; }
    }

    public class Quest
    {
        public Quest()
        {
            CurChecks = new List<Quest_Check>();
            QuestMonsters = new List<QuestMonster>();
            QActions = new List<string>();
        }
        public string Title { get; set; }
        public List<QuestMonster> QuestMonsters { get; set; }
        public int NumDiffMonsters { get; set; }
        // Idea: specific rolls?
        public List<Quest_Check> CurChecks { get; set; }
        // Idea: battles?
        // Idea: rewards?
        public List<string> QActions { get; set; }
    }

    public class QuestMonster
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public int BaseDamage { get; set; }
        public int DamageRange { get; set; }
        public int Speed { get; set; }
    }

    public Quest CurQuest { get; set; }
    public Quest LastQuest { get; set; }
    public Quest_LoadPool QuestPool { get; set; }
    public List<QuestMonster> QuestMonsters { get; set; }

    public int currentDiffDistribution;
    public int currentNumChecks;

    public void GenerateNewQuest()
    {
        if (CurQuest != null)
            LastQuest = CurQuest;
        Quest q = new Quest();
        q.Title = QuestPool.GetNewTitle();
        q.CurChecks = QuestPool.GenerateChecks(currentDiffDistribution, currentNumChecks);
        CurQuest = q;
    }

    public List<string> RunQuest(List<NPCDataStorage.Guild_NPC> npcsOnQuest)
    {
        List<string> questActions = new List<string>();
        List<bool> succFail = new List<bool>();
        foreach(var c in CurQuest.CurChecks)
        {
            // Get the highest stat of certain type, of the action, from the list of different NPCs. 
            // Roll d20, add 1/2 * stat to the roll, compare to difficulty, check succ / fail.

            // weights.Sort((a, b) => a.statValue.CompareTo(b.statValue));
            //npcsOnQuest.Sort((a,b) => a.statData.stats[]);
            if(CheckStat(c.Type))
            {
                var id = (GeneratorData.NPC_STAT_ID)c.Type;
                // WHAT IS THIS LINE DOING
                npcsOnQuest.Sort((a, b) => a.statData.stats[id].statValue.CompareTo(b.statData.stats[id].statValue));
                // roll.
                RollNonPartyCheck(npcsOnQuest, succFail, c, questActions, npcsOnQuest[npcsOnQuest.Count - 1].statData.stats[id].statValue);
            }
            else if(c.Type == GeneratorData.NPC_STAT_CHECK.LEVEL_CHECK)
            {
                npcsOnQuest.Sort((a, b) => a.Level.CompareTo(b.Level));
                RollNonPartyCheck(npcsOnQuest, succFail, c, questActions, npcsOnQuest[npcsOnQuest.Count - 1].Level);
            }
            else if(c.Type == GeneratorData.NPC_STAT_CHECK.PARTY_MAX_CHECK)
            {
                float diff = 1.0f;
                float succChance = npcsOnQuest.Count / npcsOnQuest.Count;
                float failChance = diff - succChance;
                float roll = Rand.GetFloat();
                succFail.Add(roll > failChance);
                AddCheckDisplay(questActions, "NAME", npcsOnQuest[npcsOnQuest.Count - 1].firstName, c, succFail[succFail.Count - 1]);
            }

            Debug.Log("I'm here!");
        }

        DoChats(questActions, npcsOnQuest);
        ShuffleActions(questActions);

        string s = "The party returned, ";
        s += CheckQuestSuccess(succFail) ? "succeeding in their task!" : "failing the task.";
        questActions.Add(s);

        CurQuest.QActions = questActions;

        foreach (var n in npcsOnQuest)
            n.LevelUp();

        return questActions;
    }

    private bool CheckStat(GeneratorData.NPC_STAT_CHECK c)
    {
        return c == GeneratorData.NPC_STAT_CHECK.STRENGTH ||
            c == GeneratorData.NPC_STAT_CHECK.DEXTERITY ||
            c == GeneratorData.NPC_STAT_CHECK.CONSTITUTION ||
            c == GeneratorData.NPC_STAT_CHECK.INTELLIGENCE ||
            c == GeneratorData.NPC_STAT_CHECK.WISDOM;
    }

    private void RollNonPartyCheck(List<NPCDataStorage.Guild_NPC> npcsOnQuest, List<bool> succFail, Quest_Check c, List<string> questActions, int stat)
    {
        int roll = Rand.GetRandom(0, 21);
        roll += stat / 2;
        succFail.Add(roll >= c.Difficulty);
        AddCheckDisplay(questActions, "NAME", npcsOnQuest[npcsOnQuest.Count - 1].firstName, c, roll >= c.Difficulty);
    }

    private void AddCheckDisplay(List<string> l, string tag, string name, Quest_Check c, bool pass)
    {
        string p = pass ? QuestPool.PositiveCheck : QuestPool.NegativeCheck;
        l.Add(c.CheckValue.Replace(tag, name) + p);
    }

    private void DoChats(List<string> actions, List<NPCDataStorage.Guild_NPC> npcs)
    {
        int numChats = Rand.GetRandom(CurQuest.CurChecks.Count / 2, CurQuest.CurChecks.Count - 1);

        for(int i = 0; i < numChats; ++i)
        {
            string origChat = QuestPool.Chat.Chat[Rand.GetRandom(0, QuestPool.Chat.Chat.Count)];
            var npc1 = npcs[Rand.GetRandom(0, npcs.Count)];
            var npc2 = npcs[Rand.GetRandom(0, npcs.Count)];
            while (npc1 == npc2)
            {
                npc2 = npcs[Rand.GetRandom(0, npcs.Count)];
            }
            var @object = QuestPool.Chat.Objects[Rand.GetRandom(0, QuestPool.Chat.Objects.Count)];

            string s = RepChat(origChat, npc1.firstName, npc2.firstName, QuestPool.Chat.name1Tag, QuestPool.Chat.name2Tag, @object, QuestPool.Chat.objectTag);

            actions.Add(s);
        }

    }

    private string RepChat(string orig, string name1, string name2, string tag1, string tag2, string obj, string objTag)
    {
        orig = FuncLib.ExactReplace(orig, tag1, name1);
        orig = FuncLib.ExactReplace(orig, tag2, name2);
        orig = FuncLib.ExactReplace(orig, objTag, obj);
        return orig;
    }

    private void ShuffleActions(List<string> actions)
    {
        // https://stackoverflow.com/questions/273313/randomize-a-listt
        var n = actions.Count;
        while(n > 1)
        {
            --n;
            var k = Rand.GetRandom(0, n);
            var value = actions[k];
            actions[k] = actions[n];
            actions[n] = value;
        }
    }

    /// <summary>
    /// Roll to check whether the quest was a success or failure.
    /// </summary>
    /// <param name="successes">The amount of successful ability checks, basically.</param>
    private bool CheckQuestSuccess(List<bool> successes)
    {
        // Find how many successes there were.
        var succs = successes.FindAll(x => x == true);
        // Find how likely we are to fail.
        float succRate = (float)succs.Count / (float)successes.Count;
        float failRate = 1 - succRate;
        // Roll the dice, fire emblem style.
        var roll1 = Rand.GetFloat();
        var roll2 = Rand.GetFloat();
        var avgRoll = (roll1 + roll2) / 2.0f;
        return avgRoll > failRate;
    }
}
