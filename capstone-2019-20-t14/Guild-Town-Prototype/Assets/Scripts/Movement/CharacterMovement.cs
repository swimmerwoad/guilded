﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    public Transform tpCam;
    public float speed;

    private CharacterController cc;
    private Vector3 moveDirection = Vector3.zero;

    private void Start()
    {
        cc = GetComponent<CharacterController>();
    }
 

    void Update()
    {
        //get movement vector and speed
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
        moveDirection *= speed;

        Vector3 actualMotion = Quaternion.Euler(0, tpCam.eulerAngles.y, 0) * moveDirection;

        cc.Move(actualMotion  * Time.deltaTime);
        
        
    }
}
