﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class CameraInput : MonoBehaviour
{
    CinemachineFreeLook tpCam; //third person camera
    float xSpeed;
    float ySpeed;

    private void Start()
    {
        tpCam = GetComponent<CinemachineFreeLook>();
        xSpeed = tpCam.m_XAxis.m_MaxSpeed;
        ySpeed = tpCam.m_YAxis.m_MaxSpeed;
        //getting initial camera speed values

        tpCam.m_XAxis.m_MaxSpeed = 0;
        tpCam.m_YAxis.m_MaxSpeed = 0;


    }
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            tpCam.m_XAxis.m_MaxSpeed = xSpeed;
            tpCam.m_YAxis.m_MaxSpeed = ySpeed;

        }
        if (Input.GetMouseButton(1))
        {
           
        }
        else if (Input.GetMouseButtonUp(1))
        {
            tpCam.m_YAxis.m_MaxSpeed = 0f;
            tpCam.m_XAxis.m_MaxSpeed = 0f;
            
        }

        

       
    }

   
    
}
