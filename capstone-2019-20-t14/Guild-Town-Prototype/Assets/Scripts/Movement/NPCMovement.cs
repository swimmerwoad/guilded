﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using TMPro;
public class NPCMovement : MonoBehaviour
{
    //source: https://gist.github.com/Templar2020/8e4f5296de96d8ccf03263bf1a9f277f
    //but added a stop when the player is near, and a random range for how long

    //make sure min and max timer are more than 0.
    

    public float minWanderTimer = 3f;
    public float maxWanderTimer = 10f;
    public float wanderRadius = .5f;
    public bool isPlayerNear;

    private NavMeshAgent agent;
    private Transform goal;
    
    private float timer;
    private float timerEnd;

    //private TMP_Text NPCDataText;
    public InformationUIManager manager;

    private void Awake()
    {
        //NPCDataText = GameObject.Find("NPCDataText").GetComponent<TMP_Text>();
    }

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        timerEnd = Rand.GetFloatRange(minWanderTimer, maxWanderTimer);
        timer = timerEnd;
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if(timer >= timerEnd && !isPlayerNear)
        {
            Vector3 newPos = RandomNavSphere(transform.position, wanderRadius, -1);
            agent.SetDestination(newPos);
            timer = 0;
            timerEnd = Rand.GetFloatRange(minWanderTimer, maxWanderTimer);
        }
    }

    public static Vector3 RandomNavSphere(Vector3 origin, float dist, int layerMask)
    {
        Vector3 randomDirection = Random.insideUnitSphere * dist;

        randomDirection += origin;

        NavMeshHit navHit;

        NavMesh.SamplePosition(randomDirection, out navHit, dist, layerMask);

        return navHit.position;
    }

    public void DebugInteract()
    {
        Debug.Log("interacted with " + gameObject.name);
        BringUpMenuDisplay();
    }

    public void BringUpMenuDisplay()
    {
        //NPCDataText.transform.parent.gameObject.SetActive(true);
        //NPCDataText.text = GetComponent<TownNPC>().GetNPCData();
        manager.DisplayNPCStats(GetComponent<TownNPC>().GetNPCData());
        //NPCDataText.transform.parent.GetComponent<Image>().canvasRenderer.SetAlpha(0.0f);
        //NPCDataText.transform.parent.GetComponent<Image>().CrossFadeAlpha(1.0f, 0.5f, true);
    }

}
