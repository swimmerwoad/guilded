﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Interactable : MonoBehaviour
{
     
     public UnityEvent interactEvent;
    public bool isPlayerNear;
    private void OnMouseDown()
    {
        Interact();

    }

    

    void Interact()
    {
        if (isPlayerNear)
        {
          

            interactEvent.Invoke();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            isPlayerNear = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            isPlayerNear = false;
        }
    }


}
