﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownNPC : MonoBehaviour
{
    private NPCDataStorage.Guild_NPC thisNPC;
    public int index;
    public NPCDataStorage d;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetNPC(NPCDataStorage.Guild_NPC newNPC)
    {
        thisNPC = newNPC;
    }

    public string GetNPCData()
    {
        //return thisNPC.DisplayNPCData();
        return d.listOfNPCs[index].DisplayNPCData();
    }
}
