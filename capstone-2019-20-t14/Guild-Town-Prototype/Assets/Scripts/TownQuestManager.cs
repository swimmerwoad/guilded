﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TownQuestManager : MonoBehaviour
{
    public QuestDataStorage qds;
    private List<NPCDataStorage.Guild_NPC> npcs = new List<NPCDataStorage.Guild_NPC>();
    public GameObject nextDayButton;
    // Start is called before the first frame update
    void Start()
    {
        if (qds.CurQuest == null)
            qds.GenerateNewQuest();

        nextDayButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateQuest()
    {
        qds.GenerateNewQuest();
    }

    public string GetQuestTitle()
    {
        return qds.CurQuest.Title;
    }

    public void RunQuest()
    {
        qds.RunQuest(npcs);
        nextDayButton.SetActive(true);
        npcs.Clear();
    }

    public void AddNPC(int index)
    {
        npcs.Add(GetComponent<TownManager>().data.listOfNPCs[index]);
    }

    public void RemoveNPC(int index)
    {
        //npcs.Remove(GetComponent<TownManager>().data.listOfNPCs[index]);
        var npcToFind = GetComponent<TownManager>().data.listOfNPCs[index];
        npcs.Remove(npcToFind);
    }

    public int GetCount()
    {
        return npcs.Count;
    }

    public List<string> GetActions()
    {
        return qds.LastQuest.QActions;
    }
}
