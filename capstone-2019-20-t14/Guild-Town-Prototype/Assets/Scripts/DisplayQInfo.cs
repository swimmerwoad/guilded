﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
public class DisplayQInfo : MonoBehaviour
{
    private Text t;
    public QuestDataStorage s;
    public NPCDataStorage d;
    public List<string> prevQActions = new List<string>();
    // Start is called before the first frame update
    void Start()
    {
        t = GetComponent<Text>();
        d.GenerateNewNPC();
        d.GenerateNewNPC();
        d.GenerateNewNPC();
        if (s.CurQuest == null)
            NewQuest();
        t.text = s.CurQuest.Title;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < prevQActions.Count; ++i)
            {

                s.Append(i + 1 + ": ");
                s.AppendLine(prevQActions[i]);
            }

            t.text = s.ToString();
        }

    }

    public void NewQuest()
    {
        s.GenerateNewQuest();
        prevQActions = s.RunQuest(d.listOfNPCs);
    }
}
