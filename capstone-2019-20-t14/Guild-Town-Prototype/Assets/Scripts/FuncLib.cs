﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

public class FuncLib : MonoBehaviour
{
    // https://stackoverflow.com/questions/13870725/how-to-search-and-replace-exact-matching-strings-only
    public static string ExactReplace(string input, string find, string replace)
    {
        string textToFind = string.Format(@"\b{0}\b", find);
        return Regex.Replace(input, textToFind, replace);
    }
}
