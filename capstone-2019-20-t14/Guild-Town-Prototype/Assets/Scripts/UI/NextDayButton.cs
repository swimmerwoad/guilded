﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
public class NextDayButton : MonoBehaviour
{
    public QuestControl qCtrl;
    public CharacterStatControl cCtrl;
    public GameObject questActionHolder;
    public TownQuestManager qMngr;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Click()
    {
        qMngr.GenerateQuest(); //DEBUG. THIS MUST BE MOVED ELSEWHERE.
        qCtrl.Close();
        cCtrl.Close();
        questActionHolder.SetActive(true);
        StringBuilder b = new StringBuilder();
        var l = qMngr.GetActions();
        for(int i = 0; i < l.Count; ++i)
        {
            b.AppendLine((i + 1) + ": " + l[i]);
        }
        questActionHolder.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = b.ToString();

        gameObject.SetActive(false);
    }
}
