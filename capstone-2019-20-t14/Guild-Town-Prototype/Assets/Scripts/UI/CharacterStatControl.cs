﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterStatControl : MonoBehaviour
{
    private Image image;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        image.canvasRenderer.SetAlpha(0.0f);
        gameObject.SetActive(false);
    }

    private void Update()
    {
        if (image.canvasRenderer.GetAlpha() <= 0.0f)
            gameObject.SetActive(false);
    }

    public void Close()
    {
        image.canvasRenderer.SetAlpha(0.0f);
        image.CrossFadeAlpha(0.0f, 0.5f, true);
    }
}
