﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonScript : MonoBehaviour
{
    private bool clicked = false;
    public TownQuestManager mngr;
    private Image image;
    private Button butt;
    // Start is called before the first frame update
    void Start()
    {
        mngr = GameObject.Find("Manager").GetComponent<TownQuestManager>();
        GetComponent<Button>().onClick.AddListener(Click);
        image = GetComponent<Image>();
        butt = GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Click()
    {
        if (mngr.GetCount() >= 4 && !clicked)
            return;
        else if(mngr.GetCount() >= 4 && clicked)
        {
            mngr.RemoveNPC(transform.GetSiblingIndex());
            image.color = butt.colors.normalColor;
            clicked = !clicked;
            return;
        }

        clicked = !clicked;

        if (clicked)
        {
            mngr.AddNPC(transform.GetSiblingIndex());
            //GetComponent<Button>().interactable = false;
            image.color = butt.colors.disabledColor;
        }
        else
        {
            mngr.RemoveNPC(transform.GetSiblingIndex());
            image.color = butt.colors.normalColor;
        }
    }
}
